package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.*;
import net.ihe.gazelle.oasis.testassertion.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Ignore
public class OasisIntegrationTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        OasisTestAssertionFactory.cleanOasisTestAssertions();
        super.tearDown();
    }

    @Test
    public void test_assertion() {
        String description_text = "Integration testing description";
        String normativeSourcecontent = "Integration testing normative source";
        String targetContent = "Integration testing target";

        OasisTestAssertion testAssertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        OasisDescription description = OasisDescriptionFactory.createDescriptionWithMandatoryFields();
        description.setContent(description_text);

        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        target.setContent(targetContent);
        OasisPrerequisite prerequisite = OasisPrerequisiteFactory.createPrerequisiteWithMandatoryFieldsAndSaveIt();
        OasisPredicate predicate = OasisPredicateFactory.createPredicateWithMandatoryFieldsAndSaveIt();
        OasisPrescription prescription = OasisPrescriptionFactory.createPrescriptionWithMandatoryFieldsAndSaveIt();
        OasisTag tag = OasisTagFactory.createTagWithMandatoryFieldsAndSaveIt();
        OasisVariable variable = OasisVariableFactory.createVariableWithMandatoryFieldsAndSaveIt();
        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        OasisComment comment = OasisCommentFactory.createCommentWithMandatoryFields();
        OasisInterpretation interpretation = OasisInterpretationFactory.createInterpretationWithMandatoryFields();
        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        OasisTextSourceItem textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();
        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();

        List<OasisDerivedSourceItem> derivedSourceItems = new ArrayList<OasisDerivedSourceItem>();
        derivedSourceItems.add(derivedSourceItem);

        List<OasisRefSourceItem> refSourceItems = new ArrayList<OasisRefSourceItem>();
        refSourceItems.add(refSourceItem);

        List<OasisTextSourceItem> textSourceItems = new ArrayList<OasisTextSourceItem>();
        textSourceItems.add(textSourceItem);

        normativeSource.setComment(comment);
        normativeSource.setContent(normativeSourcecontent);
        normativeSource.setDerivedSourceItems(derivedSourceItems);
        normativeSource.setInterpretation(interpretation);
        normativeSource.setRefSourceItems(refSourceItems);
        normativeSource.setTextSourceItems(textSourceItems);
        normativeSource = (OasisNormativeSource) DatabaseManager.writeObject(normativeSource);

        testAssertion.setNormativeSource(normativeSource);
        List<OasisTag> tags = new ArrayList<OasisTag>();
        tags.add(tag);

        List<OasisVariable> variables = new ArrayList<OasisVariable>();
        variables.add(variable);

        testAssertion.setDescription(description);
        testAssertion.setTarget(target);
        testAssertion.setPrerequisite(prerequisite);
        testAssertion.setPredicate(predicate);
        testAssertion.setPrescription(prescription);
        testAssertion.setTags(tags);
        testAssertion.setVariables(variables);

        testAssertion = (OasisTestAssertion) DatabaseManager.writeObject(testAssertion);

        OasisTestAssertion result = OasisTestAssertionFactory.getOasisTestAssertionFromDatabase(testAssertion);

        assertEquals(1, result.getVariables().size());
        assertNotNull(result.getNormativeSource());
        assertNotNull(result.getNormativeSource().getComment());
        assertNotNull(result.getNormativeSource().getInterpretation());
        assertNotNull(result.getNormativeSource().getRefSourceItems());
        assertNotNull(result.getNormativeSource().getTextSourceItems());
        assertNotNull(result.getNormativeSource().getDerivedSourceItems());
        assertNotNull(result.getTarget());
        assertNotNull(result.getPrerequisite());
        assertNotNull(result.getPredicate());
        assertNotNull(result.getPrescription());
        assertNotNull(result.getTags());
        assertNotNull(result.getVariables());
        assertEquals(targetContent, result.getTarget().getContent());
        assertEquals(description_text, result.getDescription().getContent());
        assertEquals(normativeSourcecontent, result.getNormativeSource().getContent());

    }
}
