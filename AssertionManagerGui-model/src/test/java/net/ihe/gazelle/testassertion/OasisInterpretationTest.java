package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisInterpretationFactory;
import net.ihe.gazelle.oasis.testassertion.OasisInterpretation;
import net.ihe.gazelle.oasis.testassertion.OasisInterpretationQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisInterpretationTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public int get_Interpretation_Table_Size() {
        OasisInterpretationQuery query = new OasisInterpretationQuery();
        return query.getList().size();
    }

    @Test
    public void test_Interpretation_has_a_valid_factory() {
        int initSize = get_Interpretation_Table_Size();

        OasisInterpretation interpretation = OasisInterpretationFactory.createInterpretationWithMandatoryFields();
        DatabaseManager.writeObject(interpretation);

        int endSize = get_Interpretation_Table_Size();
        assertEquals("1 interpretation should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisInterpretation_works_with_300_char_long_content() {
        int initSize = get_Interpretation_Table_Size();

        OasisInterpretation interpretation = OasisInterpretationFactory.createInterpretationWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        interpretation.setContent(randomAscii);

        OasisInterpretation obj = (OasisInterpretation) DatabaseManager.writeObject(interpretation);

        int endSize = get_Interpretation_Table_Size();
        assertEquals("1 new interpretation should be stored", 1, endSize - initSize);

        OasisInterpretation result = OasisInterpretationFactory.getOasisInterpretationFromDatabase(obj);
        assertEquals(randomAscii, result.getContent());
    }

}
