package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisRefSourceItemFactory;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItemQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@Ignore
public class OasisRefSourceItemTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public int get_OasisRefSourceItem_Table_Size() {
        OasisRefSourceItemQuery query = new OasisRefSourceItemQuery();
        return query.getList().size();
    }

    @Test
    public void test_OasisRefSourceItem_has_a_valid_factory() {
        int initSize = get_OasisRefSourceItem_Table_Size();

        OasisRefSourceItem normativeSource = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_OasisRefSourceItem_Table_Size();
        assertEquals("1 refSourceItem should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisRefSourceItem_works_with_300_char_long_name() {
        int initSize = get_OasisRefSourceItem_Table_Size();

        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        refSourceItem.setName(randomAscii);

        OasisRefSourceItem obj = (OasisRefSourceItem) DatabaseManager.writeObject(refSourceItem);

        int endSize = get_OasisRefSourceItem_Table_Size();
        assertEquals("1 new refSourceItem should be stored", 1, endSize - initSize);

        OasisRefSourceItem result = OasisRefSourceItemFactory.getOasisRefSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getName());
    }

    @Test
    public void test_OasisRefSourceItem_works_with_300_char_long_uri() {
        int initSize = get_OasisRefSourceItem_Table_Size();

        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        refSourceItem.setUri(randomAscii);

        OasisRefSourceItem obj = (OasisRefSourceItem) DatabaseManager.writeObject(refSourceItem);

        int endSize = get_OasisRefSourceItem_Table_Size();
        assertEquals("1 new refSourceItem should be stored", 1, endSize - initSize);

        OasisRefSourceItem result = OasisRefSourceItemFactory.getOasisRefSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getUri());
    }

    @Test
    public void test_OasisRefSourceItem_works_with_300_char_long_documentID() {
        int initSize = get_OasisRefSourceItem_Table_Size();

        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        refSourceItem.setDocumentId(randomAscii);

        OasisRefSourceItem obj = (OasisRefSourceItem) DatabaseManager.writeObject(refSourceItem);

        int endSize = get_OasisRefSourceItem_Table_Size();
        assertEquals("1 new refSourceItem should be stored", 1, endSize - initSize);

        OasisRefSourceItem result = OasisRefSourceItemFactory.getOasisRefSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getDocumentId());
    }

    @Test
    public void test_OasisRefSourceItem_works_with_300_char_long_versionID() {
        int initSize = get_OasisRefSourceItem_Table_Size();

        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        refSourceItem.setVersionId(randomAscii);

        OasisRefSourceItem obj = (OasisRefSourceItem) DatabaseManager.writeObject(refSourceItem);

        int endSize = get_OasisRefSourceItem_Table_Size();
        assertEquals("1 new refSourceItem should be stored", 1, endSize - initSize);

        OasisRefSourceItem result = OasisRefSourceItemFactory.getOasisRefSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getVersionId());
    }

    @Test
    public void test_OasisRefSourceItem_works_with_300_char_long_revisionID() {
        int initSize = get_OasisRefSourceItem_Table_Size();

        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        refSourceItem.setRevisionId(randomAscii);

        OasisRefSourceItem obj = (OasisRefSourceItem) DatabaseManager.writeObject(refSourceItem);

        int endSize = get_OasisRefSourceItem_Table_Size();
        assertEquals("1 new refSourceItem should be stored", 1, endSize - initSize);

        OasisRefSourceItem result = OasisRefSourceItemFactory.getOasisRefSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getRevisionId());
    }

    @Test
    public void test_OasisRefSourceItem_works_with_300_char_long_resourceProvenanceID() {
        int initSize = get_OasisRefSourceItem_Table_Size();

        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        refSourceItem.setResourceProvenanceId(randomAscii);

        OasisRefSourceItem obj = (OasisRefSourceItem) DatabaseManager.writeObject(refSourceItem);

        int endSize = get_OasisRefSourceItem_Table_Size();
        assertEquals("1 new refSourceItem should be stored", 1, endSize - initSize);

        OasisRefSourceItem result = OasisRefSourceItemFactory.getOasisRefSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getResourceProvenanceId());
    }


    protected String getDb() {
        return "assertion-manager-junit";
    }

    /**
     * Method getHbm2ddl. Used to control how hibernate manages database schema migrations possible values are: validate, update, create, create-drop.
     *
     * @return String
     */
    protected String getHbm2ddl() {
        return "update";
    }
}
