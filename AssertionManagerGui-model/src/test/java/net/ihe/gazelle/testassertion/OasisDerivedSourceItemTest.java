package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisDerivedSourceItemFactory;
import net.ihe.gazelle.oasis.testassertion.OasisDerivedSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisDerivedSourceItemQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisDerivedSourceItemTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public int get_derivedSourceItem_Table_Size() {
        OasisDerivedSourceItemQuery query = new OasisDerivedSourceItemQuery();
        return query.getList().size();
    }

    @Test
    public void test_derivedSourceItem_has_a_valid_factory() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 derivedSourceItem should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisDerivedSourceItem_works_with_300_char_long_content() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        derivedSourceItem.setContent(randomAscii);

        OasisDerivedSourceItem obj = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 new derivedSourceItem should be stored", 1, endSize - initSize);

        OasisDerivedSourceItem result = OasisDerivedSourceItemFactory.getOasisDerivedSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getContent());
    }

    @Test
    public void test_OasisDerivedSourceItem_works_with_300_char_long_name() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        derivedSourceItem.setName(randomAscii);

        OasisDerivedSourceItem obj = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 new derivedSourceItem should be stored", 1, endSize - initSize);

        OasisDerivedSourceItem result = OasisDerivedSourceItemFactory.getOasisDerivedSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getName());
    }

    @Test
    public void test_OasisDerivedSourceItem_works_with_255_char_long_date() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(255);
        derivedSourceItem.setDate(randomAscii);

        OasisDerivedSourceItem obj = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 new derivedSourceItem should be stored", 1, endSize - initSize);

        OasisDerivedSourceItem result = OasisDerivedSourceItemFactory.getOasisDerivedSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getDate());
    }

    @Test
    public void test_OasisDerivedSourceItem_works_with_300_char_long_documentID() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        derivedSourceItem.setDocumentId(randomAscii);

        OasisDerivedSourceItem obj = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 new derivedSourceItem should be stored", 1, endSize - initSize);

        OasisDerivedSourceItem result = OasisDerivedSourceItemFactory.getOasisDerivedSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getDocumentId());
    }

    @Test
    public void test_OasisDerivedSourceItem_works_with_300_char_long_revisionID() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        derivedSourceItem.setRevisionId(randomAscii);

        OasisDerivedSourceItem obj = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 new derivedSourceItem should be stored", 1, endSize - initSize);

        OasisDerivedSourceItem result = OasisDerivedSourceItemFactory.getOasisDerivedSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getRevisionId());
    }

    @Test
    public void test_OasisDerivedSourceItem_works_with_300_char_long_ResourceProvenanceId() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        derivedSourceItem.setResourceProvenanceId(randomAscii);

        OasisDerivedSourceItem obj = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 new derivedSourceItem should be stored", 1, endSize - initSize);

        OasisDerivedSourceItem result = OasisDerivedSourceItemFactory.getOasisDerivedSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getResourceProvenanceId());
    }

    @Test
    public void test_OasisDerivedSourceItem_works_with_300_char_long_uri() {
        int initSize = get_derivedSourceItem_Table_Size();

        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        derivedSourceItem.setUri(randomAscii);

        OasisDerivedSourceItem obj = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);

        int endSize = get_derivedSourceItem_Table_Size();
        assertEquals("1 new derivedSourceItem should be stored", 1, endSize - initSize);

        OasisDerivedSourceItem result = OasisDerivedSourceItemFactory.getOasisDerivedSourceItemFromDatabase(obj);
        assertEquals(randomAscii, result.getUri());
    }
}
