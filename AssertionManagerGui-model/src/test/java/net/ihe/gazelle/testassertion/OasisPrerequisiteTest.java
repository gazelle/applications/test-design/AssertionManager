package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisPrerequisiteFactory;
import net.ihe.gazelle.oasis.testassertion.OasisLanguageValues;
import net.ihe.gazelle.oasis.testassertion.OasisPrerequisite;
import net.ihe.gazelle.oasis.testassertion.OasisPrerequisiteQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisPrerequisiteTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private int get_Oasis_Prerequisite_Table_Size() {
        OasisPrerequisiteQuery query = new OasisPrerequisiteQuery();
        return query.getList().size();
    }

    @Test
    public void test_OasisPrerequisite_has_a_valid_factory() {
        int initSize = get_Oasis_Prerequisite_Table_Size();

        OasisPrerequisite prerequisite = OasisPrerequisiteFactory.createPrerequisiteWithMandatoryFieldsAndSaveIt();
        DatabaseManager.writeObject(prerequisite);

        int endSize = get_Oasis_Prerequisite_Table_Size();
        assertEquals("1 prerequisite should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisPrerequisite_fails_without_content() {
        int initSize = get_Oasis_Prerequisite_Table_Size();

        OasisPrerequisite prerequisite = OasisPrerequisiteFactory.createPrerequisiteWithMandatoryFields();
        prerequisite.setContent(null);

        DatabaseManager.writeObject(prerequisite);

        int endSize = get_Oasis_Prerequisite_Table_Size();
        assertEquals("No prerequisite should be stored", 0, endSize - initSize);
    }

    @Test
    public void test_OasisPrerequisite_works_without_language() {
        int initSize = get_Oasis_Prerequisite_Table_Size();

        OasisPrerequisite prerequisite = OasisPrerequisiteFactory.createPrerequisiteWithMandatoryFields();
        prerequisite.setLanguage(null);

        DatabaseManager.writeObject(prerequisite);

        int endSize = get_Oasis_Prerequisite_Table_Size();
        assertEquals("1 prerequisite should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisPrerequisite_works_with_language() {
        int initSize = get_Oasis_Prerequisite_Table_Size();

        OasisPrerequisite prerequisite = OasisPrerequisiteFactory.createPrerequisiteWithMandatoryFields();
        prerequisite.setLanguage(OasisLanguageValues.NATURAL);

        DatabaseManager.writeObject(prerequisite);

        int endSize = get_Oasis_Prerequisite_Table_Size();
        assertEquals("1 new prerequisite should be stored", 1, endSize - initSize);
    }

}
