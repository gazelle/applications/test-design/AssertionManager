package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisCommentFactory;
import net.ihe.gazelle.oasis.testassertion.OasisComment;
import net.ihe.gazelle.oasis.testassertion.OasisCommentQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisCommentTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        OasisCommentFactory.cleanOasisComment();
        super.tearDown();
    }

    public int get_comment_Table_Size() {
        OasisCommentQuery query = new OasisCommentQuery();
        return query.getList().size();
    }

    @Test
    public void test_comment_has_a_valid_factory() {
        int initSize = get_comment_Table_Size();

        OasisComment comment = OasisCommentFactory.createCommentWithMandatoryFields();
        DatabaseManager.writeObject(comment);

        int endSize = get_comment_Table_Size();
        assertEquals("1 comment should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisComment_works_with_300_char_long_content() {
        int initSize = get_comment_Table_Size();

        OasisComment comment = OasisCommentFactory.createCommentWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        comment.setContent(randomAscii);

        OasisComment obj = (OasisComment) DatabaseManager.writeObject(comment);

        int endSize = get_comment_Table_Size();
        assertEquals("1 new comment should be stored", 1, endSize - initSize);

        OasisComment result = OasisCommentFactory.getOasisCommentFromDatabase(obj);
        assertEquals(randomAscii, result.getContent());
    }
}
