package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTagFactory;
import net.ihe.gazelle.oasis.testassertion.OasisTag;
import net.ihe.gazelle.oasis.testassertion.OasisTagQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
@Ignore
public class OasisTagTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private int get_OasisTag_Table_Size() {
        OasisTagQuery query = new OasisTagQuery();
        return query.getList().size();
    }

    @Test
    public void test_OasisTag_has_a_valid_factory() {
        int initSize = get_OasisTag_Table_Size();

        OasisTag tag = OasisTagFactory.createTagWithMandatoryFieldsAndSaveIt();
        tag = (OasisTag) DatabaseManager.writeObject(tag);
        assertNotNull(tag.getId());

        int endSize = get_OasisTag_Table_Size();
        assertEquals("1 tag should be stored", 1, endSize - initSize);
        assertEquals(OasisTagFactory.getName(), tag.getName());
    }

    @Test
    public void test_OasisTag_fails_without_Name() {
        int initSize = get_OasisTag_Table_Size();

        OasisTag tag = OasisTagFactory.createTagWithMandatoryFields();
        tag.setName(null);

        tag = (OasisTag) DatabaseManager.writeObject(tag);

        int endSize = get_OasisTag_Table_Size();
        assertEquals("No tag should be stored", 0, endSize - initSize);
    }

    @Test
    public void test_OasisTag_fails_with_256_char_long_language() {
        int initSize = get_OasisTag_Table_Size();

        OasisTag tag = OasisTagFactory.createTagWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(256);
        tag.setContent(randomAscii);

        OasisTag obj = (OasisTag) DatabaseManager.writeObject(tag);

        int endSize = get_OasisTag_Table_Size();
        assertEquals("1 tag should be stored", 1, endSize - initSize);

        OasisTag result = OasisTagFactory.getOasisTagFromDatabase(obj);
        assertNotNull(result);
        assertEquals(randomAscii, result.getContent());
    }
}
