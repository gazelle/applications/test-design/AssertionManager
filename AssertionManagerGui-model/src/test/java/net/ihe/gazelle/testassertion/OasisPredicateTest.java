package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisPredicateFactory;
import net.ihe.gazelle.oasis.testassertion.OasisLanguageValues;
import net.ihe.gazelle.oasis.testassertion.OasisPredicate;
import net.ihe.gazelle.oasis.testassertion.OasisPredicateQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
@Ignore
public class OasisPredicateTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private int get_OasisPredicate_Table_Size() {
        OasisPredicateQuery query = new OasisPredicateQuery();
        return query.getList().size();
    }

    @Test
    public void test_OasisPredicate_has_a_valid_factory() {
        int initSize = get_OasisPredicate_Table_Size();

        OasisPredicate predicate = OasisPredicateFactory.createPredicateWithMandatoryFieldsAndSaveIt();
        DatabaseManager.writeObject(predicate);

        int endSize = get_OasisPredicate_Table_Size();
        assertEquals("1 predicate should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisPredicate_fails_without_content() {
        int initSize = get_OasisPredicate_Table_Size();

        OasisPredicate predicate = OasisPredicateFactory.createPredicateWithMandatoryFields();
        predicate.setContent(null);

        OasisPredicate obj = (OasisPredicate) DatabaseManager.writeObject(predicate);

        int endSize = get_OasisPredicate_Table_Size();
        assertEquals("No predicate should be stored", 0, endSize - initSize);

        OasisPredicate result = getOasisPredicateFromDatabase(obj);
        assertNull(result);
    }

    @Test
    public void test_OasisPredicate_works_with_300_char_long_content() {
        int initSize = get_OasisPredicate_Table_Size();

        OasisPredicate predicate = OasisPredicateFactory.createPredicateWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        predicate.setContent(randomAscii);

        OasisPredicate obj = (OasisPredicate) DatabaseManager.writeObject(predicate);

        int endSize = get_OasisPredicate_Table_Size();
        assertEquals("1 new predicate should be stored", 1, endSize - initSize);

        OasisPredicate result = getOasisPredicateFromDatabase(obj);
        assertEquals(randomAscii, result.getContent());
    }

    @Test
    public void test_OasisPredicate_works_without_language() {
        int initSize = get_OasisPredicate_Table_Size();

        OasisPredicate predicate = OasisPredicateFactory.createPredicateWithMandatoryFields();
        predicate.setLanguage(null);

        OasisPredicate obj = (OasisPredicate) DatabaseManager.writeObject(predicate);

        int endSize = get_OasisPredicate_Table_Size();
        assertEquals("1 predicate should be stored", 1, endSize - initSize);

        OasisPredicate result = getOasisPredicateFromDatabase(obj);
        assertNotNull(result);
    }

    @Test
    public void test_OasisPredicate_works_with_language() {
        int initSize = get_OasisPredicate_Table_Size();

        OasisPredicate predicate = OasisPredicateFactory.createPredicateWithMandatoryFields();
        predicate.setLanguage(OasisLanguageValues.NATURAL);

        OasisPredicate obj = (OasisPredicate) DatabaseManager.writeObject(predicate);

        int endSize = get_OasisPredicate_Table_Size();
        assertEquals("1 new predicate should be stored", 1, endSize - initSize);

        OasisPredicate result = getOasisPredicateFromDatabase(obj);
        assertEquals(OasisLanguageValues.NATURAL, result.getLanguage());
    }


    private OasisPredicate getOasisPredicateFromDatabase(OasisPredicate predicate) {
        if (predicate.getId() == null) {
            return null;
        } else {
            OasisPredicateQuery query = new OasisPredicateQuery();
            query.eq(predicate);
            List<OasisPredicate> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }
}
