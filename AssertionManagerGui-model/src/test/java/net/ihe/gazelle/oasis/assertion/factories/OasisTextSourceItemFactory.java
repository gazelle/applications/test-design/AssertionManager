package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisTextSourceItem;

public class OasisTextSourceItemFactory {

    public static OasisTextSourceItem createTextSourceItemWithMandatoryFields() {
        OasisTextSourceItem textSourceItem = new OasisTextSourceItem();
        textSourceItem.setName("textSourceItem name");
        textSourceItem.setContent("textSourceItem content");
        textSourceItem = (OasisTextSourceItem) DatabaseManager.writeObject(textSourceItem);
        return textSourceItem;
    }
}
