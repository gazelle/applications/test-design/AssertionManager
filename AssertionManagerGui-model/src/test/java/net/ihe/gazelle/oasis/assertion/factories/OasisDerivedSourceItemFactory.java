package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisDerivedSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisDerivedSourceItemQuery;

import java.util.List;

public class OasisDerivedSourceItemFactory {
    public static OasisDerivedSourceItem createDerivedSourceItemWithMandatoryFields() {
        OasisDerivedSourceItem derivedSourceItem = new OasisDerivedSourceItem();
        derivedSourceItem = (OasisDerivedSourceItem) DatabaseManager.writeObject(derivedSourceItem);
        return derivedSourceItem;
    }

    public static void cleanOasisDerivedSourceItem() {
        OasisDerivedSourceItemQuery derivedSourceItemQuery = new OasisDerivedSourceItemQuery();
        List<OasisDerivedSourceItem> derivedSourceItems = derivedSourceItemQuery.getListDistinct();
        for (OasisDerivedSourceItem derivedSourceItem : derivedSourceItems) {
            DatabaseManager.removeObject(derivedSourceItem);
        }
    }

    public static OasisDerivedSourceItem getOasisDerivedSourceItemFromDatabase(OasisDerivedSourceItem derivedSourceItem) {
        if (derivedSourceItem.getId() == null) {
            return null;
        } else {
            OasisDerivedSourceItemQuery query = new OasisDerivedSourceItemQuery();
            query.eq(derivedSourceItem);
            List<OasisDerivedSourceItem> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }
}
