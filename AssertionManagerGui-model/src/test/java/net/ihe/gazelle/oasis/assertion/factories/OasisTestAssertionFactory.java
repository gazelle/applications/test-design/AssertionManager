package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.*;

import java.util.List;

public class OasisTestAssertionFactory {
    private static int number = 1;

    public static OasisTestAssertion createTestAssertionWithMandatoryFields() {
        OasisTestAssertion assertion = new OasisTestAssertion();
        OasisPredicate predicate = OasisPredicateFactory.createPredicateWithMandatoryFieldsAndSaveIt();
        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        assertion.setTarget(target);
        assertion.setPredicate(predicate);
        assertion.setNormativeSource(normativeSource);
        assertion.setAssertionId("Assertion-id" + number);
        number++;
        assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);
        return assertion;
    }

    public static OasisTestAssertion getOasisTestAssertionFromDatabase(OasisTestAssertion assertion) {
        if (assertion.getId() == null) {
            return null;
        } else {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery();
            query.id().eq(assertion.getId());
            List<OasisTestAssertion> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }

    public static void cleanOasisTestAssertions() {
        OasisTestAssertionQuery assertionQuery = new OasisTestAssertionQuery();
        List<OasisTestAssertion> assertions = assertionQuery.getListDistinct();
        for (OasisTestAssertion assertion : assertions) {
            DatabaseManager.removeObject(assertion);
        }
        OasisPredicateFactory.cleanOasisPredicates();
        OasisTargetFactory.cleanOasisTargets();
        OasisNormativeSourceFactory.cleanOasisNormativeSource();
        OasisDescriptionFactory.cleanOasisDescription();
        OasisTagFactory.cleanOasisTags();
        OasisVariableFactory.cleanOasisVariables();
        OasisPrerequisiteFactory.cleanOasisPrerequisites();
        OasisRefSourceItemFactory.cleanOasisRefSourceItem();
    }
}
