package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisPrescription;
import net.ihe.gazelle.oasis.testassertion.OasisPrescriptionLevelValues;
import net.ihe.gazelle.oasis.testassertion.OasisPrescriptionQuery;

import java.util.List;

public class OasisPrescriptionFactory {

    private static OasisPrescriptionLevelValues defaultLevel = OasisPrescriptionLevelValues.mandatory;

    public static OasisPrescription createPrescriptionWithMandatoryFieldsAndSaveIt() {
        return (OasisPrescription) DatabaseManager.writeObject(createPrescriptionWithMandatoryFields());
    }

    public static OasisPrescription createPrescriptionWithMandatoryFields() {
        OasisPrescription prescription = new OasisPrescription();
        prescription.setContent("");
        prescription.setLevel(defaultLevel);
        return prescription;
    }

    public static OasisPrescription getOasisPrescriptionFromDatabase(OasisPrescription prescription) {
        if (prescription.getId() == null) {
            return null;
        } else {
            OasisPrescriptionQuery query = new OasisPrescriptionQuery();
            query.eq(prescription);
            List<OasisPrescription> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }

    public static OasisPrescriptionLevelValues getDefaultLevel() {
        return defaultLevel;
    }
}
