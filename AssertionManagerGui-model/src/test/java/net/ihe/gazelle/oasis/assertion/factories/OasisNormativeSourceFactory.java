package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisNormativeSource;
import net.ihe.gazelle.oasis.testassertion.OasisNormativeSourceQuery;

import java.util.List;

public class OasisNormativeSourceFactory {

    public static OasisNormativeSource createNormativeSourceWithMandatoryFields() {
        OasisNormativeSource normativeSource = new OasisNormativeSource();
        normativeSource = (OasisNormativeSource) DatabaseManager.writeObject(normativeSource);
        return normativeSource;
    }

    public static void cleanOasisNormativeSource() {
        OasisNormativeSourceQuery normativeSourceQuery = new OasisNormativeSourceQuery();
        List<OasisNormativeSource> normativeSources = normativeSourceQuery.getListDistinct();
        for (OasisNormativeSource normativeSource : normativeSources) {
            DatabaseManager.removeObject(normativeSource);
        }

    }
}
