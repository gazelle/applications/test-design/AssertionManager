package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisInterpretation;
import net.ihe.gazelle.oasis.testassertion.OasisInterpretationQuery;

import java.util.List;

public class OasisInterpretationFactory {
    public static OasisInterpretation createInterpretationWithMandatoryFields() {
        OasisInterpretation interpretation = new OasisInterpretation();
        interpretation = (OasisInterpretation) DatabaseManager.writeObject(interpretation);
        return interpretation;
    }

    public static void cleanOasisInterpretation() {
        OasisInterpretationQuery interpretationQuery = new OasisInterpretationQuery();
        List<OasisInterpretation> interpretations = interpretationQuery.getListDistinct();
        for (OasisInterpretation interpretation : interpretations) {
            DatabaseManager.removeObject(interpretation);
        }
    }

    public static OasisInterpretation getOasisInterpretationFromDatabase(OasisInterpretation interpretation) {
        if (interpretation.getId() == null) {
            return null;
        } else {
            OasisInterpretationQuery query = new OasisInterpretationQuery();
            query.eq(interpretation);
            List<OasisInterpretation> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }
}
