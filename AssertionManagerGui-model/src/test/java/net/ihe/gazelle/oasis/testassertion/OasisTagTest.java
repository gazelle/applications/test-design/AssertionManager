package net.ihe.gazelle.oasis.testassertion;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisTagTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {

        List<OasisTag> tags = new ArrayList<OasisTag>();
        OasisTag pageTag = new OasisTag("Page");
        pageTag.setContent("0");
        tags.add(pageTag);

        OasisTag sectionTag = new OasisTag();
        tags.add(sectionTag);
        Collections.sort(tags);
        assertEquals("Page", tags.get(0).getName());
    }

    @Test
    public void test2() {

        List<OasisTag> tags = new ArrayList<OasisTag>();
        OasisTag sectionTag = new OasisTag();
        tags.add(sectionTag);

        OasisTag pageTag = new OasisTag("Page");
        pageTag.setContent("0");
        tags.add(pageTag);


        Collections.sort(tags);
        assertEquals("Page", tags.get(0).getName());
    }

}
