package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisComment;
import net.ihe.gazelle.oasis.testassertion.OasisCommentQuery;

import java.util.List;

public class OasisCommentFactory {
    public static OasisComment createCommentWithMandatoryFields() {
        OasisComment comment = new OasisComment();
        comment = (OasisComment) DatabaseManager.writeObject(comment);
        return comment;
    }

    public static void cleanOasisComment() {
        OasisCommentQuery commentQuery = new OasisCommentQuery();
        List<OasisComment> comments = commentQuery.getListDistinct();
        for (OasisComment comment : comments) {
            DatabaseManager.removeObject(comment);
        }
    }

    public static OasisComment getOasisCommentFromDatabase(OasisComment comment) {
        if (comment.getId() == null) {
            return null;
        } else {
            OasisCommentQuery query = new OasisCommentQuery();
            query.eq(comment);
            List<OasisComment> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }
}
