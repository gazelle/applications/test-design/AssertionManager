package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisVariable;
import net.ihe.gazelle.oasis.testassertion.OasisVariableQuery;

import java.util.List;

public class OasisVariableFactory {

    public static OasisVariable createVariableWithMandatoryFieldsAndSaveIt() {
        return (OasisVariable) DatabaseManager.writeObject(createVariableWithMandatoryFields());
    }

    public static OasisVariable createVariableWithMandatoryFields() {
        OasisVariable variable = new OasisVariable();
        variable.setName("variable name");
        return variable;
    }

    public static void cleanOasisVariables() {
        OasisVariableQuery variableQuery = new OasisVariableQuery();
        List<OasisVariable> variables = variableQuery.getListDistinct();
        for (OasisVariable variable : variables) {
            DatabaseManager.removeObject(variable);
        }
    }
}
