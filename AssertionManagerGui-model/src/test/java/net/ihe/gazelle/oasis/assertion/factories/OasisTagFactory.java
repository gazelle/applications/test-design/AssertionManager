package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisTag;
import net.ihe.gazelle.oasis.testassertion.OasisTagQuery;

import java.util.List;

public class OasisTagFactory {

    private static String name = "tag name";

    public static OasisTag createTagWithMandatoryFieldsAndSaveIt() {
        return (OasisTag) DatabaseManager.writeObject(createTagWithMandatoryFields());
    }

    public static OasisTag createTagWithMandatoryFields() {
        OasisTag tag = new OasisTag();
        tag.setName(name);
        return tag;
    }

    public static OasisTag getOasisTagFromDatabase(OasisTag tag) {
        if (tag.getId() == null) {
            return null;
        } else {
            OasisTagQuery query = new OasisTagQuery();
            query.eq(tag);
            List<OasisTag> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }

    public static String getName() {
        return name;
    }

    public static void cleanOasisTags() {
        OasisTagQuery tagQuery = new OasisTagQuery();
        List<OasisTag> tags = tagQuery.getListDistinct();
        for (OasisTag tag : tags) {
            DatabaseManager.removeObject(tag);
        }
    }
}
