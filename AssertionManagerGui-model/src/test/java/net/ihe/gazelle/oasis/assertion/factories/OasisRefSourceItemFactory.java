package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItemQuery;

import java.util.List;

public class OasisRefSourceItemFactory {
    public static OasisRefSourceItem createRefSourceItemWithMandatoryFields() {
        OasisRefSourceItem refSourceItem = new OasisRefSourceItem();
        refSourceItem = (OasisRefSourceItem) DatabaseManager.writeObject(refSourceItem);
        return refSourceItem;
    }

    public static void cleanOasisRefSourceItem() {
        OasisRefSourceItemQuery refSourceItemQuery = new OasisRefSourceItemQuery();
        List<OasisRefSourceItem> refSourceItems = refSourceItemQuery.getListDistinct();
        for (OasisRefSourceItem refSourceItem : refSourceItems) {
            DatabaseManager.removeObject(refSourceItem);
        }
    }

    public static OasisRefSourceItem getOasisRefSourceItemFromDatabase(OasisRefSourceItem refSourceItem) {
        if (refSourceItem.getId() == null) {
            return null;
        } else {
            OasisRefSourceItemQuery query = new OasisRefSourceItemQuery();
            query.eq(refSourceItem);
            List<OasisRefSourceItem> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }
}
