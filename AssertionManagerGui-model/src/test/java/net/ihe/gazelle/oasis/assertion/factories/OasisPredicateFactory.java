package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisLanguageValues;
import net.ihe.gazelle.oasis.testassertion.OasisPredicate;
import net.ihe.gazelle.oasis.testassertion.OasisPredicateQuery;

import java.util.List;

public class OasisPredicateFactory {

    public static OasisPredicate createPredicateWithMandatoryFieldsAndSaveIt() {
        return (OasisPredicate) DatabaseManager.writeObject(createPredicateWithMandatoryFields());
    }

    public static OasisPredicate createPredicateWithMandatoryFields() {
        OasisPredicate predicate = new OasisPredicate();
        predicate.setContent("predicate content");
        predicate.setLanguage(OasisLanguageValues.NATURAL);
        return predicate;
    }

    public static void cleanOasisPredicates() {
        OasisPredicateQuery sectionQuery = new OasisPredicateQuery();
        List<OasisPredicate> predicates = sectionQuery.getListDistinct();
        for (OasisPredicate predicate : predicates) {
            DatabaseManager.removeObject(predicate);
        }
    }
}
