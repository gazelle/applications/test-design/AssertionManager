package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import net.ihe.gazelle.oasis.testassertion.OasisTargetQuery;

import java.util.List;

public class OasisTargetFactory {

    private static int number = 1;

    public static OasisTarget createTargetWithMandatoryFields() {
        OasisTarget target = new OasisTarget();
        target.setIdScheme("testing " + number);
        number++;
        target = (OasisTarget) DatabaseManager.writeObject(target);
        return target;
    }

    public static void cleanOasisTargets() {
        OasisTargetQuery sectionQuery = new OasisTargetQuery();
        List<OasisTarget> targets = sectionQuery.getListDistinct();
        for (OasisTarget target : targets) {
            DatabaseManager.removeObject(target);
        }
    }
}
