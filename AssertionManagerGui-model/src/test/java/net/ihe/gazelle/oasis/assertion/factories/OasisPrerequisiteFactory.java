package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisLanguageValues;
import net.ihe.gazelle.oasis.testassertion.OasisPrerequisite;
import net.ihe.gazelle.oasis.testassertion.OasisPrerequisiteQuery;

import java.util.List;

public class OasisPrerequisiteFactory {

    public static OasisPrerequisite createPrerequisiteWithMandatoryFieldsAndSaveIt() {
        return (OasisPrerequisite) DatabaseManager.writeObject(createPrerequisiteWithMandatoryFields());
    }

    public static OasisPrerequisite createPrerequisiteWithMandatoryFields() {
        OasisPrerequisite prerequisite = new OasisPrerequisite();
        prerequisite.setContent("prerequisite content");
        prerequisite.setLanguage(OasisLanguageValues.NATURAL);
        return prerequisite;
    }

    public static void cleanOasisPrerequisites() {
        OasisPrerequisiteQuery prerequisiteQuery = new OasisPrerequisiteQuery();
        List<OasisPrerequisite> prerequisites = prerequisiteQuery.getListDistinct();
        for (OasisPrerequisite prerequisite : prerequisites) {
            DatabaseManager.removeObject(prerequisite);
        }
    }
}
