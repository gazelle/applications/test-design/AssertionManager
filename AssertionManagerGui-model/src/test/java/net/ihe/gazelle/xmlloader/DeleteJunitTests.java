package net.ihe.gazelle.xmlloader;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import net.ihe.gazelle.oasis.testassertion.OasisTargetQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import javax.persistence.EntityManager;
import java.util.List;
@Ignore
public class DeleteJunitTests extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void test_deleteAsserion() {
        OasisTargetQuery query = new OasisTargetQuery();
        List<OasisTarget> list = query.getList();

        OasisTarget target = list.get(0);

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.getTransaction().begin();
        entityManager.remove(target);
        entityManager.flush();

        entityManager.getTransaction().commit();
    }
}
