package net.ihe.gazelle.assertion.appliesTo.factories;


import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToActor;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToActorQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;

import java.util.List;

public class AssertionAppliesToActorFactory {

    public static AssertionAppliesToActor createAssertionAppliesToActorWithMandatoryFields(OasisTestAssertion assertion) {
        AssertionAppliesToActor actorLink = new AssertionAppliesToActor();
        actorLink.setAssertion(assertion);
        actorLink.setLinkedEntityKeyword("ACTOR_KEY");
        actorLink = (AssertionAppliesToActor) DatabaseManager.writeObject(actorLink);
        return actorLink;
    }

    public static void cleanAssertionAppliesToActor() {
        AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery();
        List<AssertionAppliesToActor> appliesToActors = query.getListDistinct();
        for (AssertionAppliesToActor item : appliesToActors) {
            DatabaseManager.removeObject(item);
        }
    }
}
