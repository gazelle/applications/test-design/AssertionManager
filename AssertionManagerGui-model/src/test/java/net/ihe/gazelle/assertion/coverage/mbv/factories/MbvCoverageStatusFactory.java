package net.ihe.gazelle.assertion.coverage.mbv.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.MbvCoverageStatus;
import net.ihe.gazelle.oasis.testassertion.MbvCoverageStatusQuery;

import java.util.List;

public class MbvCoverageStatusFactory {
    public static MbvCoverageStatus createMbvCoverageStatusFactoryWithMandatoryFields() {
        MbvCoverageStatus mbvCoverageStatus = new MbvCoverageStatus(MbvCoverageStatus.CoverageStatusEnum.Removed, "idScheme", "assertionId", MbvServiceFactory.createMbvServiceWithMandatoryFields());
        mbvCoverageStatus = (MbvCoverageStatus) DatabaseManager.writeObject(mbvCoverageStatus);
        return mbvCoverageStatus;
    }

    public static void cleanMbvCoverageStatus() {
        MbvCoverageStatusQuery mbvAssertionQuery = new MbvCoverageStatusQuery();
        List<MbvCoverageStatus> mbvAssertion = mbvAssertionQuery.getListDistinct();
        for (MbvCoverageStatus mbv : mbvAssertion) {
            DatabaseManager.removeObject(mbv);
        }
    }
}
