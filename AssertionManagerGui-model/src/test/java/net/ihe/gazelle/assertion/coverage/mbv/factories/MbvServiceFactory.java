package net.ihe.gazelle.assertion.coverage.mbv.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.MbvService;
import net.ihe.gazelle.oasis.testassertion.MbvServiceQuery;

import java.util.List;

public class MbvServiceFactory {

    public static MbvService createMbvServiceWithMandatoryFields() {
        MbvService service = new MbvService("testing service", "http://google.com");
        service = (MbvService) DatabaseManager.writeObject(service);
        return service;
    }

    public static void cleanMbvService() {
        MbvServiceQuery serviceQuery = new MbvServiceQuery();
        List<MbvService> service = serviceQuery.getListDistinct();
        for (MbvService serv : service) {
            DatabaseManager.removeObject(serv);
        }
    }
}
