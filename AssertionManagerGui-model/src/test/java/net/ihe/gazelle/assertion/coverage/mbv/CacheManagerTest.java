package net.ihe.gazelle.assertion.coverage.mbv;

import net.ihe.gazelle.oasis.testassertion.CacheManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;

public class CacheManagerTest {

    private static int CLIENTS = 8;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() throws IOException {
        CacheManager.put("key", "value");
        String string = CacheManager.get("key");
        assertEquals("value", string);
    }

    @Test
    public void cacheManagerCallThreaded() throws IOException, InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(8);
        final ArrayList<Integer> threadsCompleted = new ArrayList<Integer>();

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                CacheManager.put("key", "value2");

                String string = CacheManager.get("key");
                assertEquals("value2", string);

                threadsCompleted.add(1);
            }
        };

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                CacheManager.put("key2", "value");

                String string = CacheManager.get("key2");
                assertEquals("value", string);

                threadsCompleted.add(2);
            }
        };

        Thread t1 = new Thread(runnable1);
        Thread t2 = new Thread(runnable2);

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        System.out.println("Threads completed: " + threadsCompleted);
        Assert.assertEquals(2, threadsCompleted.size());

    }

}
