package net.ihe.gazelle.assertion.appliesTo.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToAipo;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToAipoQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;

import java.util.List;

public class AssertionAppliesToAipoFactory {

    public static AssertionAppliesToAipo createAssertionAppliesToAipoWithMandatoryFields(OasisTestAssertion assertion) {
        AssertionAppliesToAipo AipoLink = new AssertionAppliesToAipo();
        AipoLink.setAssertion(assertion);
        AipoLink.setLinkedEntityKeyword("Aipo_KEY");
        AipoLink = (AssertionAppliesToAipo) DatabaseManager.writeObject(AipoLink);
        return AipoLink;
    }

    public static void cleanAssertionAppliesToAipo() {
        AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery();
        List<AssertionAppliesToAipo> appliesToAipos = query.getListDistinct();
        for (AssertionAppliesToAipo item : appliesToAipos) {
            DatabaseManager.removeObject(item);
        }
    }
}
