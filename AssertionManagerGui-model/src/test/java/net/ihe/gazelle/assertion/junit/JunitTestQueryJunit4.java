package net.ihe.gazelle.assertion.junit;

import net.ihe.gazelle.junit.AbstractTestQueryJunit4;

/**
 * <b>Class Description : </b>JunitTestQueryJunit4<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 26/07/16
 * @class JunitTestQueryJunit4
 * @package PACKAGE_NAME
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
public class JunitTestQueryJunit4 extends AbstractTestQueryJunit4 {

    protected String getDb() {
        return "assertion-manager-junit";
    }

    /**
     * Method getHbm2ddl. Used to control how hibernate manages database schema migrations possible values are: validate, update, create, create-drop.
     *
     * @return String
     */
    protected String getHbm2ddl() {
        return "create-drop";
    }
}
