package net.ihe.gazelle.assertion.appliesTo;

import net.ihe.gazelle.assertion.appliesTo.factories.AssertionAppliesToActorFactory;
import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesTo;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToActor;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToActorQuery;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.util.List;

import static org.junit.Assert.assertEquals;
@Ignore
public class AssertionAppliesToActorTest extends JunitTestQueryJunit4 {

    private EntityManager em;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();

        em.setFlushMode(FlushModeType.COMMIT);
    }

    @After
    public void tearDown() throws Exception {
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.flush();
        cleanData();
        super.tearDown();
    }

    private void cleanData() {
        AssertionAppliesToActorFactory.cleanAssertionAppliesToActor();
        OasisTestAssertionFactory.cleanOasisTestAssertions();
    }

    @Test
    public void test() {
        AssertionAppliesToActor actorLink = new AssertionAppliesToActor();
        actorLink.setAssertion(OasisTestAssertionFactory.createTestAssertionWithMandatoryFields());
        actorLink.setLinkedEntityKeyword("ACTOR_KEY");
        actorLink.setLinkedEntityId(1);
        actorLink.setLinkedEntityName("actor name");
        actorLink.setLinkedEntityPermalink("http://gazelle.ihe.net");
        actorLink.setLinkedEntityProvider("tm");
        DatabaseManager.writeObject(actorLink);

        AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery();
        List<AssertionAppliesToActor> appliesToActors = query.getListDistinct();
        assertEquals(1, appliesToActors.size());

        AssertionAppliesToQuery query2 = new AssertionAppliesToQuery();
        List<AssertionAppliesTo> appliesTo = query2.getListDistinct();
        assertEquals(1, appliesTo.size());
    }
}
