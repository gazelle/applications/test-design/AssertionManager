package net.ihe.gazelle.assertion.coverage.mbv;

import net.ihe.gazelle.assertion.coverage.mbv.factories.MbvServiceFactory;
import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToMBV;
import net.ihe.gazelle.oasis.testassertion.MbvService;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;

import static org.junit.Assert.assertEquals;
@Ignore
public class AssertionLinkedToMBVTest extends JunitTestQueryJunit4 {

    EntityManager em;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();

        em.setFlushMode(FlushModeType.COMMIT);
    }

    @After
    public void tearDown() throws Exception {
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.flush();
        super.tearDown();
    }

    @Test
    public void createAssertionLinkedToMbv() {
        MbvService mbvService = MbvServiceFactory.createMbvServiceWithMandatoryFields();
        OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        OasisTestAssertion assertion2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();

        AssertionLinkedToMBV link = new AssertionLinkedToMBV(assertion, mbvService);
        link = (AssertionLinkedToMBV) DatabaseManager.writeObject(link);
        link = new AssertionLinkedToMBV(assertion2, mbvService);
        link = (AssertionLinkedToMBV) DatabaseManager.writeObject(link);
        DatabaseManager.update(mbvService);
        assertEquals(2, mbvService.getAssertions().size());

    }
}
