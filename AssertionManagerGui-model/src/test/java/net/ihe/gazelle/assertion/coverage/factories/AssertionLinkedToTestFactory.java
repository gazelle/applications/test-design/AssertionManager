package net.ihe.gazelle.assertion.coverage.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTest;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTestQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;

import java.util.List;

public class AssertionLinkedToTestFactory {

    public static AssertionLinkedToTest createAssertionLinkedToTestWithMandatoryFields(OasisTestAssertion assertion) {
        AssertionLinkedToTest testLink = new AssertionLinkedToTest();
        testLink.setAssertion(assertion);
        testLink.setLinkedEntityKeyword("12233");
        testLink = (AssertionLinkedToTest) DatabaseManager.writeObject(testLink);
        return testLink;
    }

    public static void cleanAssertionLinkedToTest() {
        AssertionLinkedToTestQuery testLinkQuery = new AssertionLinkedToTestQuery();
        List<AssertionLinkedToTest> testLink = testLinkQuery.getListDistinct();
        for (AssertionLinkedToTest item : testLink) {
            DatabaseManager.removeObject(item);
        }
    }
}
