package net.ihe.gazelle.assertion.coverage.mbv.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToMBV;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToMBVQuery;

import java.util.List;

public class AssertionLinkedToMBVFactory {

    public static AssertionLinkedToMBV createAssertionLinkedToMBVWithMandatoryFields() {
        AssertionLinkedToMBV mbvAssertion = new AssertionLinkedToMBV();
        mbvAssertion = (AssertionLinkedToMBV) DatabaseManager.writeObject(mbvAssertion);
        return mbvAssertion;
    }

    public static void cleanAssertionLinkedToMBV() {
        AssertionLinkedToMBVQuery mbvAssertionQuery = new AssertionLinkedToMBVQuery();
        List<AssertionLinkedToMBV> mbvAssertion = mbvAssertionQuery.getListDistinct();
        for (AssertionLinkedToMBV mbv : mbvAssertion) {
            DatabaseManager.removeObject(mbv);
        }
    }
}
