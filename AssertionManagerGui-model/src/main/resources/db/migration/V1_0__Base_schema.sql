--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: am_assertion_scope; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_assertion_scope (
    id integer NOT NULL,
    description text,
    keyword character varying(64) NOT NULL
);


ALTER TABLE public.am_assertion_scope OWNER TO gazelle;

--
-- Name: am_assertion_scope_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_assertion_scope_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_assertion_scope_id_seq OWNER TO gazelle;

--
-- Name: am_assertion_scope_to_test_assertions; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_assertion_scope_to_test_assertions (
    scope_id integer NOT NULL,
    assertion_id integer NOT NULL
);


ALTER TABLE public.am_assertion_scope_to_test_assertions OWNER TO gazelle;

--
-- Name: am_assertions_links; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_assertions_links (
    linked_entity character varying(31) NOT NULL,
    id integer NOT NULL,
    linked_entity_keyword character varying(255) NOT NULL,
    assertion_id integer NOT NULL
);


ALTER TABLE public.am_assertions_links OWNER TO gazelle;

--
-- Name: am_assertions_links_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_assertions_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_assertions_links_id_seq OWNER TO gazelle;

--
-- Name: am_assertions_links_mbv; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_assertions_links_mbv (
    id integer NOT NULL,
    assertion_id integer NOT NULL,
    mbv_service_id integer NOT NULL
);


ALTER TABLE public.am_assertions_links_mbv OWNER TO gazelle;

--
-- Name: am_mbv_service; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_mbv_service (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    url character varying(255) NOT NULL
);


ALTER TABLE public.am_mbv_service OWNER TO gazelle;

--
-- Name: am_mbv_service_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_mbv_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_mbv_service_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_comment; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_comment (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text
);


ALTER TABLE public.am_oasis_comment OWNER TO gazelle;

--
-- Name: am_oasis_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_comment_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_derived_source_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_derived_source_item (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text,
    date_string character varying(255),
    document_id text,
    name text,
    resource_provenance_id text,
    revision_id text,
    uri text,
    version_id text
);


ALTER TABLE public.am_oasis_derived_source_item OWNER TO gazelle;

--
-- Name: am_oasis_derived_source_item_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_derived_source_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_derived_source_item_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_description; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_description (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text
);


ALTER TABLE public.am_oasis_description OWNER TO gazelle;

--
-- Name: am_oasis_description_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_description_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_description_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_interpretation; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_interpretation (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text
);


ALTER TABLE public.am_oasis_interpretation OWNER TO gazelle;

--
-- Name: am_oasis_interpretation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_interpretation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_interpretation_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_normative_source; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_normative_source (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text,
    comment_id integer,
    interpretation_id integer
);


ALTER TABLE public.am_oasis_normative_source OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_derived_source_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_normative_source_derived_source_item (
    normative_source_id integer NOT NULL,
    derived_source_item_id integer NOT NULL
);


ALTER TABLE public.am_oasis_normative_source_derived_source_item OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_normative_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_normative_source_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_ref_source_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_normative_source_ref_source_item (
    normative_source_id integer NOT NULL,
    ref_source_item_id integer NOT NULL
);


ALTER TABLE public.am_oasis_normative_source_ref_source_item OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_text_source_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_normative_source_text_source_item (
    normative_source_id integer NOT NULL,
    text_source_item_id integer NOT NULL
);


ALTER TABLE public.am_oasis_normative_source_text_source_item OWNER TO gazelle;

--
-- Name: am_oasis_predicate; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_predicate (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text NOT NULL,
    language integer
);


ALTER TABLE public.am_oasis_predicate OWNER TO gazelle;

--
-- Name: am_oasis_predicate_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_predicate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_predicate_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_prerequisite; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_prerequisite (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text NOT NULL,
    language integer
);


ALTER TABLE public.am_oasis_prerequisite OWNER TO gazelle;

--
-- Name: am_oasis_prerequisite_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_prerequisite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_prerequisite_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_prescription; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_prescription (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text,
    level integer NOT NULL
);


ALTER TABLE public.am_oasis_prescription OWNER TO gazelle;

--
-- Name: am_oasis_prescription_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_prescription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_prescription_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_ref_source_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_ref_source_item (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    document_id text,
    name text,
    resource_provenance_id text,
    revision_id text,
    uri text,
    version_id text
);


ALTER TABLE public.am_oasis_ref_source_item OWNER TO gazelle;

--
-- Name: am_oasis_ref_source_item_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_ref_source_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_ref_source_item_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_tag; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_tag (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.am_oasis_tag OWNER TO gazelle;

--
-- Name: am_oasis_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_tag_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_target; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_target (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text,
    idscheme character varying(255),
    language integer,
    type text
);


ALTER TABLE public.am_oasis_target OWNER TO gazelle;

--
-- Name: am_oasis_target_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_target_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_target_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_test_assertion (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    assertion_id character varying(255) NOT NULL,
    description_id integer,
    normative_source_id integer NOT NULL,
    predicate_id integer NOT NULL,
    prerequisite_id integer,
    prescription_id integer,
    target_id integer NOT NULL,
    comment character varying(1024),
    mbv_coverage_count integer,
    tests_coverage_count integer,
    tf_rules_coverage_count integer,
    status integer
);


ALTER TABLE public.am_oasis_test_assertion OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_test_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_test_assertion_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion_tag; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_test_assertion_tag (
    test_assertion_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.am_oasis_test_assertion_tag OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion_variable; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_test_assertion_variable (
    test_assertion_id integer NOT NULL,
    variable_id integer NOT NULL
);


ALTER TABLE public.am_oasis_test_assertion_variable OWNER TO gazelle;

--
-- Name: am_oasis_text_source_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_text_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_text_source_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_text_source_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_text_source_item (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text,
    name character varying(255)
);


ALTER TABLE public.am_oasis_text_source_item OWNER TO gazelle;

--
-- Name: am_oasis_variable; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_oasis_variable (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    content text,
    language integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.am_oasis_variable OWNER TO gazelle;

--
-- Name: am_oasis_variable_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_oasis_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_variable_id_seq OWNER TO gazelle;

--
-- Name: cmn_application_preference; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE cmn_application_preference (
    id integer NOT NULL,
    class_name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    preference_name character varying(64) NOT NULL,
    preference_value character varying(512) NOT NULL
);


ALTER TABLE public.cmn_application_preference OWNER TO gazelle;

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_application_preference_id_seq OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE cmn_path_linking_a_document (
    id integer NOT NULL,
    comment character varying(255),
    path character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.cmn_path_linking_a_document OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_path_linking_a_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_path_linking_a_document_id_seq OWNER TO gazelle;

--
-- Name: am_assertion_scope_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_assertion_scope
    ADD CONSTRAINT am_assertion_scope_keyword_key UNIQUE (keyword);


--
-- Name: am_assertion_scope_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_assertion_scope
    ADD CONSTRAINT am_assertion_scope_pkey PRIMARY KEY (id);


--
-- Name: am_assertion_scope_to_test_assertions_scope_id_assertion_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_assertion_scope_to_test_assertions
    ADD CONSTRAINT am_assertion_scope_to_test_assertions_scope_id_assertion_id_key UNIQUE (scope_id, assertion_id);


--
-- Name: am_assertions_links_linked_entity_linked_entity_keyword_ass_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_assertions_links
    ADD CONSTRAINT am_assertions_links_linked_entity_linked_entity_keyword_ass_key UNIQUE (linked_entity, linked_entity_keyword, assertion_id);


--
-- Name: am_assertions_links_mbv_assertion_id_mbv_service_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_assertions_links_mbv
    ADD CONSTRAINT am_assertions_links_mbv_assertion_id_mbv_service_id_key UNIQUE (assertion_id, mbv_service_id);


--
-- Name: am_assertions_links_mbv_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_assertions_links_mbv
    ADD CONSTRAINT am_assertions_links_mbv_pkey PRIMARY KEY (id);


--
-- Name: am_assertions_links_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_assertions_links
    ADD CONSTRAINT am_assertions_links_pkey PRIMARY KEY (id);


--
-- Name: am_mbv_service_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_mbv_service
    ADD CONSTRAINT am_mbv_service_name_key UNIQUE (name);


--
-- Name: am_mbv_service_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_mbv_service
    ADD CONSTRAINT am_mbv_service_pkey PRIMARY KEY (id);


--
-- Name: am_mbv_service_url_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_mbv_service
    ADD CONSTRAINT am_mbv_service_url_key UNIQUE (url);


--
-- Name: am_oasis_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_comment
    ADD CONSTRAINT am_oasis_comment_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_derived_source_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_derived_source_item
    ADD CONSTRAINT am_oasis_derived_source_item_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_description
    ADD CONSTRAINT am_oasis_description_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_interpretation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_interpretation
    ADD CONSTRAINT am_oasis_interpretation_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_normative_source_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_normative_source
    ADD CONSTRAINT am_oasis_normative_source_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_predicate_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_predicate
    ADD CONSTRAINT am_oasis_predicate_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_prerequisite_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_prerequisite
    ADD CONSTRAINT am_oasis_prerequisite_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_prescription_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_prescription
    ADD CONSTRAINT am_oasis_prescription_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_ref_source_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_ref_source_item
    ADD CONSTRAINT am_oasis_ref_source_item_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_tag
    ADD CONSTRAINT am_oasis_tag_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_target_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_target
    ADD CONSTRAINT am_oasis_target_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_test_assertion_assertion_id_target_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT am_oasis_test_assertion_assertion_id_target_id_key UNIQUE (assertion_id, target_id);


--
-- Name: am_oasis_test_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT am_oasis_test_assertion_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_text_source_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_text_source_item
    ADD CONSTRAINT am_oasis_text_source_item_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_variable_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_oasis_variable
    ADD CONSTRAINT am_oasis_variable_pkey PRIMARY KEY (id);


--
-- Name: cmn_application_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_pkey PRIMARY KEY (id);


--
-- Name: cmn_application_preference_preference_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_preference_name_key UNIQUE (preference_name);


--
-- Name: cmn_path_linking_a_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY cmn_path_linking_a_document
    ADD CONSTRAINT cmn_path_linking_a_document_pkey PRIMARY KEY (id);


--
-- Name: fk1e40a915bacacc05; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_assertion_scope_to_test_assertions
    ADD CONSTRAINT fk1e40a915bacacc05 FOREIGN KEY (assertion_id) REFERENCES am_oasis_test_assertion(id);


--
-- Name: fk1e40a915d49756e5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_assertion_scope_to_test_assertions
    ADD CONSTRAINT fk1e40a915d49756e5 FOREIGN KEY (scope_id) REFERENCES am_assertion_scope(id);


--
-- Name: fk34774f86bcf5517; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source
    ADD CONSTRAINT fk34774f86bcf5517 FOREIGN KEY (comment_id) REFERENCES am_oasis_comment(id);


--
-- Name: fk34774f8ea3f225d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source
    ADD CONSTRAINT fk34774f8ea3f225d FOREIGN KEY (interpretation_id) REFERENCES am_oasis_interpretation(id);


--
-- Name: fk527ed3977eff6e92; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion_tag
    ADD CONSTRAINT fk527ed3977eff6e92 FOREIGN KEY (test_assertion_id) REFERENCES am_oasis_test_assertion(id);


--
-- Name: fk527ed397aeadedb7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion_tag
    ADD CONSTRAINT fk527ed397aeadedb7 FOREIGN KEY (tag_id) REFERENCES am_oasis_tag(id);


--
-- Name: fk63d4675ebacacc05; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_assertions_links
    ADD CONSTRAINT fk63d4675ebacacc05 FOREIGN KEY (assertion_id) REFERENCES am_oasis_test_assertion(id);


--
-- Name: fk65d651c4589d978d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source_ref_source_item
    ADD CONSTRAINT fk65d651c4589d978d FOREIGN KEY (ref_source_item_id) REFERENCES am_oasis_ref_source_item(id);


--
-- Name: fk65d651c4afb2664a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source_ref_source_item
    ADD CONSTRAINT fk65d651c4afb2664a FOREIGN KEY (normative_source_id) REFERENCES am_oasis_normative_source(id);


--
-- Name: fk84f5b49f7eff6e92; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion_variable
    ADD CONSTRAINT fk84f5b49f7eff6e92 FOREIGN KEY (test_assertion_id) REFERENCES am_oasis_test_assertion(id);


--
-- Name: fk84f5b49ff5cd941d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion_variable
    ADD CONSTRAINT fk84f5b49ff5cd941d FOREIGN KEY (variable_id) REFERENCES am_oasis_variable(id);


--
-- Name: fkaa25d201569506c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_assertions_links_mbv
    ADD CONSTRAINT fkaa25d201569506c FOREIGN KEY (mbv_service_id) REFERENCES am_mbv_service(id);


--
-- Name: fkaa25d20bacacc05; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_assertions_links_mbv
    ADD CONSTRAINT fkaa25d20bacacc05 FOREIGN KEY (assertion_id) REFERENCES am_oasis_test_assertion(id);


--
-- Name: fkd199a2bc351d1ddd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT fkd199a2bc351d1ddd FOREIGN KEY (prescription_id) REFERENCES am_oasis_prescription(id);


--
-- Name: fkd199a2bc426b7ad7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT fkd199a2bc426b7ad7 FOREIGN KEY (predicate_id) REFERENCES am_oasis_predicate(id);


--
-- Name: fkd199a2bc6503485d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT fkd199a2bc6503485d FOREIGN KEY (prerequisite_id) REFERENCES am_oasis_prerequisite(id);


--
-- Name: fkd199a2bcafb2664a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT fkd199a2bcafb2664a FOREIGN KEY (normative_source_id) REFERENCES am_oasis_normative_source(id);


--
-- Name: fkd199a2bcdfe86d77; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT fkd199a2bcdfe86d77 FOREIGN KEY (description_id) REFERENCES am_oasis_description(id);


--
-- Name: fkd199a2bcf5a45cbd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_test_assertion
    ADD CONSTRAINT fkd199a2bcf5a45cbd FOREIGN KEY (target_id) REFERENCES am_oasis_target(id);


--
-- Name: fkef852bceafb2664a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source_derived_source_item
    ADD CONSTRAINT fkef852bceafb2664a FOREIGN KEY (normative_source_id) REFERENCES am_oasis_normative_source(id);


--
-- Name: fkef852bcec42e674d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source_derived_source_item
    ADD CONSTRAINT fkef852bcec42e674d FOREIGN KEY (derived_source_item_id) REFERENCES am_oasis_derived_source_item(id);


--
-- Name: fkfd46690c31d47373; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source_text_source_item
    ADD CONSTRAINT fkfd46690c31d47373 FOREIGN KEY (text_source_item_id) REFERENCES am_oasis_text_source_item(id);


--
-- Name: fkfd46690cafb2664a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_oasis_normative_source_text_source_item
    ADD CONSTRAINT fkfd46690cafb2664a FOREIGN KEY (normative_source_id) REFERENCES am_oasis_normative_source(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

