package net.ihe.gazelle.assertion.tags;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum TagKeywords {

    Figure("Figure"),
    Section("Section"),
    Table("Table");

    private String friendlyName;

    TagKeywords(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public static List<String> getTagFriendlyNames() {
        final List<TagKeywords> tagKeywords = Arrays.asList(TagKeywords.values());
        final List<String> result = new ArrayList<String>();
        for (TagKeywords tagKeyword : tagKeywords) {
            result.add(tagKeyword.getFriendlyName());
        }
        return result;
    }

    public static SelectItem[] getTagkeywords() {
        final List<TagKeywords> tagKeywords = Arrays.asList(TagKeywords.values());
        final List<SelectItem> result = new ArrayList<SelectItem>();
        for (TagKeywords tagKeyword : tagKeywords) {
            result.add(new SelectItem(tagKeyword.getFriendlyName(), tagKeyword.getFriendlyName() + "*"));
        }
        return result.toArray(new SelectItem[result.size()]);
    }
}
