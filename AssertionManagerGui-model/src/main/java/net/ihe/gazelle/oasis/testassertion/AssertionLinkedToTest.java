package net.ihe.gazelle.oasis.testassertion;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = AssertionLinkedToTest.DB_DISCRIMINATOR)
public class AssertionLinkedToTest extends AbstractAssertionLink implements Serializable {

    public static final String DB_DISCRIMINATOR = "TEST";
    private static final long serialVersionUID = -7444867375667066922L;

    public AssertionLinkedToTest(String entityKeyword, OasisTestAssertion assertion, String linkedEntityProvider,
                                 String linkedEntityPermalink, String linkedEntityName) {
        super(entityKeyword, assertion, linkedEntityProvider, linkedEntityPermalink, linkedEntityName);
    }

    public AssertionLinkedToTest() {
        super();
    }


}
