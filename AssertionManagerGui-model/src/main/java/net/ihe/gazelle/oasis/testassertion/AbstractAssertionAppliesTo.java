package net.ihe.gazelle.oasis.testassertion;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractAssertionAppliesTo extends AssertionAppliesTo {

    public AbstractAssertionAppliesTo(String entityKeyword, String entityName, int entityId, String entityProvider, String permalink, OasisTestAssertion assertion) {
        super();
        super.setLinkedEntityKeyword(entityKeyword);
        this.assertion = assertion;
        super.setLinkedEntityId(entityId);
        super.setLinkedEntityProvider(entityProvider);
        super.setLinkedEntityName(entityName);
        super.setLinkedEntityPermalink(permalink);
    }

    public AbstractAssertionAppliesTo() {
        super();
    }

    /**
     * Field assertion.
     */
    @ManyToOne
    @JoinColumn(name = "assertion_id", nullable = false)
    private OasisTestAssertion assertion;

    /**
     * Method getAssertion.
     *
     * @return OasisTestAssertion
     */
    public OasisTestAssertion getAssertion() {
        return assertion;
    }

    /**
     * Method setAssertion.
     *
     * @param assertion OasisTestAssertion
     */
    public void setAssertion(OasisTestAssertion assertion) {
        this.assertion = assertion;
    }

}
