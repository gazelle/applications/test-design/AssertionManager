package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;

@Name("assertionsLinks")
@Entity
@Audited
@Table(name = "am_assertions_links_mbv", uniqueConstraints = @UniqueConstraint(columnNames = {"assertion_id",
        "mbv_service_id"}))
@SequenceGenerator(name = "am_assertions_links_mbv_sequence", sequenceName = "am_assertions_links_mbv_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
public class AssertionLinkedToMBV implements java.io.Serializable {


    private static final long serialVersionUID = 5209794122553563657L;

    public AssertionLinkedToMBV(OasisTestAssertion assertion, MbvService mbvService) {
        super();
        this.assertion = assertion;
        this.mbvService = mbvService;
    }

    public AssertionLinkedToMBV() {
        super();
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_assertions_links_mbv_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "assertion_id", nullable = false)
    private OasisTestAssertion assertion;

    @ManyToOne
    @JoinColumn(name = "mbv_service_id", nullable = false)
    private MbvService mbvService;

    public OasisTestAssertion getAssertion() {
        return assertion;
    }

    public MbvService getMbvService() {
        return mbvService;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
