package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToTransactionQuery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
@DiscriminatorValue(value = AssertionAppliesToTransaction.DB_DISCRIMINATOR)
public class AssertionAppliesToTransaction extends AbstractAssertionAppliesTo implements Serializable {

    public static final String DB_DISCRIMINATOR = "TF_TRANSACTION";
    private static final long serialVersionUID = -5520706199028539145L;

    public AssertionAppliesToTransaction(String entityKeyword, String entityName, int entityId, String entityProvider,
                                         String permalink, OasisTestAssertion assertion) {
        super(entityKeyword, entityName, entityId, entityProvider, permalink, assertion);
    }

    public AssertionAppliesToTransaction() {
        super();
    }

    public List<AssertionAppliesToTransaction> getAssertionsLinked() {
        AssertionAppliesToTransactionQuery query = new AssertionAppliesToTransactionQuery();
        query.linkedEntityKeyword().eq(this.getLinkedEntityKeyword());
        query.id().neq(this.getId());
        return query.getList();
    }
}
