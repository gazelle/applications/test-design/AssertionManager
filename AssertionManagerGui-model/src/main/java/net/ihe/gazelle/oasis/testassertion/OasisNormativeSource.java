package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.hql.HibernateHelper;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Audited
@Name("oasisNormativeSource")
@Table(name = "am_oasis_normative_source")
@SequenceGenerator(name = "am_oasis_normative_source_sequence", sequenceName = "am_oasis_normative_source_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = OasisNormativeSource.INSTANCES)
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisNormativeSource extends GazellePersiste implements java.io.Serializable {


    private static final long serialVersionUID = 9058698524111313634L;
    public static final String INSTANCES = "instances";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_normative_source_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "content")
    @Lob
    @Type(type = "text")
    private String content;

    @ManyToOne
    @JoinColumn(name = "comment_id")
    private OasisComment comment;

    @ManyToOne
    @JoinColumn(name = "interpretation_id")
    private OasisInterpretation interpretation;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "am_oasis_normative_source_ref_source_item", joinColumns = {@JoinColumn(name = "normative_source_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "ref_source_item_id", referencedColumnName = "id")})

    @XmlElement(name = "refSourceItem")
    private List<OasisRefSourceItem> refSourceItems;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "am_oasis_normative_source_text_source_item", joinColumns = {@JoinColumn(name = "normative_source_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "text_source_item_id", referencedColumnName = "id")})

    private List<OasisTextSourceItem> textSourceItems;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "am_oasis_normative_source_derived_source_item", joinColumns = {@JoinColumn(name = "normative_source_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "derived_source_item_id", referencedColumnName = "id")})

    private List<OasisDerivedSourceItem> derivedSourceItems;

    @OneToMany(mappedBy = "normativeSource")
    @XmlTransient
    private List<OasisTestAssertion> testAssertions;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public OasisComment getComment() {
        return this.comment;
    }

    public void setComment(OasisComment comment) {
        this.comment = comment;
    }

    public OasisInterpretation getInterpretation() {
        return this.interpretation;
    }

    public void setInterpretation(OasisInterpretation interpretation) {
        this.interpretation = interpretation;
    }

    public List<OasisRefSourceItem> getRefSourceItems() {
        return HibernateHelper.getLazyValue(this, "refSourceItems", this.refSourceItems);
    }

    public void setRefSourceItems(List<OasisRefSourceItem> refSourceItems) {
        this.refSourceItems = refSourceItems;
    }

    public List<OasisTextSourceItem> getTextSourceItems() {
        return HibernateHelper.getLazyValue(this, "textSourceItems", this.textSourceItems);
    }

    public void setTextSourceItems(List<OasisTextSourceItem> textSourceItems) {
        this.textSourceItems = textSourceItems;
    }

    public List<OasisDerivedSourceItem> getDerivedSourceItems() {
        return HibernateHelper.getLazyValue(this, "derivedSourceItems", this.derivedSourceItems);
    }

    public void setDerivedSourceItems(List<OasisDerivedSourceItem> derivedSourceItems) {
        this.derivedSourceItems = derivedSourceItems;
    }

    public List<OasisTestAssertion> getTestAssertions() {
        return HibernateHelper.getLazyValue(this, "testAssertions", this.testAssertions);
    }

    public void setTestAssertions(List<OasisTestAssertion> testAssertions) {
        this.testAssertions = testAssertions;
    }

    public void addRefSourceItem(OasisRefSourceItem refSourceItem) {
        this.refSourceItems = new ArrayList<OasisRefSourceItem>();
        this.refSourceItems.add(refSourceItem);
    }

    public void removeRefSourceItem(OasisRefSourceItem refSourceItem) {
        this.getRefSourceItems().remove(refSourceItem);
    }

    public boolean isUsed() {
        boolean result = true;
        if ((testAssertions == null || testAssertions.size() == 0) && (refSourceItems == null || refSourceItems.size() == 0)) {
            result = false;
        }

        return result;
    }

    public static void markAssertionsForReview(EntityManager entityManager, OasisRefSourceItem refSourceItem) {
        List<OasisNormativeSource> normativeSources = refSourceItem.getNormativeSources();
        if (normativeSources != null) {
            for (OasisNormativeSource oasisNormativeSource : normativeSources) {
                OasisTestAssertion.markAssertionsForReview(entityManager, oasisNormativeSource);
            }
        }
        entityManager.flush();
    }

    public static void unlinkNormativeSources(EntityManager entityManager, OasisRefSourceItem refSourceItem) {
        if (refSourceItem != null) {
            List<OasisNormativeSource> normativeSources = refSourceItem.getNormativeSources();
            if (normativeSources != null) {
                for (OasisNormativeSource normativeSource : normativeSources) {
                    normativeSource.removeRefSourceItem(refSourceItem);
                    OasisNormativeSource normativeSourceUpdated = entityManager.merge(normativeSource);
                    deleteUnused(entityManager, normativeSourceUpdated);
                }
            }
            entityManager.flush();
        }
    }

    private static void deleteUnused(EntityManager entityManager, OasisNormativeSource normativeSourceUpdated) {
        if (!normativeSourceUpdated.isUsed()) {
            entityManager.remove(normativeSourceUpdated);
        }
    }

    public void linkToDocument(EntityManager em, List<OasisRefSourceItem> sources) {
        setRefSourceItems(sources);
        em.merge(this);
    }

    public static String getIdSchemeNormativeSourceUri(String idSchemeIn) {
        OasisNormativeSourceQuery query = new OasisNormativeSourceQuery();
        query.testAssertions().target().idScheme().eq(idSchemeIn);
        List<OasisNormativeSource> listDistinct = query.getListDistinct();
        String result = "";
        if (listDistinct.size() == 0) {
            result = "";
        } else {
            OasisNormativeSource normativeSource = listDistinct.get(0);
            if (normativeSource.getRefSourceItems().size() > 0) {
                result = normativeSource.getRefSourceItems().get(0).getUri();
            }
            if (result == null) {
                result = "";
            }
        }
        return result;
    }

}
