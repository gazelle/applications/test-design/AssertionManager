package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.common.model.AuditedObject;

import javax.persistence.EntityManager;

public abstract class GazellePersiste extends AuditedObject {

    public Object save(EntityManager em) {
        Object obj = em.merge(this);
        em.flush();
        return obj;
    }
}
