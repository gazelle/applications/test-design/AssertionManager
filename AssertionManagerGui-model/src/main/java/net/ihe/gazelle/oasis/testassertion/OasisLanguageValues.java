package net.ihe.gazelle.oasis.testassertion;

public enum OasisLanguageValues {

    NATURAL("natural language"), Java("Java");

    private String friendlyName;

    OasisLanguageValues(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}