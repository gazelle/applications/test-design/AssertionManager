package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;

@Entity
@Audited
@Name("oasisPredicate")
@Table(name = "am_oasis_predicate")
@SequenceGenerator(name = "am_oasis_predicate_sequence", sequenceName = "am_oasis_predicate_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisPredicate implements java.io.Serializable {


    private static final long serialVersionUID = 6632349669056703396L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_predicate_sequence")
    @XmlTransient
    private Integer id;

    @NotNull
    @Column(name = "content")
    @Lob
    @Type(type = "text")
    @XmlValue
    private String content;

    @Column(name = "language")
    @XmlAttribute(name = "lg")
    private OasisLanguageValues language;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public OasisLanguageValues getLanguage() {
        return this.language;
    }

    public void setLanguage(OasisLanguageValues language) {
        this.language = language;
    }


    public Object save(EntityManager em) {
        Object obj = em.merge(this);
        em.flush();
        return obj;
    }
}
