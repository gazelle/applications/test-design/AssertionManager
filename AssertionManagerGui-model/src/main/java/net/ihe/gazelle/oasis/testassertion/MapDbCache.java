package net.ihe.gazelle.oasis.testassertion;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.util.concurrent.ConcurrentNavigableMap;

public final class MapDbCache {
    private static ConcurrentNavigableMap<String, String> treeMap;
    private static DB db;

    private MapDbCache() {
        db = DBMaker.newMemoryDB().closeOnJvmShutdown().make();
        treeMap = db.getTreeMap("progress");
    }

    public static DB getInstance() {
        if (db == null) {
            new MapDbCache();
        }
        return db;
    }

    public static ConcurrentNavigableMap<String, String> getCache() {
        if (treeMap == null) {
            new MapDbCache();
        }
        return treeMap;
    }


    public void close() {
        getInstance().close();
    }
}
