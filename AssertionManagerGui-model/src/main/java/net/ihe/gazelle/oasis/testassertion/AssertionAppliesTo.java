package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 */
@Entity
@Audited
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "am_assertions_applies_to", uniqueConstraints = @UniqueConstraint(columnNames = {"linked_entity", "linked_entity_keyword", "assertion_id"}))
@SequenceGenerator(name = "am_assertions_applies_to_sequence", sequenceName = "am_assertions_applies_to_id_seq", allocationSize = 1)
@DiscriminatorColumn(name = "linked_entity")
@DiscriminatorOptions(force = true)
public abstract class AssertionAppliesTo {

    /**
     * Constructor for AssertionAppliesTo.
     */
    public AssertionAppliesTo() {
        super();
    }

    /**
     * Field id.
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_assertions_applies_to_sequence")
    private Integer id;

    /**
     * Field linkedEntityKeyword.
     */
    @Column(name = "linked_entity_keyword", nullable = false)
    private String linkedEntityKeyword;

    @Column(name = "linked_entity_name", nullable = false)
    private String linkedEntityName;

    @Column(name = "linked_entity_id", nullable = false)
    private Integer linkedEntityId;

    @Column(name = "linked_entity_provider", nullable = false)
    private String linkedEntityProvider;

    @Column(name = "linked_entity_permalink", nullable = false)
    private String linkedEntityPermalink;

    @Column(name = "linked_entity", insertable = false, updatable = false)
    private String linkedEntity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLinkedEntityKeyword() {
        return linkedEntityKeyword;
    }

    public void setLinkedEntityKeyword(String entityKeyword) {
        this.linkedEntityKeyword = entityKeyword;
    }

    public Integer getLinkedEntityId() {
        return linkedEntityId;
    }

    public void setLinkedEntityId(Integer linkedEntityId) {
        this.linkedEntityId = linkedEntityId;
    }

    public String getLinkedEntityProvider() {
        return linkedEntityProvider;
    }

    public void setLinkedEntityProvider(String linkedEntityProvider) {
        this.linkedEntityProvider = linkedEntityProvider;
    }

    /**
     * Method getAssertion.
     *
     * @return OasisTestAssertion
     */
    abstract OasisTestAssertion getAssertion();

    /**
     * Method setAssertion.
     *
     * @param assertion OasisTestAssertion
     */
    abstract void setAssertion(OasisTestAssertion assertion);

    public String getLinkedEntityName() {
        return linkedEntityName;
    }

    public void setLinkedEntityName(String linkedEntityName) {
        this.linkedEntityName = linkedEntityName;
    }

    public String getLinkedEntityPermalink() {
        return linkedEntityPermalink;
    }

    public void setLinkedEntityPermalink(String linkedEntityPermalink) {
        this.linkedEntityPermalink = linkedEntityPermalink;
    }

    public String getLinkedEntity() {
        return linkedEntity;
    }

    public void setLinkedEntity(String linkedEntity) {
        this.linkedEntity = linkedEntity;
    }
}
