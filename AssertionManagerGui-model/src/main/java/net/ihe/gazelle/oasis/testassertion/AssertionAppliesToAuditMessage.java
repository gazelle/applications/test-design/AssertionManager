package net.ihe.gazelle.oasis.testassertion;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = AssertionAppliesToAuditMessage.DB_DISCRIMINATOR)
public class AssertionAppliesToAuditMessage extends AbstractAssertionAppliesTo {

    public static final String DB_DISCRIMINATOR = "TF_AUDIT_MESSAGE";

    public AssertionAppliesToAuditMessage(String entityKeyword, String entityName, int entityId, String entityProvider, String permalink, OasisTestAssertion assertion) {
        super(entityKeyword, entityName, entityId, entityProvider, permalink, assertion);
    }

    public AssertionAppliesToAuditMessage() {
        super();
    }

    public List<AssertionAppliesToAuditMessage> getAssertionsLinked() {
        AssertionAppliesToAuditMessageQuery query = new AssertionAppliesToAuditMessageQuery();
        query.linkedEntityKeyword().eq(this.getLinkedEntityKeyword());
        query.id().neq(this.getId());
        return query.getList();
    }
}
