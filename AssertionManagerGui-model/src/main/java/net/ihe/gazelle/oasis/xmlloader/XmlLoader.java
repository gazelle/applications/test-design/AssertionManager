package net.ihe.gazelle.oasis.xmlloader;

import net.ihe.gazelle.oasis.testassertion.*;
import net.ihe.gazelle.oasis.xsd.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.persistence.EntityManager;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("restriction")
public class XmlLoader {
    private final Listener listener;
    private OasisNormativeSource globalNormativeSources = null;
    private String loggedInUser;
    private OasisTarget globalTarget = null;

    private static final Logger LOG = LoggerFactory.getLogger(XmlLoader.class);
    private EntityManager entityManager;

    public XmlLoader(EntityManager em, Listener listener, String loggedInUser) {
        this.entityManager = em;
        this.listener = listener;
        this.loggedInUser = loggedInUser;
    }

    public void load(String dump) throws JAXBException, SAXException {
        TestAssertionSetType testAssertionSet = unmarshallDocument(dump);

        processXml(testAssertionSet);
        entityManager.refresh(globalTarget);
        globalTarget.updateCoverageCounts(entityManager);
    }

    private TestAssertionSetType unmarshallDocument(String dump) throws JAXBException, SAXException {
        JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.oasis.xsd");
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        URL xsdUrl = XmlLoader.class.getResource("/testAssertionMarkupLanguage.xsd");

        Schema schema = schemaFactory.newSchema(xsdUrl);
        unmarshaller.setSchema(schema);

        return (TestAssertionSetType) ((JAXBElement) unmarshaller
                .unmarshal(new StringReader(dump))).getValue();
    }

    private void processXml(TestAssertionSetType testAssertionSet) {
        LOG.info("Starting process_xml");
        CommonType commonType = testAssertionSet.getCommon();
        loadCommons(commonType);
        List<TestAssertionType> testAssertionsType = testAssertionSet.getTestAssertion();

        /*
         * TestAssertionRefList not used on purpose
         * They will be loaded when the xml containing them will be processed
         **/

        loadTestAssertion(testAssertionsType);
        LOG.info("Ending process_xml");
    }

    @SuppressWarnings("rawtypes")
    private void loadCommons(CommonType commonType) {
        if (commonType != null) {
            List<Serializable> contents = commonType.getContent();

            JAXBElement contentCast = null;
            String nodeName = null;
            // load target
            for (Object content : contents) {
                if (content instanceof JAXBElement) {
                    contentCast = (JAXBElement) content;
                    nodeName = contentCast.getDeclaredType().getName();
                    if (nodeName.equals("net.ihe.gazelle.oasis.xsd.TargetType")) {
                        loadCommonTarget((TargetType) contentCast.getValue());
                    }

                    nodeName = null;
                    contentCast = null;
                }
            }
            // load normative source
            for (Object content : contents) {
                if (content instanceof JAXBElement) {
                    contentCast = (JAXBElement) content;
                    nodeName = contentCast.getDeclaredType().getName();
                    if (nodeName.equals("net.ihe.gazelle.oasis.xsd.NormativeSourceType")) {
                        globalNormativeSources = loadNormativeSource((NormativeSourceType) contentCast.getValue());
                    }

                    nodeName = null;
                    contentCast = null;
                }
            }
        }
    }

    private void loadCommonTarget(TargetType targetType) {
        if (targetType != null) {
            OasisTargetQuery query = new OasisTargetQuery(entityManager);
            query.idScheme().eq(targetType.getIdscheme());
            OasisTarget target = query.getUniqueResult();
            if (target == null) {
                globalTarget = new OasisTarget();
                globalTarget.setIdScheme(targetType.getIdscheme());
                globalTarget.setContent(targetType.getDescriptionScheme());

                globalTarget = (OasisTarget) globalTarget.save(entityManager);
            } else {
                globalTarget = target;
            }
        }
    }

    private OasisNormativeSource loadNormativeSource(NormativeSourceType normativeSourceType) {
        OasisNormativeSource normativeSource = null;
        if (globalNormativeSources == null) {
            if (normativeSourceType != null) {
                normativeSource = new OasisNormativeSource();
                List<Object> content = normativeSourceType.getContent();
                for (Object obj : content) {
                    if (obj instanceof JAXBElement) {
                        JAXBElement objType = (JAXBElement) obj;
                        if (objType.getDeclaredType().getName()
                                .equals("net.ihe.gazelle.oasis.xsd.RefSourceItemType")) {

                            OasisRefSourceItem refSourceItem = loadRefSourceItem((RefSourceItemType) objType.getValue());
                            if ((refSourceItem.getNormativeSources() == null) || refSourceItem.getNormativeSources()
                                    .isEmpty()) {
                                normativeSource.addRefSourceItem(refSourceItem);
                                normativeSource = (OasisNormativeSource) normativeSource.save(entityManager);
                            } else {
                                if (refSourceItem.getNormativeSources().size() > 1) {
                                    normativeSource = refSourceItem.getNormativeSources()
                                            .get(refSourceItem.getNormativeSources().size() - 2);
                                } else {
                                    normativeSource = refSourceItem.getNormativeSources().get(0);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            normativeSource = globalNormativeSources;
        }
        return normativeSource;
    }

    private OasisRefSourceItem loadRefSourceItem(RefSourceItemType input) {
        OasisRefSourceItem refSourceItem = null;
        if (input != null) {

            // search existing refSourceItem
            OasisRefSourceItemQuery query = new OasisRefSourceItemQuery(entityManager);

            query.name().eq(input.getSrcname());
            query.revisionId().eq(input.getRevisionId());
            query.uri().eq(input.getUri());
            List<OasisRefSourceItem> oasisRefSourceItems = query.getListDistinct();

            if (oasisRefSourceItems.size() > 0) {
                refSourceItem = oasisRefSourceItems.get(0);
            } else {
                refSourceItem = new OasisRefSourceItem();
                refSourceItem.setDocumentId(input.getDocumentId());
                refSourceItem.setName(input.getSrcname());
                refSourceItem.setResourceProvenanceId(input.getResourceProvenanceId());

                refSourceItem.setRevisionId(input.getRevisionId());
                refSourceItem.setUri(input.getUri());
                refSourceItem.setVersionId(input.getVersionId());

                refSourceItem = (OasisRefSourceItem) refSourceItem.save(entityManager);
            }
        }
        return refSourceItem;
    }

    private void loadTestAssertion(List<TestAssertionType> testAssertionsType) {

        int total = testAssertionsType.size();
        int i = 0;
        for (TestAssertionType testAssertionType : testAssertionsType) {
            listener.send("Importing: " + testAssertionType.getId() + ", " + i + "/" + total);
            i++;
            LOG.info("Starting loadTestAssertion " + testAssertionType.getId());
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(entityManager);
            query.assertionId().eq(testAssertionType.getId());
            String idScheme = loadIdScheme(testAssertionType);
            query.target().idScheme().eq(idScheme);
            OasisTestAssertion assertion = query.getUniqueResult();

            if (assertion == null) {
                createNewAssertion(testAssertionType);
            } else {
                updateExistingAssertion(testAssertionType, assertion);
            }
            LOG.info("Ending loadTestAssertion " + testAssertionType.getId());
        }
        listener.sendFinalMessage(i + "/" + total + " Imported");
    }

    private String loadIdScheme(TestAssertionType testAssertionType) {
        String idScheme = "";
        if (testAssertionType.getTarget() != null) {
            idScheme = testAssertionType.getTarget().getIdscheme();
        } else {
            idScheme = getCommonTarget().getIdScheme();
        }
        return idScheme;
    }

    private void updateExistingAssertion(TestAssertionType testAssertionType, OasisTestAssertion assertion) {
        OasisPredicate predicate = loadPredicate(testAssertionType.getPredicate());
        OasisPrescription prescription = loadPrescription(testAssertionType.getPrescription());
        List<OasisTag> tags = loadTags(testAssertionType.getTag());
        OasisDescription description = loadDescription(testAssertionType.getDescription());
        OasisNormativeSource normativeSourceLocal = loadNormativeSource(
                testAssertionType.getNormativeSource());
        String comment = loadComment(testAssertionType);
        assertion.setAssertionId(testAssertionType.getId());
        assertion.setDescription(description);
        assertion.setNormativeSource(normativeSourceLocal);
        assertion.setTags(tags);
        assertion.setPredicate(predicate);
        assertion.setPrescription(prescription);
        assertion.setComment(comment);
        assertion.setTestable(getTestableValue(testAssertionType));
        assertion.setLastModifierId(loggedInUser);
        entityManager.merge(assertion);
    }

    private Boolean getTestableValue(TestAssertionType testAssertionType) {
        boolean testable = true;
        if (testAssertionType.getTestable() != null && testAssertionType.getTestable().getContent().size() > 0) {
            testable = Boolean.valueOf(testAssertionType.getTestable().getContent().get(0).toString());
        }
        return testable;
    }

    private String loadComment(TestAssertionType testAssertionType) {
        String comment = "";
        if (testAssertionType.getComment() != null && testAssertionType.getComment().getContent().size() > 0) {
            comment = (String) testAssertionType.getComment().getContent().get(0);
        }
        return comment;
    }

    private void createNewAssertion(TestAssertionType testAssertionType) {
        OasisPredicate predicate = loadPredicate(testAssertionType.getPredicate());
        OasisPrescription prescription = loadPrescription(testAssertionType.getPrescription());
        List<OasisTag> tags = loadTags(testAssertionType.getTag());
        OasisTarget target = loadTarget(testAssertionType.getTarget());
        OasisDescription description = loadDescription(testAssertionType.getDescription());
        OasisNormativeSource normativeSourceLocal = loadNormativeSource(
                testAssertionType.getNormativeSource());
        String comment = loadComment(testAssertionType);
        OasisTestAssertion testAssertion = new OasisTestAssertion();

        testAssertion.setAssertionId(testAssertionType.getId());
        testAssertion.setDescription(description);
        testAssertion.setNormativeSource(normativeSourceLocal);
        testAssertion.setTarget(target);
        testAssertion.setTags(tags);
        testAssertion.setPredicate(predicate);
        testAssertion.setPrescription(prescription);
        testAssertion.setStatus(TestAssertionStatus.REVIEWED);
        testAssertion.setComment(comment);
        testAssertion.setLastModifierId(loggedInUser);
        testAssertion.setTestable(getTestableValue(testAssertionType));

        testAssertion.save(entityManager);
    }

    private OasisDescription loadDescription(DescriptionType descriptionType) {
        OasisDescription description = null;
        if (descriptionType != null) {
            description = new OasisDescription();
            description.setContent(descriptionType.getContent().get(0).toString());
            description = (OasisDescription) description.save(entityManager);
        }

        return description;
    }

    private OasisTarget loadTarget(TargetType targetType) {
        OasisTarget newTarget = null;
        if (targetType != null) {
            OasisTargetQuery query = new OasisTargetQuery();
            query.idScheme().eq(targetType.getIdscheme());
            OasisTarget existingTarget = query.getUniqueResult();
            if (existingTarget == null) {
                newTarget = newTarget(targetType);
            } else {
                newTarget = existingTarget;
            }
        } else {
            newTarget = getCommonTarget();
        }
        return newTarget;
    }

    private OasisTarget newTarget(TargetType targetType) {
        OasisTarget newTarget;
        newTarget = new OasisTarget();
        newTarget.setType(targetType.getType());
        newTarget.setContent(targetType.getContent().get(0).toString());
        newTarget.setIdScheme(targetType.getIdscheme());
        OasisLanguageValues language = getLanguage(targetType);
        newTarget.setLanguage(language);
        newTarget = (OasisTarget) newTarget.save(entityManager);
        return newTarget;
    }

    private OasisTarget getCommonTarget() {
        return globalTarget;
    }

    private List<OasisTag> loadTags(List<TagType> tagsType) {
        boolean needToAddPageTag = true;
        List<OasisTag> tags = new ArrayList<OasisTag>();
        for (TagType tag : tagsType) {
            OasisTag newTag = new OasisTag(tag.getTname(), tag.getValue());
            newTag = (OasisTag) newTag.save(entityManager);
            tags.add(newTag);

            if ("Page".equalsIgnoreCase(tag.getTname())) {
                needToAddPageTag = false;
            }
        }

        if (needToAddPageTag) {
            OasisTag newTag = new OasisTag("Page", "0");
            newTag = (OasisTag) newTag.save(entityManager);
            tags.add(newTag);
        }

        return tags;
    }

    private OasisPrescription loadPrescription(PrescriptionType prescriptionType) {

        OasisPrescription prescription = null;
        if (prescriptionType != null && prescriptionType.getLevel() != null) {
            prescription = new OasisPrescription();
            prescription.setContent(prescriptionType.getValue());
            prescription.setLevel(OasisPrescriptionLevelValues.valueOf(prescriptionType.getLevel().toLowerCase()));
            prescription = (OasisPrescription) prescription.save(entityManager);
        }
        return prescription;
    }

    private OasisPredicate loadPredicate(PredicateType predicateType) {
        OasisPredicate predicate = new OasisPredicate();
        OasisLanguageValues language = getLanguage(predicateType);
        predicate.setLanguage(language);
        predicate.setContent(predicateType.getContent().get(0).toString());
        predicate = (OasisPredicate) predicate.save(entityManager);

        return predicate;
    }

    private OasisLanguageValues getLanguage(Object object) {
        java.lang.reflect.Method method;
        String languageType = null;
        try {
            method = object.getClass().getMethod("getLg");
            try {
                languageType = (String) method.invoke(object, new Object[]{});
            } catch (IllegalArgumentException e) {
                logExceptionError(e);
            } catch (IllegalAccessException e) {
                logExceptionError(e);
            } catch (InvocationTargetException e) {
                logExceptionError(e);
            }
        } catch (SecurityException e) {
            logExceptionError(e);
        } catch (NoSuchMethodException e) {
            logExceptionError(e);
        }

        OasisLanguageValues language = (languageType == null) ?
                OasisLanguageValues.NATURAL :
                OasisLanguageValues.valueOf(languageType);

        return language;
    }

    private void logExceptionError(Exception e) {
        LOG.error(Arrays.toString(e.getStackTrace()));
    }
}
