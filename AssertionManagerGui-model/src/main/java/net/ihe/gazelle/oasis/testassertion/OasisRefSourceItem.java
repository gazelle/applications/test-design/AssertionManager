package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Audited
@Name("oasisRefSourceItem")
@Table(name = "am_oasis_ref_source_item")
@SequenceGenerator(name = "am_oasis_ref_source_item_sequence", sequenceName = "am_oasis_ref_source_item_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisRefSourceItem extends GazellePersiste
        implements java.io.Serializable, Comparable<OasisRefSourceItem> {

    private static final long serialVersionUID = 239732241564358787L;
    public static final String TEXT = "text";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_ref_source_item_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "name")
    @Lob
    @Type(type = TEXT)
    @XmlAttribute(name = "srcname")
    private String name;

    @Column(name = "uri")
    @Lob
    @Type(type = TEXT)
    @XmlAttribute(name = "uri")
    private String uri;

    @Column(name = "document_id")
    @Lob
    @Type(type = TEXT)
    @XmlAttribute(name = "documentId")
    private String documentId;

    @Column(name = "version_id")
    @Lob
    @Type(type = TEXT)
    @XmlAttribute(name = "versionId")
    private String versionId;

    @Column(name = "revision_id")
    @Lob
    @Type(type = TEXT)
    @XmlAttribute(name = "revisionId")
    private String revisionId;

    @Column(name = "resource_provenance_id")
    @Lob
    @Type(type = TEXT)
    @XmlAttribute(name = "resourceProvenanceId")
    private String resourceProvenanceId;

    @ManyToMany(mappedBy = "refSourceItems")
    @XmlTransient
    private List<OasisNormativeSource> normativeSources;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        if (name == null) {
            return "";
        } else {
            return this.name;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return this.uri;
    }

    public String getEncodedUri() throws UnsupportedEncodingException {
        String uriEncoded = uri;
        if (uri != null) {
            uriEncoded = URLEncoder.encode(uri, "utf-8");

        }
        return uriEncoded;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDocumentId() {
        return this.documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getVersionId() {
        return this.versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getRevisionId() {
        return this.revisionId;
    }

    public void setRevisionId(String revisionId) {
        this.revisionId = revisionId;
    }

    public String getResourceProvenanceId() {
        return this.resourceProvenanceId;
    }

    public void setResourceProvenanceId(String resourceProvenanceId) {
        this.resourceProvenanceId = resourceProvenanceId;
    }

    public List<OasisNormativeSource> getNormativeSources() {
        return this.normativeSources;
    }

    public void setNormativeSources(List<OasisNormativeSource> normativeSources) {
        this.normativeSources = normativeSources;
    }

    public void addNormativeSource(OasisNormativeSource normativeSources) {
        this.normativeSources = new ArrayList<OasisNormativeSource>();
        this.normativeSources.add(normativeSources);
    }

    @Override
    public int compareTo(OasisRefSourceItem o) {
        final int len1 = getName().length();
        final int len2 = o.getName().length();

        if (len1 > len2) {
            return 1;
        } else if (len1 < len2) {
            return -1;
        } else {
            return getName().compareToIgnoreCase(o.getName());
        }
    }

    public int countAssertionsLinked() {
        OasisTestAssertionQuery q = new OasisTestAssertionQuery();
        q.normativeSource().refSourceItems().id().eq(id);
        return q.getCount();
    }

    public List<String> getIdSchemesLinked() {
        OasisTestAssertionQuery q = new OasisTestAssertionQuery();
        q.normativeSource().refSourceItems().id().eq(id);
        List<String> idSchemes = q.target().idScheme().getListDistinct();
        Collections.sort(idSchemes);
        return idSchemes;
    }

    public List<Integer> getTargetLinkedById() {
        OasisTestAssertionQuery q = new OasisTestAssertionQuery();
        q.normativeSource().refSourceItems().id().eq(id);
        return q.target().id().getListDistinct();
    }

    public void markAssertionsForReview(EntityManager entityManager) {
        OasisNormativeSource.markAssertionsForReview(entityManager,this);
    }

    public boolean pointsToPdf() {
        if(getUri() != null) {
            return getUri().endsWith(".pdf");
        }else {
            return false;
        }
    }
}
