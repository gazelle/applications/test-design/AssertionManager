package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Audited
@Name("oasisPrerequisite")
@Table(name = "am_oasis_prerequisite")
@SequenceGenerator(name = "am_oasis_prerequisite_sequence", sequenceName = "am_oasis_prerequisite_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisPrerequisite extends GazellePersiste implements java.io.Serializable {


    private static final long serialVersionUID = -7597426700418834629L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_prerequisite_sequence")
    @XmlTransient
    private Integer id;

    @NotNull
    @Column(name = "content")
    @Lob
    @Type(type = "text")
    private String content;

    @Column(name = "language")
    private OasisLanguageValues language;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public OasisLanguageValues getLanguage() {
        return this.language;
    }

    public void setLanguage(OasisLanguageValues language) {
        this.language = language;
    }

}
