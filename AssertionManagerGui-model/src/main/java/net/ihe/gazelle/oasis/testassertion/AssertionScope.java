package net.ihe.gazelle.oasis.testassertion;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Audited
@Name("assertionScope")
@Table(name = "am_assertion_scope")
@SequenceGenerator(name = "am_assertion_scope_sequence", sequenceName = "am_assertion_scope_id_seq", allocationSize = 1)
public class AssertionScope implements java.io.Serializable, Comparable<AssertionScope> {


    private static final long serialVersionUID = 8196666878647586877L;
    public static final int MAX_LENGTH = 64;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_assertion_scope_sequence")
    private Integer id;

    @Column(name = "description")
    @Lob
    @Type(type = "text")
    private String description;

    @Column(name = "keyword", unique = true, nullable = false)
    @Size(max = MAX_LENGTH)
    private String keyword;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "am_assertion_scope_to_test_assertions", joinColumns = {
            @JoinColumn(name = "scope_id", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "assertion_id", nullable = false, updatable = false)}, uniqueConstraints = @UniqueConstraint(columnNames = {
            "scope_id", "assertion_id"}))
    private List<OasisTestAssertion> assertions;

    /*A scope can be composed of other scopes*/
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "am_assertion_scope_links", joinColumns = {
            @JoinColumn(name = "father_scope_id", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "child_scope_id", nullable = false, updatable = false)}, uniqueConstraints = @UniqueConstraint(columnNames = {
            "father_scope_id", "child_scope_id"}))
    private List<AssertionScope> linkedScopes;

    public AssertionScope(String description, String keyword) {
        super();
        this.description = description;
        this.keyword = keyword;
    }

    public AssertionScope() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<OasisTestAssertion> getAssertions() {
        if (assertions == null) {
            assertions = new ArrayList<OasisTestAssertion>();
        }
        return assertions;
    }

    public Set<OasisTestAssertion> getAllAssertions() {
        Set<OasisTestAssertion> allAssertions = new HashSet<OasisTestAssertion>();
        List<Integer> processedScopesIds = new ArrayList<Integer>();
        getScopeAssertions(allAssertions, this, processedScopesIds);

        return allAssertions;
    }

    private static Set<OasisTestAssertion> getScopeAssertions(Set<OasisTestAssertion> allAssertions,
                                                              AssertionScope scope, List<Integer> processedScopesIds) {
        if (!processedScopesIds.contains(scope.getId())) {
            allAssertions.addAll(scope.getAssertions());
            processedScopesIds.add(scope.getId());

            if (scope.getLinkedScopes() != null) {
                for (AssertionScope linkedScope : scope.getLinkedScopes()) {
                    getScopeAssertions(allAssertions, linkedScope, processedScopesIds);
                }
            }
        }
        return allAssertions;
    }

    public static List<Integer> getLinkedScopeIds(AssertionScope scope, List<Integer> processedScopesIds) {
        if (!processedScopesIds.contains(scope.getId())) {
            processedScopesIds.add(scope.getId());

            if (scope.getLinkedScopes() != null) {
                for (AssertionScope linkedScope : scope.getLinkedScopes()) {
                    getLinkedScopeIds(linkedScope, processedScopesIds);
                }
            }
        }
        return processedScopesIds;
    }

    public void setAssertions(List<OasisTestAssertion> assertions) {
        this.assertions = assertions;
    }

    public void addAssertions(List<OasisTestAssertion> assertions) {
        this.assertions.addAll(assertions);
    }

    public void removeAssertion(List<OasisTestAssertion> assertion) {
        for (OasisTestAssertion oasisTestAssertion : assertion) {
            assertions.remove(oasisTestAssertion);
        }

    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AssertionScope)) {
            return false;
        }
        AssertionScope o = (AssertionScope) obj;
        if (!getKeyword().equals(o.getKeyword())) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(AssertionScope o) {
        if (o == null) {
            return 1;
        }
        return this.getKeyword().compareTo(o.getKeyword());
    }


    public int hashCode() {
        return new HashCodeBuilder().append(getKeyword()).toHashCode();
    }

    public List<AssertionScope> getLinkedScopes() {
        return linkedScopes;
    }

    public void setLinkedScopes(List<AssertionScope> linkedScopes) {
        this.linkedScopes = linkedScopes;
    }

    public void addLinkedScopes(List<AssertionScope> linkedScopes) {
        this.linkedScopes.addAll(linkedScopes);
    }

    public static List<AssertionScope> getAssertionsScopesReferencing(AssertionScope scope, EntityManager em) {
        AssertionScopeQuery query = new AssertionScopeQuery(em);
        query.linkedScopes().id().eq(scope.getId());
        List<AssertionScope> listDistinct = query.getListDistinct();
        Collections.sort(listDistinct);
        return listDistinct;
    }

    public static int getAssertionsScopesReferencingSize(AssertionScope scope, EntityManager em) {
        AssertionScopeQuery query = new AssertionScopeQuery(em);
        query.linkedScopes().id().eq(scope.getId());
        return query.getCount();
    }

    public void removeScopeLink(AssertionScope scopeToUnlink) {
        linkedScopes.remove(scopeToUnlink);
    }
}
