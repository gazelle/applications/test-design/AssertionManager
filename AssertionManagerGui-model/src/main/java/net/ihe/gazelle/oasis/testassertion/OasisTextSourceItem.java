package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@Entity
@Audited
@Name("oasisTextSourceItem")
@Table(name = "am_oasis_text_source_item")
@SequenceGenerator(name = "am_oasis_text_source_sequence", sequenceName = "am_oasis_text_source_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisTextSourceItem extends GazellePersiste implements java.io.Serializable {


    private static final long serialVersionUID = -4962211929625014856L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_text_source_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "content")
    @Lob
    @Type(type = "text")
    private String content;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "textSourceItems")
    private transient List<OasisNormativeSource> normativeSources;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OasisNormativeSource> getNormativeSources() {
        return this.normativeSources;
    }

    public void setNormativeSources(List<OasisNormativeSource> normativeSources) {
        this.normativeSources = normativeSources;
    }
}
