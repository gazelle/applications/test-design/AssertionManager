package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Audited
@Name("oasisPrescription")
@Table(name = "am_oasis_prescription")
@SequenceGenerator(name = "am_oasis_prescription_sequence", sequenceName = "am_oasis_prescription_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisPrescription extends GazellePersiste implements java.io.Serializable {


    private static final long serialVersionUID = 3896165825410248162L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_prescription_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "content")
    @Lob
    @Type(type = "text")
    @XmlTransient
    private String content;

    @NotNull
    @Column(name = "level")
    @XmlAttribute(name = "level")
    private OasisPrescriptionLevelValues level;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public OasisPrescriptionLevelValues getLevel() {
        return this.level;
    }

    public void setLevel(OasisPrescriptionLevelValues level) {
        this.level = level;
    }

}
