package net.ihe.gazelle.oasis.testassertion;

public enum TestAssertionStatus {

    TO_BE_REVIEWED("to be reviewed"),
    REVIEWED("reviewed"),
    TO_DELETE("to delete"),
    VALIDATED("validated"),
    DEPRECATED("deprecated");

    private String friendlyName;

    TestAssertionStatus(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
