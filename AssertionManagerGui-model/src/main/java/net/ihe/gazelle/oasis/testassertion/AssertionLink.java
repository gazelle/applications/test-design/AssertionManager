package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 */
@Entity
@Audited
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "am_assertions_links", uniqueConstraints = @UniqueConstraint(columnNames = {"linked_entity",
        "linked_entity_keyword", "assertion_id"}))
@SequenceGenerator(name = "am_assertions_links_sequence", sequenceName = "am_assertions_links_id_seq", allocationSize = 1)
@DiscriminatorColumn(name = "linked_entity")
@DiscriminatorOptions(force = true)
public abstract class AssertionLink {

    public static final int MAX_LENGTH = 255;

    /**
     * Constructor for AssertionLink.
     */
    public AssertionLink() {
        super();
    }

    /**
     * Field id.
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_assertions_links_sequence")
    private Integer id;

    /**
     * Field linkedEntityKeyword.
     */
    @Column(name = "linked_entity_keyword", nullable = false)
    private String linkedEntityKeyword;

    @Column(name = "linked_entity_provider")
    private String linkedEntityProvider;

    @Column(name = "linked_entity_permalink")
    private String linkedEntityPermalink;

    @Column(name = "linked_entity_name")
    private String linkedEntityName;

    @Column(name = "linked_entity", insertable = false, updatable = false)
    private String linkedEntity;

    @Column(name = "deprecated", columnDefinition = "boolean default false")
    private boolean deprecated = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDeprecated() {
        return deprecated;
    }

    public void setDeprecated(boolean ready) {
        this.deprecated = ready;
    }

    public String getLinkedEntityKeyword() {
        return linkedEntityKeyword;
    }

    public void setLinkedEntityKeyword(String entityKeyword) {
        this.linkedEntityKeyword = entityKeyword;
    }

    /**
     * Method getAssertion.
     *
     * @return OasisTestAssertion
     */
    abstract OasisTestAssertion getAssertion();

    /**
     * Method setAssertion.
     *
     * @param assertion OasisTestAssertion
     */
    abstract void setAssertion(OasisTestAssertion assertion);

    public String getLinkedEntityProvider() {
        return linkedEntityProvider;
    }

    public void setLinkedEntityProvider(String linkedEntityProvider) {
        this.linkedEntityProvider = linkedEntityProvider;
    }

    public String getLinkedEntityPermalink() {
        return linkedEntityPermalink;
    }

    public void setLinkedEntityPermalink(String linkedEntityPermalink) {
        this.linkedEntityPermalink = linkedEntityPermalink;
    }

    public String getLinkedEntityName() {
        return linkedEntityName;
    }

    public void setLinkedEntityName(String linkedEntityName) {
        if (linkedEntityName.length() > MAX_LENGTH) {
            this.linkedEntityName = linkedEntityName.substring(0, MAX_LENGTH - 1);
        } else {
            this.linkedEntityName = linkedEntityName;
        }
    }

    public String getLinkedEntity() {
        return linkedEntity;
    }

    public void setLinkedEntity(String linkedEntity) {
        this.linkedEntity = linkedEntity;
    }
}
