package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToAipoQuery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
@DiscriminatorValue(value = AssertionAppliesToAipo.DB_DISCRIMINATOR)
public class AssertionAppliesToAipo extends AbstractAssertionAppliesTo implements Serializable {

    public static final String DB_DISCRIMINATOR = "TF_AIPO";
    private static final long serialVersionUID = -8888891576402685931L;

    public AssertionAppliesToAipo(String entityKeyword, String entityName, int entityId, String entityProvider, String permalink, OasisTestAssertion assertion) {
        super(entityKeyword, entityName, entityId, entityProvider, permalink, assertion);
    }

    public AssertionAppliesToAipo() {
        super();
    }

    public List<AssertionAppliesToAipo> getAssertionsLinked() {
        AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery();
        query.linkedEntityKeyword().eq(this.getLinkedEntityKeyword());
        query.id().neq(this.getId());
        return query.getList();
    }
}
