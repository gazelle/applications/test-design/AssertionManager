package net.ihe.gazelle.oasis.testassertion;

public enum OasisPrescriptionLevelValues {

    mandatory("Mandatory / Required / Shall"),
    permitted("Permitted / May / Can"),
    preferred("Preferred / Should / Recommended");

    private String friendlyName;

    OasisPrescriptionLevelValues(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}