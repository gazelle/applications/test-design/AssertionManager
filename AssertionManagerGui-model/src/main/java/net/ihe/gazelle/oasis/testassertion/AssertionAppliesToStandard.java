package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToStandardQuery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
@DiscriminatorValue(value = AssertionAppliesToStandard.DB_DISCRIMINATOR)
public class AssertionAppliesToStandard extends AbstractAssertionAppliesTo implements Serializable {

    public static final String DB_DISCRIMINATOR = "TF_STANDARD";
    private static final long serialVersionUID = 6839362999627422443L;

    public AssertionAppliesToStandard(String entityKeyword, String entityName, int entityId, String entityProvider,
                                      String permalink, OasisTestAssertion assertion) {
        super(entityKeyword, entityName, entityId, entityProvider, permalink, assertion);
    }

    public AssertionAppliesToStandard() {
        super();
    }

    public List<AssertionAppliesToStandard> getAssertionsLinked() {
        AssertionAppliesToStandardQuery query = new AssertionAppliesToStandardQuery();
        query.linkedEntityKeyword().eq(this.getLinkedEntityKeyword());
        query.id().neq(this.getId());
        return query.getList();
    }
}
