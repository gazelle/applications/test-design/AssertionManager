package net.ihe.gazelle.oasis.testassertion;

import org.jboss.seam.annotations.Destroy;

public class CacheManager {

    public static void put(String key, String value) {
        MapDbCache.getCache().put(key, value);
    }

    public static String get(String key) {
        return MapDbCache.getCache().get(key);
    }

    @Destroy
    public void shutdown() {
        MapDbCache.getInstance().close();
    }
}
