package net.ihe.gazelle.oasis.testassertion;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;

@Entity
@Audited
@Name("oasisTag")
@Table(name = "am_oasis_tag")
@SequenceGenerator(name = "am_oasis_tag_sequence", sequenceName = "am_oasis_tag_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisTag implements java.io.Serializable, Comparable<OasisTag> {


    private static final long serialVersionUID = 244976032256605212L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_tag_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "content")
    @Lob
    @Type(type = "text")
    @XmlValue
    private String content = "";

    @NotNull
    @Column(name = "name")
    @XmlAttribute(name = "tname")
    private String name = "";

    public OasisTag() {
    }

    public OasisTag(String name, String content) {
        super();
        this.content = content;
        this.name = name;
    }

    public OasisTag(String name) {
        this.name = name;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        if (content != null){
            this.content = content;
        } else {
            this.content = "";
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object save(EntityManager em) {
        Object obj = em.merge(this);
        em.flush();
        return obj;
    }

    public String toString() {
        return name + ": " + content;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof OasisTag)) {
            return false;
        }
        OasisTag o = (OasisTag) obj;
        if (!name.equals(o.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(OasisTag o) {
        if (o == null || o.name == null) {
            return -1;
        }
        if (this.name == null) {
            return 1;
        }
        return this.name.compareTo(o.name);
    }

    public int hashCode() {
        return new HashCodeBuilder().append(name).toHashCode();
    }
}
