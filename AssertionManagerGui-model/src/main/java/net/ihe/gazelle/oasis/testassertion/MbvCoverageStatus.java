package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Audited
@Name("mbvCoverageStatus")
@Table(name = "am_mbv_coverage_status")
@SequenceGenerator(name = "am_mbv_coverage_status_sequence", sequenceName = "am_mbv_coverage_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
public class MbvCoverageStatus implements java.io.Serializable {

    public static final int MAX_LENGTH = 255;

    public MbvCoverageStatus() {
    }

    public MbvCoverageStatus(CoverageStatusEnum coverageStatus, String idScheme, String assertionId,
                             MbvService mbvService) {
        super();
        this.idScheme = idScheme;
        this.coverageStatus = coverageStatus;
        this.mbvService = mbvService;
        this.assertionId = assertionId;
    }


    private static final long serialVersionUID = 5352569779930496315L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_mbv_coverage_status_sequence")
    private Integer id;

    @Column(name = "id_scheme", nullable = false)
    @Size(max = MAX_LENGTH)
    private String idScheme;

    public String getIdScheme() {
        return idScheme;
    }

    public void setIdScheme(String idScheme) {
        this.idScheme = idScheme;
    }

    public String getAssertionId() {
        return assertionId;
    }

    public void setAssertionId(String assertionId) {
        this.assertionId = assertionId;
    }

    @Column(name = "assertion_id", nullable = false)
    @Size(max = MAX_LENGTH)
    private String assertionId;

    @Column(name = "coverage_status")
    private CoverageStatusEnum coverageStatus;

    @ManyToOne
    @JoinColumn(name = "mbv_service_id", nullable = false)
    private MbvService mbvService;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CoverageStatusEnum getCoverageStatus() {
        return coverageStatus;
    }

    public void setCoverageStatus(CoverageStatusEnum coverageStatus) {
        this.coverageStatus = coverageStatus;
    }

    public MbvService getMbvService() {
        return mbvService;
    }

    public void setMbvService(MbvService mbvService) {
        this.mbvService = mbvService;
    }

    public enum CoverageStatusEnum {
        Covered("Covered"),
        Missing("Missing"),
        Removed("Removed");

        private String friendlyName;

        CoverageStatusEnum(String friendlyName) {
            this.friendlyName = friendlyName;
        }

        public String getFriendlyName() {
            return friendlyName;
        }
    }

    public String toString() {
        return "assertionid: " + getAssertionId() + " and idScheme: " + getIdScheme();
    }

}
