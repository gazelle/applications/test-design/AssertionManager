package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.assertion.tags.TagKeywords;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Audited
@Name("oasisTestAssertion")
@Table(name = "am_oasis_test_assertion", uniqueConstraints = @UniqueConstraint(columnNames = {"assertion_id",
        "target_id"}))
@SequenceGenerator(name = "am_oasis_test_assertion_sequence", sequenceName = "am_oasis_test_assertion_id_seq", allocationSize = 1)
@XmlRootElement(name = "testAssertion")
@XmlSeeAlso(OasisNormativeSource.class)
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisTestAssertion extends GazellePersiste
        implements java.io.Serializable, Comparable<OasisTestAssertion> {


    private static final long serialVersionUID = -7937216739012141800L;
    public static final int MAX_COMMENT_SIZE = 1024;
    public static final String ASSERTION = "assertion";
    public static final String INSTANCES = "instances";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_test_assertion_sequence")
    @XmlTransient
    private Integer id;

    @XmlAttribute(name = "id")
    @Column(name = "assertion_id")
    @NotNull
    private String assertionId;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "predicate_id")
    @NotNull

    private OasisPredicate predicate;

    @ManyToOne
    @JoinColumn(name = "target_id")
    @NotNull

    @XmlTransient
    private OasisTarget target;

    @ManyToOne(cascade = {CascadeType.REMOVE})
    @JoinColumn(name = "prerequisite_id")

    private OasisPrerequisite prerequisite;

    @ManyToOne
    @JoinColumn(name = "normative_source_id")
    @NotNull

    @XmlTransient
    private OasisNormativeSource normativeSource;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "prescription_id")

    private OasisPrescription prescription;

    @ManyToOne
    @JoinColumn(name = "description_id")

    private OasisDescription description;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "am_oasis_test_assertion_tag", joinColumns = {
            @JoinColumn(name = "test_assertion_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "tag_id", referencedColumnName = "id")})

    @XmlElement(name = "tag")
    private List<OasisTag> tags;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "am_oasis_test_assertion_variable", joinColumns = {
            @JoinColumn(name = "test_assertion_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "variable_id", referencedColumnName = "id")})

    private List<OasisVariable> variables;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionLinkedToTest.class, cascade = {CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionLinkedToTest> tests;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionLinkedToTestStep.class, cascade = {CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionLinkedToTestStep> testSteps;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionLinkedToRule.class, cascade = {CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionLinkedToRule> tfRules;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionLinkedToMBV.class, cascade = {CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionLinkedToMBV> mbv;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionAppliesToActor.class, cascade = {CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionAppliesToActor> tfActors;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionAppliesToTransaction.class, cascade = {
            CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionAppliesToTransaction> tfTransactions;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionAppliesToAipo.class, cascade = {CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionAppliesToAipo> tfAIPOs;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionAppliesToStandard.class, cascade = {
            CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionAppliesToStandard> tfStandards;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionAppliesToIntegrationProfile.class, cascade = {
            CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionAppliesToIntegrationProfile> tfIntegrationProfiles;

    @OneToMany(mappedBy = ASSERTION, targetEntity = AssertionAppliesToAuditMessage.class, cascade = {
            CascadeType.REMOVE})
    @XmlTransient
    @NotAudited
    private List<AssertionAppliesToIntegrationProfile> tfAuditMessages;

    @ManyToMany(mappedBy = "assertions")
    @Cascade({org.hibernate.annotations.CascadeType.REFRESH})
    @XmlTransient
    @NotAudited
    private List<AssertionScope> scopes;

    @XmlElement(name = "comment")
    @Column(name = "comment")
    @Size(max = MAX_COMMENT_SIZE)
    private String comment;

    @Column(name = "tests_coverage_count")
    @XmlTransient
    private Integer testsCoverageCount;

    @Column(name = "tf_rules_coverage_count")
    @XmlTransient
    private Integer tfRulesCoverageCount;

    @Column(name = "mbv_coverage_count")
    @XmlTransient
    private Integer mbvCoverageCount;

    @XmlElement(name = "status")
    @Column(name = "status")
    private TestAssertionStatus status;

    @Column(name = "applies_to_count")
    @XmlTransient
    private Integer appliesToCount;

    @Column(name = "coverage_count")
    @XmlTransient
    private Integer coverageCount;

    @Column(name = "page_number")
    @XmlTransient
    private Integer pageNumber;

    @Column(name = "other_tag_on_assertion")
    @XmlTransient
    private String otherTagOnAssertion;

    @Transient
    @XmlTransient
    private Boolean selected = false;

    @Column(name = "testable", columnDefinition = "boolean default true")
    private boolean testable;

    public OasisTestAssertion() {
        this.tags = new ArrayList<OasisTag>();
        testable = true;
    }

    public OasisTestAssertion(String assertionId) {
        this.assertionId = assertionId;
        this.tags = new ArrayList<OasisTag>();
        testable = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OasisPredicate getPredicate() {
        return predicate;
    }

    public void setPredicate(OasisPredicate predicate) {
        this.predicate = predicate;
    }

    public OasisPrescription getPrescription() {
        return this.prescription;
    }

    public void setPrescription(OasisPrescription prescription) {
        this.prescription = prescription;
    }

    public OasisTarget getTarget() {
        return this.target;
    }

    public void setTarget(OasisTarget target) {
        this.target = target;
    }

    public OasisNormativeSource getNormativeSource() {
        return this.normativeSource;
    }

    public void setNormativeSource(OasisNormativeSource normativeSource) {
        this.normativeSource = normativeSource;
    }

    public OasisPrerequisite getPrerequisite() {
        return this.prerequisite;
    }

    public void setPrerequisite(OasisPrerequisite prerequisite) {
        this.prerequisite = prerequisite;
    }

    public OasisDescription getDescription() {
        return this.description;
    }

    public void setDescription(OasisDescription description) {
        this.description = description;
    }

    public List<OasisTag> getTags() {
        List<OasisTag> tags2 = this.tags;
        Collections.sort(tags2);

        return tags2;
    }

    public void setTags(List<OasisTag> tags) {
        this.tags = tags;
    }

    public List<OasisVariable> getVariables() {
        return this.variables;
    }

    public void setVariables(List<OasisVariable> variables) {
        this.variables = variables;
    }

    public String getAssertionId() {
        return this.assertionId;
    }

    public void setAssertionId(String assertionId) {
        this.assertionId = assertionId;
    }

    public static List<OasisTestAssertion> getTestAssertions(EntityManager entityManager) throws JAXBException {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(entityManager);
        orderByAssertionId(query);
        return query.getList();
    }

    private static void orderByAssertionId(OasisTestAssertionQuery query) {
        query.assertionId().order(true);
    }

    private static OasisTestAssertion getTestAssertionByIdAndIdScheme(String testAssertionId, String idScheme,
                                                                      EntityManager entityManager) throws JAXBException {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(entityManager);
        query.assertionId().eq(testAssertionId);
        query.target().idScheme().eq(idScheme);
        orderByAssertionId(query);
        return query.getUniqueResult();
    }

    private static List<OasisTestAssertion> getTestAssertionByIdScheme(String idScheme, EntityManager entityManager)
            throws JAXBException {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(entityManager);
        query.target().idScheme().eq(idScheme);
        orderByAssertionId(query);

        return query.getList();
    }

    public static List<OasisTestAssertion> getTestAssertion(String testAssertionId, String idScheme,
                                                            EntityManager entityManager) throws JAXBException {
        List<OasisTestAssertion> result = null;
        if ((testAssertionId == null) && (idScheme != null)) {
            result = OasisTestAssertion.getTestAssertionByIdScheme(idScheme, entityManager);
        } else if ((testAssertionId != null) && (idScheme != null)) {
            result = new ArrayList<OasisTestAssertion>();
            result.add(OasisTestAssertion.getTestAssertionByIdAndIdScheme(testAssertionId, idScheme, entityManager));
        }
        return result;
    }

    public String getTestAssertionPageNumber() {
        String pageNum = "0";
        OasisTag tag = null;
        tag = getPageTag();
        if (tag != null && !tag.getContent().isEmpty()) {
            pageNum = tag.getContent();
        }
        return pageNum;
    }

    public OasisTag getPageTag() {
        OasisTag tag = null;
        for (OasisTag oasisTag : tags) {
            if (oasisTag.getName() != null && oasisTag.getName().equalsIgnoreCase("Page")) {
                tag = oasisTag;
            }
        }
        return tag;
    }


    public OasisTag getOtherTag() {
        OasisTag tag = new OasisTag();
        List<String> tagFriendlyNames = TagKeywords.getTagFriendlyNames();
        for (OasisTag oasisTag : tags) {
            if (oasisTag.getName() != null && tagFriendlyNames.contains(oasisTag.getName())) {
                tag = oasisTag;
            }
        }
        return tag;
    }

    public List<AssertionLinkedToTest> getDeprecatedTests() {
        List<AssertionLinkedToTest> deprecatedTests = new ArrayList<AssertionLinkedToTest>();
        List<AssertionLinkedToTest> allTests = getTests();
        if (allTests != null && !allTests.isEmpty()) {
            for (AssertionLinkedToTest test : allTests) {
                if (test.isDeprecated()) {
                    deprecatedTests.add(test);
                }
            }
        }
        return deprecatedTests;
    }

    public List<AssertionLinkedToTest> getNotDeprecatedTests() {
        List<AssertionLinkedToTest> notDeprecatedTests = new ArrayList<AssertionLinkedToTest>();
        List<AssertionLinkedToTest> allTests = getTests();
        if (allTests != null && !allTests.isEmpty()) {
            for (AssertionLinkedToTest test : allTests) {
                if (!test.isDeprecated()) {
                    notDeprecatedTests.add(test);
                }
            }
        }
        return notDeprecatedTests;
    }

    public List<AssertionLinkedToTestStep> getNotDeprecatedTestSteps() {
        List<AssertionLinkedToTestStep> notDeprecatedTestSteps = new ArrayList<AssertionLinkedToTestStep>();
        List<AssertionLinkedToTestStep> allSteps = getTestSteps();
        if (allSteps != null && !allSteps.isEmpty()) {
            for (AssertionLinkedToTestStep step : allSteps) {
                if (!step.isDeprecated()) {
                    notDeprecatedTestSteps.add(step);
                }
            }
        }
        return notDeprecatedTestSteps;
    }

    public List<AssertionLinkedToTestStep> getDeprecatedTestSteps() {
        List<AssertionLinkedToTestStep> deprecatedTestSteps = new ArrayList<AssertionLinkedToTestStep>();
        List<AssertionLinkedToTestStep> allSteps = getTestSteps();
        if (allSteps != null && !allSteps.isEmpty()) {
            for (AssertionLinkedToTestStep step : allSteps) {
                if (step.isDeprecated()) {
                    deprecatedTestSteps.add(step);
                }
            }
        }
        return deprecatedTestSteps;
    }

    public List<AssertionLinkedToRule> getNotDeprecatedRules() {
        List<AssertionLinkedToRule> notDeprecatedRules = new ArrayList<AssertionLinkedToRule>();
        List<AssertionLinkedToRule> allRules = getTfRules();
        if (allRules != null && !allRules.isEmpty()) {
            for (AssertionLinkedToRule rule : allRules) {
                if (!rule.isDeprecated()) {
                    notDeprecatedRules.add(rule);
                }
            }
        }
        return notDeprecatedRules;
    }

    public List<AssertionLinkedToRule> getDeprecatedRules() {
        List<AssertionLinkedToRule> deprecatedRules = new ArrayList<AssertionLinkedToRule>();
        List<AssertionLinkedToRule> allRules = getTfRules();
        if (allRules != null && !allRules.isEmpty()) {
            for (AssertionLinkedToRule rule : allRules) {
                if (rule.isDeprecated()) {
                    deprecatedRules.add(rule);
                }
            }
        }
        return deprecatedRules;
    }


    public List<AssertionLinkedToTest> getTests() {
        if (tests == null) {
            tests = new ArrayList<AssertionLinkedToTest>();
        }
        return tests;
    }


    public List<AssertionLinkedToTestStep> getTestSteps() {
        return testSteps;
    }

    public List<AssertionLinkedToRule> getTfRules() {
        return tfRules;
    }

    public List<AssertionAppliesToActor> getActors() {
        return tfActors;
    }

    private void updateTotalAssertionAppliesTo() {
        setAppliesToCount(
                getTfActorsSize() + getTfAipoSize() + getTfAuditMessagesSize() + getTfIntegrationProfilesSize() + getTfStandardsSize() + getTfTransactionSize());
    }

    public int getTfTransactionSize() {
        if (tfTransactions == null) {
            return 0;
        }
        return tfTransactions.size();
    }

    public int getTfStandardsSize() {
        if (tfStandards == null) {
            return 0;
        }
        return tfStandards.size();
    }

    public int getTfIntegrationProfilesSize() {
        if (tfIntegrationProfiles == null) {
            return 0;
        }
        return tfIntegrationProfiles.size();
    }

    public int getTfAuditMessagesSize() {
        if (tfAuditMessages == null) {
            return 0;
        }
        return tfAuditMessages.size();
    }

    public int getTfAipoSize() {
        if (tfAIPOs == null) {
            return 0;
        }
        return tfAIPOs.size();
    }

    public int getTfActorsSize() {
        if (tfActors == null) {
            return 0;
        }
        return tfActors.size();
    }

    public void updateCoverageTotal() {
        setTestsCoverageCount(getTestsCount() + getTestStepsCount());
        setTfRulesCoverageCount(getTfRulesCount());
        setMbvCoverageCount(getMbvCount());
        setCoverageCount(getTfRulesCount() + getTestsCount() + getTestStepsCount() + getMbvCount());
    }

    public int getMbvCount() {
        if (mbv == null) {
            return 0;
        }
        return mbv.size();
    }

    public int getTfRulesCount() {
        if (tfRules == null) {
            return 0;
        }
        return tfRules.size();
    }

    public int getTestStepsCount() {
        if (testSteps == null) {
            return 0;
        }
        return testSteps.size();
    }

    public int getTestsCount() {
        if (tests == null) {
            return 0;
        }
        return tests.size();
    }

    public void updateCoverage() {
        updateCoverageTotal();
        updateTotalAssertionAppliesTo();
        getPageNumber();
        getOtherTagOnAssertion();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<AssertionScope> getScopes() {
        if (scopes == null) {
            scopes = new ArrayList<AssertionScope>();
        }
        return scopes;
    }

    public void setScopes(List<AssertionScope> scopes) {
        this.scopes = scopes;
    }

    @Override
    public int compareTo(OasisTestAssertion o) {
        final int len1 = getAssertionId().length();
        final int len2 = o.getAssertionId().length();

        if (len1 > len2) {
            return 1;
        } else if (len1 < len2) {
            return -1;
        } else {
            return getAssertionId().compareToIgnoreCase(o.getAssertionId());
        }
    }

    public Integer getTestsCoverageCount() {
        return testsCoverageCount;
    }

    public Integer getTfRulesCoverageCount() {
        return tfRulesCoverageCount;
    }

    public Integer getMbvCoverageCount() {
        return mbvCoverageCount;
    }

    public TestAssertionStatus getStatus() {
        return status;
    }

    public void setStatus(TestAssertionStatus status) {
        this.status = status;
    }

    public List<AssertionAppliesToTransaction> getTfTransactions() {
        return tfTransactions;
    }

    public List<AssertionAppliesToAipo> getTfAIPOs() {
        return tfAIPOs;
    }

    public List<AssertionAppliesToStandard> getTfStandards() {
        return tfStandards;
    }

    public List<AssertionAppliesToIntegrationProfile> getTfIntegrationProfiles() {
        return tfIntegrationProfiles;
    }

    public List<AssertionAppliesToIntegrationProfile> getTfAuditMessages() {
        return tfAuditMessages;
    }

    public Integer getAppliesToCount() {
        return appliesToCount;
    }

    public void setAppliesToCount(Integer appliesToCount) {
        this.appliesToCount = appliesToCount;
    }

    public Integer getCoverageCount() {
        return coverageCount;
    }

    public void setCoverageCount(Integer coverageCount) {
        this.coverageCount = coverageCount;
    }

    public Integer getPageNumber() {
        try {
            pageNumber = 0;
            OasisTag pageTag = getPageTag();
            if (pageTag != null && !pageTag.getContent().isEmpty()) {
                pageNumber = Integer.valueOf(pageTag.getContent());
            }
        } catch (NumberFormatException e) {
            pageNumber = 0;
        }
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getOtherTagOnAssertion() {
        otherTagOnAssertion = getOtherTag().getName() + "&#160;" + getOtherTag().getContent();
        return otherTagOnAssertion;
    }

    public void setOtherTagOnAssertion(String otherTag) {
        this.otherTagOnAssertion = otherTag;
    }

    public String toString() {
        return getAssertionId();
    }

    public void setTestsCoverageCount(Integer testsCoverageCount) {
        this.testsCoverageCount = testsCoverageCount;
    }

    public void setTfRulesCoverageCount(Integer tfRulesCoverageCount) {
        this.tfRulesCoverageCount = tfRulesCoverageCount;
    }

    public void setMbvCoverageCount(Integer mbvCoverageCount) {
        this.mbvCoverageCount = mbvCoverageCount;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public static void markAssertionsForReview(EntityManager entityManager, OasisNormativeSource normativeSource) {
        List<OasisTestAssertion> testAssertions = normativeSource.getTestAssertions();
        for (OasisTestAssertion oasisTestAssertion : testAssertions) {
            oasisTestAssertion.setStatus(TestAssertionStatus.TO_BE_REVIEWED);
            entityManager.merge(oasisTestAssertion);
        }
        entityManager.flush();
    }

    public void linkToDocument(EntityManager em, List<OasisRefSourceItem> sources) {
        normativeSource.linkToDocument(em, sources);
    }

    public static void updateCoverageCounts(EntityManager em) {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(em);
        List<OasisTestAssertion> listDistinct = query.getListDistinct();
        updateCoverageCounts(em, listDistinct);
        em.flush();
    }

    public static void updateCoverageCounts(EntityManager em, List<OasisTestAssertion> assertions) {
        for (OasisTestAssertion oasisTestAssertion : assertions) {
            oasisTestAssertion.updateCoverage();
            em.merge(oasisTestAssertion);
        }
    }

    public String getUri() {
        return OasisNormativeSource.getIdSchemeNormativeSourceUri(this.target.getIdScheme());
    }

    public boolean pointsToPdf() {
        return getUri().endsWith(".pdf");
    }

    public boolean isTestable() {
        return testable;
    }

    public void setTestable(boolean testable) {
        this.testable = testable;
    }
}
