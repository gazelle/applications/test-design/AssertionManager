@XmlSchema(namespace = "http://docs.oasis-open.org/ns/tag/taml-201002/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.UNSET, attributeFormDefault = XmlNsForm.UNSET)
package net.ihe.gazelle.oasis.testassertion;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;