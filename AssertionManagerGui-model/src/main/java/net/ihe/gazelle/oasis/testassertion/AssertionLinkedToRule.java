package net.ihe.gazelle.oasis.testassertion;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = AssertionLinkedToRule.DB_DISCRIMINATOR)
public class AssertionLinkedToRule extends AbstractAssertionLink implements Serializable {

    public static final String DB_DISCRIMINATOR = "TF_RULE";
    private static final long serialVersionUID = 8708962955821478131L;

    public AssertionLinkedToRule(String entityKeyword, OasisTestAssertion assertion, String linkedEntityProvider,
                                 String linkedEntityPermalink, String linkedEntityName) {
        super(entityKeyword, assertion, linkedEntityProvider, linkedEntityPermalink, linkedEntityName);
    }

    public AssertionLinkedToRule() {
        super();
    }

    public int getRuleId() {
        return Integer.valueOf(getLinkedEntityKeyword()).intValue();
    }
}
