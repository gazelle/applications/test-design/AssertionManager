Drop function update_page_number_based_on_scheme_and_section(in id_scheme text,in section_name text,in page int);
CREATE FUNCTION update_page_number_based_on_scheme_and_section(in id_scheme text,in section_name text,in page int) RETURNS integer AS $$
BEGIN
update am_oasis_tag set content=$3 where id in(
select am_oasis_tag.id
from 
am_oasis_tag,
am_oasis_test_assertion_tag,
am_oasis_test_assertion
where name='Page'
and am_oasis_test_assertion_tag.tag_id = am_oasis_tag.id
and am_oasis_test_assertion_tag.test_assertion_id= am_oasis_test_assertion.id
and am_oasis_test_assertion.id in(
select am_oasis_test_assertion.id
FROM am_oasis_tag,
am_oasis_test_assertion_tag,
am_oasis_test_assertion,
am_oasis_target
WHERE am_oasis_tag.name ='Section' 
AND am_oasis_tag.content =$2
AND am_oasis_test_assertion_tag.tag_id=am_oasis_tag.id
AND am_oasis_test_assertion.id = am_oasis_test_assertion_tag.test_assertion_id
And am_oasis_target.idscheme=$1
AND am_oasis_target.id = am_oasis_test_assertion.target_id
)
);
return 1 ;
END;
$$ LANGUAGE plpgsql;
