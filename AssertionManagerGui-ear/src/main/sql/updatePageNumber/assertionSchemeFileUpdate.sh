srcName='IHE IT Infrastructure (ITI) Technical Framework - Volume 1'
uri='https://ehealthsuisse.ihe-europe.net/docs/reference_documents/IHE_ITI_TF_Vol1.pdf'
docId='ITI TF-1'
revId='14.0'
provenanceId='IHE'

for file in assertion-atna.xml assertion-iti20.xml
do
perl -pi -e "s|srcname=\".*?\"\B?|srcname=\"$srcName\"|g" $file
perl -pi -e "s|uri=\".*?\"\B?|uri=\"$uri\"|g" $file
perl -pi -e "s|documentId=\".*?\"\B?|documentId=\"$docId\"|g" $file
perl -pi -e "s|revisionId=\".*?\"\B?|revisionId=\"$revId\"|g" $file
perl -pi -e "s|resourceProvenanceId=\".*?\"\B?|resourceProvenanceId=\"$provenanceId\"|g" $file
done

