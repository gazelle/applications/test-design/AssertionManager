--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain (
    id integer NOT NULL,
    keyword character varying(255),
    label_to_display character varying(255),
    profile character varying(255)
);


ALTER TABLE public.affinity_domain OWNER TO gazelle;

--
-- Name: affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain_transactions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain_transactions (
    affinity_domain_id integer NOT NULL,
    transaction_id integer NOT NULL
);


ALTER TABLE public.affinity_domain_transactions OWNER TO gazelle;

--
-- Name: am_assertion_scope; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_assertion_scope (
    id integer NOT NULL,
    description text,
    keyword character varying(64) NOT NULL
);


ALTER TABLE public.am_assertion_scope OWNER TO gazelle;

--
-- Name: am_assertion_scope_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_assertion_scope_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_assertion_scope_id_seq OWNER TO gazelle;

--
-- Name: am_assertion_scope_links; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_assertion_scope_links (
    father_scope_id integer NOT NULL,
    child_scope_id integer NOT NULL
);


ALTER TABLE public.am_assertion_scope_links OWNER TO gazelle;

--
-- Name: am_assertion_scope_to_test_assertions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_assertion_scope_to_test_assertions (
    scope_id integer NOT NULL,
    assertion_id integer NOT NULL
);


ALTER TABLE public.am_assertion_scope_to_test_assertions OWNER TO gazelle;

--
-- Name: am_assertions_applies_to; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_assertions_applies_to (
    linked_entity character varying(31) NOT NULL,
    id integer NOT NULL,
    linked_entity_id integer NOT NULL,
    linked_entity_keyword character varying(255) NOT NULL,
    linked_entity_name character varying(255) NOT NULL,
    linked_entity_permalink character varying(255) NOT NULL,
    linked_entity_provider character varying(255) NOT NULL,
    assertion_id integer NOT NULL
);


ALTER TABLE public.am_assertions_applies_to OWNER TO gazelle;

--
-- Name: am_assertions_applies_to_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_assertions_applies_to_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_assertions_applies_to_id_seq OWNER TO gazelle;

--
-- Name: am_assertions_links; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_assertions_links (
    linked_entity character varying(31) NOT NULL,
    id integer NOT NULL,
    deprecated boolean DEFAULT false,
    linked_entity_keyword character varying(255) NOT NULL,
    linked_entity_name character varying(255),
    linked_entity_permalink character varying(255),
    linked_entity_provider character varying(255),
    assertion_id integer NOT NULL
);


ALTER TABLE public.am_assertions_links OWNER TO gazelle;

--
-- Name: am_assertions_links_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_assertions_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_assertions_links_id_seq OWNER TO gazelle;

--
-- Name: am_assertions_links_mbv; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_assertions_links_mbv (
    id integer NOT NULL,
    assertion_id integer NOT NULL,
    mbv_service_id integer NOT NULL
);


ALTER TABLE public.am_assertions_links_mbv OWNER TO gazelle;

--
-- Name: am_assertions_links_mbv_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_assertions_links_mbv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_assertions_links_mbv_id_seq OWNER TO gazelle;

--
-- Name: am_mbv_coverage_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_mbv_coverage_status (
    id integer NOT NULL,
    assertion_id character varying(255) NOT NULL,
    coverage_status integer,
    id_scheme character varying(255) NOT NULL,
    mbv_service_id integer NOT NULL
);


ALTER TABLE public.am_mbv_coverage_status OWNER TO gazelle;

--
-- Name: am_mbv_coverage_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_mbv_coverage_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_mbv_coverage_status_id_seq OWNER TO gazelle;

--
-- Name: am_mbv_service; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_mbv_service (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    name character varying(64) NOT NULL,
    last_updated timestamp without time zone,
    update_status integer,
    url character varying(255) NOT NULL
);


ALTER TABLE public.am_mbv_service OWNER TO gazelle;

--
-- Name: am_mbv_service_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_mbv_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_mbv_service_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_comment; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_comment (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text
);


ALTER TABLE public.am_oasis_comment OWNER TO gazelle;

--
-- Name: am_oasis_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_comment_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_derived_source_item; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_derived_source_item (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text,
    date_string character varying(255),
    document_id text,
    name text,
    resource_provenance_id text,
    revision_id text,
    uri text,
    version_id text
);


ALTER TABLE public.am_oasis_derived_source_item OWNER TO gazelle;

--
-- Name: am_oasis_derived_source_item_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_derived_source_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_derived_source_item_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_description; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_description (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text
);


ALTER TABLE public.am_oasis_description OWNER TO gazelle;

--
-- Name: am_oasis_description_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_description_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_description_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_interpretation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_interpretation (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text
);


ALTER TABLE public.am_oasis_interpretation OWNER TO gazelle;

--
-- Name: am_oasis_interpretation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_interpretation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_interpretation_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_normative_source; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_normative_source (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text,
    comment_id integer,
    interpretation_id integer
);


ALTER TABLE public.am_oasis_normative_source OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_derived_source_item; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_normative_source_derived_source_item (
    normative_source_id integer NOT NULL,
    derived_source_item_id integer NOT NULL
);


ALTER TABLE public.am_oasis_normative_source_derived_source_item OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_normative_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_normative_source_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_ref_source_item; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_normative_source_ref_source_item (
    normative_source_id integer NOT NULL,
    ref_source_item_id integer NOT NULL
);


ALTER TABLE public.am_oasis_normative_source_ref_source_item OWNER TO gazelle;

--
-- Name: am_oasis_normative_source_text_source_item; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_normative_source_text_source_item (
    normative_source_id integer NOT NULL,
    text_source_item_id integer NOT NULL
);


ALTER TABLE public.am_oasis_normative_source_text_source_item OWNER TO gazelle;

--
-- Name: am_oasis_predicate; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_predicate (
    id integer NOT NULL,
    content text NOT NULL,
    language integer
);


ALTER TABLE public.am_oasis_predicate OWNER TO gazelle;

--
-- Name: am_oasis_predicate_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_predicate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_predicate_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_prerequisite; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_prerequisite (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text NOT NULL,
    language integer
);


ALTER TABLE public.am_oasis_prerequisite OWNER TO gazelle;

--
-- Name: am_oasis_prerequisite_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_prerequisite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_prerequisite_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_prescription; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_prescription (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text,
    level integer NOT NULL
);


ALTER TABLE public.am_oasis_prescription OWNER TO gazelle;

--
-- Name: am_oasis_prescription_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_prescription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_prescription_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_ref_source_item; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_ref_source_item (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    document_id text,
    name text,
    resource_provenance_id text,
    revision_id text,
    uri text,
    version_id text
);


ALTER TABLE public.am_oasis_ref_source_item OWNER TO gazelle;

--
-- Name: am_oasis_ref_source_item_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_ref_source_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_ref_source_item_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_tag; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_tag (
    id integer NOT NULL,
    content text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.am_oasis_tag OWNER TO gazelle;

--
-- Name: am_oasis_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_tag_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_target; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_target (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text,
    idscheme character varying(255) NOT NULL,
    language integer,
    type text
);


ALTER TABLE public.am_oasis_target OWNER TO gazelle;

--
-- Name: am_oasis_target_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_target_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_target_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_test_assertion (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    applies_to_count integer,
    assertion_id character varying(255) NOT NULL,
    comment character varying(1024),
    coverage_count integer,
    mbv_coverage_count integer,
    other_tag_on_assertion character varying(255),
    page_number integer,
    status integer,
    testable boolean DEFAULT true,
    tests_coverage_count integer,
    tf_rules_coverage_count integer,
    description_id integer,
    normative_source_id integer NOT NULL,
    predicate_id integer NOT NULL,
    prerequisite_id integer,
    prescription_id integer,
    target_id integer NOT NULL
);


ALTER TABLE public.am_oasis_test_assertion OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_test_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_test_assertion_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion_tag; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_test_assertion_tag (
    test_assertion_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.am_oasis_test_assertion_tag OWNER TO gazelle;

--
-- Name: am_oasis_test_assertion_variable; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_test_assertion_variable (
    test_assertion_id integer NOT NULL,
    variable_id integer NOT NULL
);


ALTER TABLE public.am_oasis_test_assertion_variable OWNER TO gazelle;

--
-- Name: am_oasis_text_source_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_text_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_text_source_id_seq OWNER TO gazelle;

--
-- Name: am_oasis_text_source_item; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_text_source_item (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text,
    name character varying(255)
);


ALTER TABLE public.am_oasis_text_source_item OWNER TO gazelle;

--
-- Name: am_oasis_variable; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.am_oasis_variable (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    content text,
    language integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.am_oasis_variable OWNER TO gazelle;

--
-- Name: am_oasis_variable_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.am_oasis_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_oasis_variable_id_seq OWNER TO gazelle;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_configuration (
    id integer NOT NULL,
    comment character varying(255),
    approved boolean NOT NULL,
    is_secured boolean,
    host_id integer
);


ALTER TABLE public.cfg_configuration OWNER TO gazelle;

--
-- Name: cfg_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scp_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scp_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scp_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scp_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scu_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scu_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scu_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scu_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_out integer,
    port_secure integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_host; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_host (
    id integer NOT NULL,
    alias character varying(255),
    comment character varying(255),
    hostname character varying(255) NOT NULL,
    ip character varying(255)
);


ALTER TABLE public.cfg_host OWNER TO gazelle;

--
-- Name: cfg_host_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_host_id_seq OWNER TO gazelle;

--
-- Name: cfg_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_sop_class (
    id integer NOT NULL,
    keyword character varying(255) NOT NULL,
    name character varying(255)
);


ALTER TABLE public.cfg_sop_class OWNER TO gazelle;

--
-- Name: cfg_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_sop_class_id_seq OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_syslog_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_syslog_configuration OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_syslog_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_syslog_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_web_service_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    assigning_authority character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_web_service_configuration OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_web_service_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_web_service_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_message_instance (
    id integer NOT NULL,
    content bytea,
    issuer character varying(255),
    type character varying(255),
    validation_detailed_result bytea,
    validation_status character varying(255),
    issuing_actor integer
);


ALTER TABLE public.cmn_message_instance OWNER TO gazelle;

--
-- Name: cmn_message_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_message_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_message_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_transaction_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_transaction_instance (
    id integer NOT NULL,
    "timestamp" timestamp without time zone,
    username character varying(255),
    is_visible boolean,
    domain_id integer,
    request_id integer,
    response_id integer,
    simulated_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.cmn_transaction_instance OWNER TO gazelle;

--
-- Name: cmn_transaction_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_transaction_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_transaction_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE public.cmn_validator_usage OWNER TO gazelle;

--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255),
    path integer NOT NULL
);


ALTER TABLE public.gs_contextual_information OWNER TO gazelle;

--
-- Name: gs_contextual_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information_instance (
    id integer NOT NULL,
    form character varying(255),
    value character varying(255),
    contextual_information_id integer NOT NULL,
    test_steps_instance_id integer NOT NULL
);


ALTER TABLE public.gs_contextual_information_instance OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_message (
    id integer NOT NULL,
    message_content bytea,
    message_type_id character varying(255),
    time_stamp timestamp without time zone,
    test_instance_participants_receiver_id integer,
    test_instance_participants_sender_id integer,
    transaction_id integer
);


ALTER TABLE public.gs_message OWNER TO gazelle;

--
-- Name: gs_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_message_id_seq OWNER TO gazelle;

--
-- Name: gs_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_system (
    id integer NOT NULL,
    institution_keyword character varying(255),
    keyword character varying(255),
    system_owner character varying(255)
);


ALTER TABLE public.gs_system OWNER TO gazelle;

--
-- Name: gs_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_system_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance (
    id integer NOT NULL,
    server_test_instance_id character varying(255) NOT NULL,
    test_instance_status_id integer
);


ALTER TABLE public.gs_test_instance OWNER TO gazelle;

--
-- Name: gs_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_oid (
    oid_configuration_id integer NOT NULL,
    test_instance_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_oid OWNER TO gazelle;

--
-- Name: gs_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_participants (
    id integer NOT NULL,
    server_aipo_id character varying(255) NOT NULL,
    server_test_instance_participants_id character varying(255) NOT NULL,
    aipo_id integer,
    system_id integer
);


ALTER TABLE public.gs_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_instance_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_participants_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_status (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.gs_test_instance_status OWNER TO gazelle;

--
-- Name: gs_test_instance_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_status_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_test_instance_participants (
    test_instance_id integer NOT NULL,
    test_instance_participants_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_steps_instance (
    id integer NOT NULL,
    dicom_scp_config_id integer,
    dicom_scu_config_id integer,
    hl7v2_initiator_config_id integer,
    hl7v2_responder_config_id integer,
    hl7v3_initiator_config_id integer,
    hl7v3_responder_config_id integer,
    syslog_config_id integer,
    testinstance_id integer,
    web_service_config_id integer
);


ALTER TABLE public.gs_test_steps_instance OWNER TO gazelle;

--
-- Name: gs_test_steps_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_steps_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_steps_instance_id_seq OWNER TO gazelle;

--
-- Name: sys_conf_type_usages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_conf_type_usages (
    system_configuration_id integer NOT NULL,
    listusages_id integer NOT NULL
);


ALTER TABLE public.sys_conf_type_usages OWNER TO gazelle;

--
-- Name: system_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.system_configuration (
    id integer NOT NULL,
    is_available boolean,
    is_public boolean,
    name character varying(255),
    owner character varying(255),
    system_name character varying(255),
    url character varying(255) NOT NULL
);


ALTER TABLE public.system_configuration OWNER TO gazelle;

--
-- Name: system_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.system_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_configuration_id_seq OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    can_act_as_responder boolean
);


ALTER TABLE public.tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile (
    id integer NOT NULL,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option (
    id integer NOT NULL,
    actor_integration_profile_id integer NOT NULL,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_integration_profiles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_integration_profiles (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_domain_integration_profiles OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tm_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    system_id integer
);


ALTER TABLE public.tm_oid OWNER TO gazelle;

--
-- Name: tm_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_id_seq OWNER TO gazelle;

--
-- Name: tm_path; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_path (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL,
    type character varying(255)
);


ALTER TABLE public.tm_path OWNER TO gazelle;

--
-- Name: tm_path_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_path_id_seq OWNER TO gazelle;

--
-- Name: usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usage_id_seq OWNER TO gazelle;

--
-- Name: usage_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usage_metadata (
    id integer NOT NULL,
    action character varying(255),
    keyword character varying(255),
    affinity_id integer,
    transaction_id integer
);


ALTER TABLE public.usage_metadata OWNER TO gazelle;

--
-- Name: affinity_domain affinity_domain_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT affinity_domain_keyword_key UNIQUE (keyword);


--
-- Name: affinity_domain affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: affinity_domain_transactions affinity_domain_transactions_affinity_domain_id_transaction_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT affinity_domain_transactions_affinity_domain_id_transaction_key UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: am_assertion_scope am_assertion_scope_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope
    ADD CONSTRAINT am_assertion_scope_pkey PRIMARY KEY (id);


--
-- Name: am_assertions_applies_to am_assertions_applies_to_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_applies_to
    ADD CONSTRAINT am_assertions_applies_to_pkey PRIMARY KEY (id);


--
-- Name: am_assertions_links_mbv am_assertions_links_mbv_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_links_mbv
    ADD CONSTRAINT am_assertions_links_mbv_pkey PRIMARY KEY (id);


--
-- Name: am_assertions_links am_assertions_links_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_links
    ADD CONSTRAINT am_assertions_links_pkey PRIMARY KEY (id);


--
-- Name: am_mbv_coverage_status am_mbv_coverage_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_mbv_coverage_status
    ADD CONSTRAINT am_mbv_coverage_status_pkey PRIMARY KEY (id);


--
-- Name: am_mbv_service am_mbv_service_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_mbv_service
    ADD CONSTRAINT am_mbv_service_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_comment am_oasis_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_comment
    ADD CONSTRAINT am_oasis_comment_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_derived_source_item am_oasis_derived_source_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_derived_source_item
    ADD CONSTRAINT am_oasis_derived_source_item_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_description am_oasis_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_description
    ADD CONSTRAINT am_oasis_description_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_interpretation am_oasis_interpretation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_interpretation
    ADD CONSTRAINT am_oasis_interpretation_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_normative_source am_oasis_normative_source_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source
    ADD CONSTRAINT am_oasis_normative_source_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_predicate am_oasis_predicate_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_predicate
    ADD CONSTRAINT am_oasis_predicate_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_prerequisite am_oasis_prerequisite_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_prerequisite
    ADD CONSTRAINT am_oasis_prerequisite_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_prescription am_oasis_prescription_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_prescription
    ADD CONSTRAINT am_oasis_prescription_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_ref_source_item am_oasis_ref_source_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_ref_source_item
    ADD CONSTRAINT am_oasis_ref_source_item_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_tag am_oasis_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_tag
    ADD CONSTRAINT am_oasis_tag_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_target am_oasis_target_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_target
    ADD CONSTRAINT am_oasis_target_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_test_assertion am_oasis_test_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT am_oasis_test_assertion_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_text_source_item am_oasis_text_source_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_text_source_item
    ADD CONSTRAINT am_oasis_text_source_item_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_variable am_oasis_variable_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_variable
    ADD CONSTRAINT am_oasis_variable_pkey PRIMARY KEY (id);


--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_configuration cfg_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT cfg_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scp_configuration cfg_dicom_scp_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT cfg_dicom_scp_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scu_configuration cfg_dicom_scu_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT cfg_dicom_scu_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_initiator_configuration cfg_hl7_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT cfg_hl7_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_responder_configuration cfg_hl7_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT cfg_hl7_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_initiator_configuration cfg_hl7_v3_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT cfg_hl7_v3_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_responder_configuration cfg_hl7_v3_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT cfg_hl7_v3_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_host cfg_host_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_host
    ADD CONSTRAINT cfg_host_pkey PRIMARY KEY (id);


--
-- Name: cfg_sop_class cfg_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_sop_class
    ADD CONSTRAINT cfg_sop_class_pkey PRIMARY KEY (id);


--
-- Name: cfg_syslog_configuration cfg_syslog_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT cfg_syslog_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_web_service_configuration cfg_web_service_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT cfg_web_service_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_iso3_language_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_iso3_language_key UNIQUE (iso3_language);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance cmn_message_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT cmn_message_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_transaction_instance cmn_transaction_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT cmn_transaction_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_validator_usage cmn_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_validator_usage
    ADD CONSTRAINT cmn_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information_instance gs_contextual_information_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT gs_contextual_information_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information gs_contextual_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT gs_contextual_information_pkey PRIMARY KEY (id);


--
-- Name: gs_message gs_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT gs_message_pkey PRIMARY KEY (id);


--
-- Name: gs_system gs_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_system
    ADD CONSTRAINT gs_system_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_server_aipo_id_server_test_in_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_server_aipo_id_server_test_in_key UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_server_test_instance_particip_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_server_test_instance_particip_key UNIQUE (server_test_instance_participants_id);


--
-- Name: gs_test_instance gs_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT gs_test_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance gs_test_instance_server_test_instance_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT gs_test_instance_server_test_instance_id_key UNIQUE (server_test_instance_id);


--
-- Name: gs_test_instance_status gs_test_instance_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_status
    ADD CONSTRAINT gs_test_instance_status_pkey PRIMARY KEY (id);


--
-- Name: gs_test_steps_instance gs_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT gs_test_steps_instance_pkey PRIMARY KEY (id);


--
-- Name: system_configuration system_configuration_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT system_configuration_name_key UNIQUE (name);


--
-- Name: system_configuration system_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT system_configuration_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile__actor_integration_profile_id__key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile__actor_integration_profile_id__key UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_actor_id_integration_profile_i_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_actor_id_integration_profile_i_key UNIQUE (actor_id, integration_profile_id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_keyword_key UNIQUE (keyword);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_integration_profiles tf_domain_integration_profile_integration_profile_id_domain_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT tf_domain_integration_profile_integration_profile_id_domain_key UNIQUE (integration_profile_id, domain_id);


--
-- Name: tf_domain tf_domain_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_keyword_key UNIQUE (keyword);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_keyword_key UNIQUE (keyword);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tm_oid tm_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT tm_oid_pkey PRIMARY KEY (id);


--
-- Name: tm_path tm_path_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_keyword_key UNIQUE (keyword);


--
-- Name: tm_path tm_path_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_pkey PRIMARY KEY (id);


--
-- Name: am_oasis_target uk_1vssy2suovafv9tawkwjs54f; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_target
    ADD CONSTRAINT uk_1vssy2suovafv9tawkwjs54f UNIQUE (idscheme);


--
-- Name: am_assertion_scope uk_6ohvgpk4ah0v450488p1cj6kt; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope
    ADD CONSTRAINT uk_6ohvgpk4ah0v450488p1cj6kt UNIQUE (keyword);


--
-- Name: am_assertion_scope_links uk_9ws8yy0jvnmmchcgjgrp8j0vb; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope_links
    ADD CONSTRAINT uk_9ws8yy0jvnmmchcgjgrp8j0vb UNIQUE (father_scope_id, child_scope_id);


--
-- Name: am_assertion_scope_to_test_assertions uk_eqf9ea1wu1ebnpcsvlpp0f0se; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope_to_test_assertions
    ADD CONSTRAINT uk_eqf9ea1wu1ebnpcsvlpp0f0se UNIQUE (scope_id, assertion_id);


--
-- Name: am_assertions_links uk_fc93bfebnins6oirceany09w7; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_links
    ADD CONSTRAINT uk_fc93bfebnins6oirceany09w7 UNIQUE (linked_entity, linked_entity_keyword, assertion_id);


--
-- Name: am_assertions_links_mbv uk_flxkvupemx0gnjic835qxhr3i; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_links_mbv
    ADD CONSTRAINT uk_flxkvupemx0gnjic835qxhr3i UNIQUE (assertion_id, mbv_service_id);


--
-- Name: am_mbv_service uk_g57jqjild606duris9s250gtf; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_mbv_service
    ADD CONSTRAINT uk_g57jqjild606duris9s250gtf UNIQUE (url);


--
-- Name: am_oasis_test_assertion uk_h93e94njivitiij0y7r2wtcn4; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT uk_h93e94njivitiij0y7r2wtcn4 UNIQUE (assertion_id, target_id);


--
-- Name: am_assertions_applies_to uk_ivtsdnyexiit6t9oxn8l1wbvq; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_applies_to
    ADD CONSTRAINT uk_ivtsdnyexiit6t9oxn8l1wbvq UNIQUE (linked_entity, linked_entity_keyword, assertion_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: am_mbv_service uk_tf5r4dwdnkc8as75op12jns25; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_mbv_service
    ADD CONSTRAINT uk_tf5r4dwdnkc8as75op12jns25 UNIQUE (name);


--
-- Name: usage_metadata usage_metadata_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT usage_metadata_keyword_key UNIQUE (keyword);


--
-- Name: usage_metadata usage_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT usage_metadata_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_oid fk200bd51aadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk200bd51aadaef596 FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_instance_oid fk200bd51afb5a2c1c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk200bd51afb5a2c1c FOREIGN KEY (oid_configuration_id) REFERENCES public.tm_oid(id);


--
-- Name: cfg_web_service_configuration fk23f4a6263927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a6263927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_web_service_configuration fk23f4a626511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a626511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea431b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea431b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea43866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea43866df480 FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_contextual_information fk4a7365f13b128c1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT fk4a7365f13b128c1 FOREIGN KEY (path) REFERENCES public.tm_path(id);


--
-- Name: gs_test_instance_participants fk5c650a50611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk5c650a50611bae11 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_test_instance_participants fk5c650a5083369963; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk5c650a5083369963 FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde4f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde4f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d698ea7bd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d698ea7bd FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d72619921; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d72619921 FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: usage_metadata fk7d18f40d7007d3ad; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk7d18f40d7007d3ad FOREIGN KEY (affinity_id) REFERENCES public.affinity_domain(id);


--
-- Name: usage_metadata fk7d18f40dbd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk7d18f40dbd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: cfg_configuration fk7d98485b9a8e05a9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT fk7d98485b9a8e05a9 FOREIGN KEY (host_id) REFERENCES public.cfg_host(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: am_oasis_test_assertion_variable fk_2pcp643jgq3myrobskdp5m6i6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion_variable
    ADD CONSTRAINT fk_2pcp643jgq3myrobskdp5m6i6 FOREIGN KEY (test_assertion_id) REFERENCES public.am_oasis_test_assertion(id);


--
-- Name: am_oasis_normative_source_text_source_item fk_3bgw2ebmmspsi0ed6ud6d8er9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source_text_source_item
    ADD CONSTRAINT fk_3bgw2ebmmspsi0ed6ud6d8er9 FOREIGN KEY (normative_source_id) REFERENCES public.am_oasis_normative_source(id);


--
-- Name: am_assertions_links_mbv fk_4c3d2xyqx98q9q9j6m6c04a2q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_links_mbv
    ADD CONSTRAINT fk_4c3d2xyqx98q9q9j6m6c04a2q FOREIGN KEY (mbv_service_id) REFERENCES public.am_mbv_service(id);


--
-- Name: am_oasis_test_assertion_variable fk_602ul80fydna4e0m0ukf5wg5k; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion_variable
    ADD CONSTRAINT fk_602ul80fydna4e0m0ukf5wg5k FOREIGN KEY (variable_id) REFERENCES public.am_oasis_variable(id);


--
-- Name: am_oasis_test_assertion fk_6ox5kh21rev7ku73ko8xsqf2u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT fk_6ox5kh21rev7ku73ko8xsqf2u FOREIGN KEY (normative_source_id) REFERENCES public.am_oasis_normative_source(id);


--
-- Name: am_oasis_test_assertion_tag fk_77buu5a1952i2p5u3ao6y4vm2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion_tag
    ADD CONSTRAINT fk_77buu5a1952i2p5u3ao6y4vm2 FOREIGN KEY (test_assertion_id) REFERENCES public.am_oasis_test_assertion(id);


--
-- Name: am_oasis_test_assertion fk_9b9ci95ylve8akjup3809aqf1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT fk_9b9ci95ylve8akjup3809aqf1 FOREIGN KEY (prerequisite_id) REFERENCES public.am_oasis_prerequisite(id);


--
-- Name: am_oasis_test_assertion fk_bje3qi7crkexvl917ve8o6w0w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT fk_bje3qi7crkexvl917ve8o6w0w FOREIGN KEY (target_id) REFERENCES public.am_oasis_target(id);


--
-- Name: am_oasis_test_assertion fk_ccumi2ndjgnwu29t3fima1axi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT fk_ccumi2ndjgnwu29t3fima1axi FOREIGN KEY (prescription_id) REFERENCES public.am_oasis_prescription(id);


--
-- Name: am_assertions_links fk_cw6r0pi6xgi7m5hvulcm5fky; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_links
    ADD CONSTRAINT fk_cw6r0pi6xgi7m5hvulcm5fky FOREIGN KEY (assertion_id) REFERENCES public.am_oasis_test_assertion(id);


--
-- Name: am_oasis_normative_source_ref_source_item fk_e2po5bov4outrvrr0mbe33a3m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source_ref_source_item
    ADD CONSTRAINT fk_e2po5bov4outrvrr0mbe33a3m FOREIGN KEY (ref_source_item_id) REFERENCES public.am_oasis_ref_source_item(id);


--
-- Name: am_mbv_coverage_status fk_fc523gtrfmdp738715mhnmxkr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_mbv_coverage_status
    ADD CONSTRAINT fk_fc523gtrfmdp738715mhnmxkr FOREIGN KEY (mbv_service_id) REFERENCES public.am_mbv_service(id);


--
-- Name: am_assertions_links_mbv fk_fkd5dtq7m92fv5al8xq4o3l5y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_links_mbv
    ADD CONSTRAINT fk_fkd5dtq7m92fv5al8xq4o3l5y FOREIGN KEY (assertion_id) REFERENCES public.am_oasis_test_assertion(id);


--
-- Name: am_assertion_scope_links fk_fq17xls72e32eq65jyt91s5o6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope_links
    ADD CONSTRAINT fk_fq17xls72e32eq65jyt91s5o6 FOREIGN KEY (child_scope_id) REFERENCES public.am_assertion_scope(id);


--
-- Name: am_oasis_normative_source_derived_source_item fk_ia6nenm7cldvjdefof0r9rp7j; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source_derived_source_item
    ADD CONSTRAINT fk_ia6nenm7cldvjdefof0r9rp7j FOREIGN KEY (derived_source_item_id) REFERENCES public.am_oasis_derived_source_item(id);


--
-- Name: am_oasis_normative_source_derived_source_item fk_ji6dadvakpdgclae4c13hl159; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source_derived_source_item
    ADD CONSTRAINT fk_ji6dadvakpdgclae4c13hl159 FOREIGN KEY (normative_source_id) REFERENCES public.am_oasis_normative_source(id);


--
-- Name: am_oasis_normative_source_text_source_item fk_k9h6xhjs0fesyq13y9juh28i8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source_text_source_item
    ADD CONSTRAINT fk_k9h6xhjs0fesyq13y9juh28i8 FOREIGN KEY (text_source_item_id) REFERENCES public.am_oasis_text_source_item(id);


--
-- Name: am_assertions_applies_to fk_khyhbaqmrlw98prkxbr6w0olm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertions_applies_to
    ADD CONSTRAINT fk_khyhbaqmrlw98prkxbr6w0olm FOREIGN KEY (assertion_id) REFERENCES public.am_oasis_test_assertion(id);


--
-- Name: am_assertion_scope_to_test_assertions fk_lw4s2nqfcauh39s361lep57od; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope_to_test_assertions
    ADD CONSTRAINT fk_lw4s2nqfcauh39s361lep57od FOREIGN KEY (scope_id) REFERENCES public.am_assertion_scope(id);


--
-- Name: am_oasis_test_assertion fk_mj0crffs3tur0ems0h7l09eel; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT fk_mj0crffs3tur0ems0h7l09eel FOREIGN KEY (predicate_id) REFERENCES public.am_oasis_predicate(id);


--
-- Name: am_assertion_scope_links fk_o7g86yj6tkhai7kyu6e7nl4vy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope_links
    ADD CONSTRAINT fk_o7g86yj6tkhai7kyu6e7nl4vy FOREIGN KEY (father_scope_id) REFERENCES public.am_assertion_scope(id);


--
-- Name: am_oasis_test_assertion fk_oc0kmows2eok6xyuwm1bsnomu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion
    ADD CONSTRAINT fk_oc0kmows2eok6xyuwm1bsnomu FOREIGN KEY (description_id) REFERENCES public.am_oasis_description(id);


--
-- Name: am_assertion_scope_to_test_assertions fk_qcr90qwm7xej0jb4tdbej7yrb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_assertion_scope_to_test_assertions
    ADD CONSTRAINT fk_qcr90qwm7xej0jb4tdbej7yrb FOREIGN KEY (assertion_id) REFERENCES public.am_oasis_test_assertion(id);


--
-- Name: am_oasis_test_assertion_tag fk_qfxn9bqy3ck31wulb260sby74; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_test_assertion_tag
    ADD CONSTRAINT fk_qfxn9bqy3ck31wulb260sby74 FOREIGN KEY (tag_id) REFERENCES public.am_oasis_tag(id);


--
-- Name: am_oasis_normative_source_ref_source_item fk_rphb0xmsbuwotynqxj3tf0ls4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source_ref_source_item
    ADD CONSTRAINT fk_rphb0xmsbuwotynqxj3tf0ls4 FOREIGN KEY (normative_source_id) REFERENCES public.am_oasis_normative_source(id);


--
-- Name: am_oasis_normative_source fk_s9hclg03dxsfcaq061ee0hnh2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source
    ADD CONSTRAINT fk_s9hclg03dxsfcaq061ee0hnh2 FOREIGN KEY (comment_id) REFERENCES public.am_oasis_comment(id);


--
-- Name: am_oasis_normative_source fk_sykbolfgbj8mjnsu0ruyfm3dr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.am_oasis_normative_source
    ADD CONSTRAINT fk_sykbolfgbj8mjnsu0ruyfm3dr FOREIGN KEY (interpretation_id) REFERENCES public.am_oasis_interpretation(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9993927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9993927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9994f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9994f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_dicom_scp_configuration fka165b999511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b999511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_syslog_configuration fka36479093927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fka36479093927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_syslog_configuration fka3647909511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fka3647909511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_message fka9c507b465385ec7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b465385ec7 FOREIGN KEY (test_instance_participants_receiver_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gs_message fka9c507b4bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b4bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_message fka9c507b4e0c3e041; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b4e0c3e041 FOREIGN KEY (test_instance_participants_sender_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cd63e3d0fb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cd63e3d0fb FOREIGN KEY (test_instance_participants_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cdadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cdadaef596 FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_instance fkc783578f43e9dc7b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT fkc783578f43e9dc7b FOREIGN KEY (test_instance_status_id) REFERENCES public.gs_test_instance_status(id);


--
-- Name: tm_oid fkcc1f0704611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT fkcc1f0704611bae11 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: tf_actor_integration_profile fkd5a41967866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fkd5a41967866df480 FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_actor_integration_profile fkd5a419679d1084ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fkd5a419679d1084ab FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f11b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: cmn_transaction_instance fkd74918f11bc07e58; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11bc07e58 FOREIGN KEY (response_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f172ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f172ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f1afd7554a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1afd7554a FOREIGN KEY (request_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f1bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: sys_conf_type_usages fkd97ede2e5e9a5b69; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2e5e9a5b69 FOREIGN KEY (system_configuration_id) REFERENCES public.system_configuration(id);


--
-- Name: sys_conf_type_usages fkd97ede2edd54bc19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2edd54bc19 FOREIGN KEY (listusages_id) REFERENCES public.usage_metadata(id);


--
-- Name: cfg_hl7_responder_configuration fkdc2545923927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc2545923927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_responder_configuration fkdc254592511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc254592511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_contextual_information_instance fke02d7f235a577540; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f235a577540 FOREIGN KEY (contextual_information_id) REFERENCES public.gs_contextual_information(id);


--
-- Name: gs_contextual_information_instance fke02d7f2383812bd3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f2383812bd3 FOREIGN KEY (test_steps_instance_id) REFERENCES public.gs_test_steps_instance(id);


--
-- Name: affinity_domain_transactions fke0a98f5972a3043a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fke0a98f5972a3043a FOREIGN KEY (affinity_domain_id) REFERENCES public.affinity_domain(id);


--
-- Name: affinity_domain_transactions fke0a98f59bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fke0a98f59bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_steps_instance fke1c7ae27146029f1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27146029f1 FOREIGN KEY (testinstance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_steps_instance fke1c7ae2724c9d386; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2724c9d386 FOREIGN KEY (hl7v3_responder_config_id) REFERENCES public.cfg_hl7_v3_responder_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae273202260; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae273202260 FOREIGN KEY (syslog_config_id) REFERENCES public.cfg_syslog_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2739f74269; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2739f74269 FOREIGN KEY (web_service_config_id) REFERENCES public.cfg_web_service_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27462961a4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27462961a4 FOREIGN KEY (hl7v2_initiator_config_id) REFERENCES public.cfg_hl7_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2753d7b3a7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2753d7b3a7 FOREIGN KEY (dicom_scp_config_id) REFERENCES public.cfg_dicom_scp_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae279cf3f066; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae279cf3f066 FOREIGN KEY (hl7v3_initiator_config_id) REFERENCES public.cfg_hl7_v3_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27a5a42187; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27a5a42187 FOREIGN KEY (dicom_scu_config_id) REFERENCES public.cfg_dicom_scu_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27cdff44c4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27cdff44c4 FOREIGN KEY (hl7v2_responder_config_id) REFERENCES public.cfg_hl7_responder_configuration(id);


--
-- Name: cmn_message_instance fkfeccef683360a4d2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT fkfeccef683360a4d2 FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- PostgreSQL database dump complete
--

