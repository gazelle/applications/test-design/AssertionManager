function checkAllCheckboxesInTable(inputId, state) {
    var tableId = inputId.substr(0, inputId.lastIndexOf(':'));
    var tableElement = document.getElementById(tableId);
    var inputs = tableElement.getElementsByTagName('input');
    for (var i = 0; i <= inputs.length; i++) {
        var input = inputs[i];
        if (input.getAttribute('type') === 'checkbox' && state) {
            input.setAttribute('checked', state);
        } else {
            input.removeAttribute('checked');
        }
    }
}
