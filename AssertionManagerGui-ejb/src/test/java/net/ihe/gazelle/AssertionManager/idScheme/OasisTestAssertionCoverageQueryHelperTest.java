package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.assertion.coverage.factories.AssertionLinkedToTestFactory;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTest;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTestStep;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * <b>Class Description : </b>OasisTestAssertionCoverageQueryHelperTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 29/09/16
 * @class OasisTestAssertionCoverageQueryHelperTest
 * @package net.ihe.gazelle.AssertionManager.idScheme
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Ignore
public class OasisTestAssertionCoverageQueryHelperTest extends AbstractTestQueryJunit4 {
    private OasisTarget target;

    protected String getDb() {
        return "assertion-manager-junit";
    }

    protected boolean getShowSql() {
        return false;
    }

    protected String getHbm2ddl() {
        return "create-drop";
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    @After
    public void tearDown() throws Exception {
        AssertionLinkedToTestFactory.cleanAssertionLinkedToTest();
        OasisTestAssertionFactory.cleanOasisTestAssertions();
        super.tearDown();
    }

    private void setupNotDeprecatedAssertionLink() {
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }



        OasisTestAssertion coveredAssertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        AssertionLinkedToTest testLink = new AssertionLinkedToTest();
        testLink.setAssertion(coveredAssertion);
        testLink.setLinkedEntityKeyword("1");
        DatabaseManager.writeObject(testLink);
        assertFalse(testLink.isDeprecated());

        target = coveredAssertion.getTarget();

        OasisTestAssertion coveredAssertion2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        coveredAssertion2.setTarget(target);
        DatabaseManager.writeObject(coveredAssertion2);

        testLink = new AssertionLinkedToTest();
        testLink.setAssertion(coveredAssertion2);
        testLink.setLinkedEntityKeyword("2");
        DatabaseManager.writeObject(testLink);

        OasisTestAssertion notCoveredAssertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        notCoveredAssertion.setTarget(target);
        DatabaseManager.writeObject(notCoveredAssertion);

        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    private void setupNotTestableAssertion() {
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
        OasisTestAssertion testAssertionWithMandatoryFields = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        target = testAssertionWithMandatoryFields.getTarget();

        OasisTestAssertion testAssertionWithMandatoryFields2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        testAssertionWithMandatoryFields2.setTarget(target);
        testAssertionWithMandatoryFields2.setTestable(false);
        DatabaseManager.writeObject(testAssertionWithMandatoryFields2);

        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    private void setupDeprecatedAssertionLink() {
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
        OasisTestAssertion testAssertionWithMandatoryFields = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        AssertionLinkedToTest testLink = new AssertionLinkedToTest();
        testLink.setAssertion(testAssertionWithMandatoryFields);
        testLink.setLinkedEntityKeyword("1");
        DatabaseManager.writeObject(testLink);

        assertFalse(testLink.isDeprecated());

        target = testAssertionWithMandatoryFields.getTarget();

        OasisTestAssertion testAssertionWithMandatoryFields2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        testAssertionWithMandatoryFields2.setTarget(target);
        DatabaseManager.writeObject(testAssertionWithMandatoryFields2);

        testLink = new AssertionLinkedToTest();
        testLink.setAssertion(testAssertionWithMandatoryFields2);
        testLink.setLinkedEntityKeyword("2");

        testLink.setDeprecated(true);

        DatabaseManager.writeObject(testLink);

        OasisTestAssertion notCoveredAssertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        notCoveredAssertion.setTarget(target);
        DatabaseManager.writeObject(notCoveredAssertion);

        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    @Test
    public void countCoveredAssertions() throws Exception {
        setupNotDeprecatedAssertionLink();
        OasisTestAssertionCoverageQueryHelper q = new OasisTestAssertionCoverageQueryHelper(target.getIdScheme(), null);
        assertEquals(2, q.countCoveredAssertions());
    }

    @Test
    public void countNotTestableAssertions() throws Exception {
        setupNotTestableAssertion();
        OasisTestAssertionCoverageQueryHelper q = new OasisTestAssertionCoverageQueryHelper(target.getIdScheme(), null);
        assertEquals(1, q.countNotTestableAssertions());
    }

    @Test
    public void countAssertionCoveredThroughTests() throws Exception {
        setupNotDeprecatedAssertionLink();
        OasisTestAssertionCoverageQueryHelper q = new OasisTestAssertionCoverageQueryHelper(target.getIdScheme(), null);
        assertEquals(2, q.countAssertionCoveredThroughTests());
    }

    @Test
    public void countAssertionCoveredThroughTestsExcludeDeprecated() throws Exception {
        setupDeprecatedAssertionLink();
        OasisTestAssertionCoverageQueryHelper q = new OasisTestAssertionCoverageQueryHelper(target.getIdScheme(), null);
        assertEquals(1, q.countAssertionCoveredThroughTests());
    }

    @Test
    public void countCoveredAssertionsExcludeDeprecated() throws Exception {
        setupDeprecatedAssertionLink();
        OasisTestAssertionCoverageQueryHelper q = new OasisTestAssertionCoverageQueryHelper(target.getIdScheme(), null);
        assertEquals(1, q.countCoveredAssertions());
    }
}
