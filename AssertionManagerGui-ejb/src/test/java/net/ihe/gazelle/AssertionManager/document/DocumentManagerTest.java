package net.ihe.gazelle.AssertionManager.document;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.assertion.factories.OasisNormativeSourceFactory;
import net.ihe.gazelle.oasis.assertion.factories.OasisRefSourceItemFactory;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.OasisNormativeSource;
import net.ihe.gazelle.oasis.testassertion.OasisNormativeSourceQuery;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItemQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * <b>Class Description : </b>DocumentManagerTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 * @class DocumentManagerTest
 * @package net.ihe.gazelle.AssertionManager.document
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Ignore
//Those tests are ignored because the dependency simulator-common-ejb makes it fail
//I cannot find a way to include simulator-common-ejb and be able to run tests database dependent
public class DocumentManagerTest extends AssertionManagerTestQueryJunit4 {

    @Before
    public void setUp() throws Exception {
        super.setUp();
        getTransaction();

    }

    private void getTransaction() {
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    @After
    public void tearDown() throws Exception {
        cleanUp();
        super.tearDown();
    }

    private void cleanUp() {
        OasisNormativeSourceFactory.cleanOasisNormativeSource();
        OasisRefSourceItemFactory.cleanOasisRefSourceItem();
        OasisTestAssertionFactory.cleanOasisTestAssertions();
    }

    @Test
    public void deleteOasisRefSourceItemNotLinkedToNormativeSource() throws Exception {
        assertEquals(0, getOasisRefSourceItemTableSize());
        OasisRefSourceItem refSourceItemWithMandatoryFields = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        assertEquals(1, getOasisRefSourceItemTableSize());
        getTransaction();
        DocumentManager dm = new DocumentManager();
        dm.setSelectedDocument(refSourceItemWithMandatoryFields);
        dm.delete();
        assertEquals(0, getOasisRefSourceItemTableSize());
    }

    @Test
    public void deleteOasisRefSourceItemLinkedToNormativeSource() throws Exception {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        assertEquals(0, getOasisNormativeSourceQueryTableSize());
        assertEquals(0, getOasisRefSourceItemTableSize());
        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
        assertEquals(1, getOasisRefSourceItemTableSize());
        assertEquals(1, getOasisNormativeSourceQueryTableSize());

        ArrayList<OasisRefSourceItem> refSourceItems = new ArrayList<>();
        refSourceItems.add(refSourceItem);

        normativeSource.setRefSourceItems(refSourceItems);

        entityManager.merge(normativeSource);
        entityManager.flush();
        entityManager.refresh(refSourceItem);

        DocumentManager dm = new DocumentManager();
        dm.setSelectedDocument(refSourceItem);
        dm.delete();

        assertEquals(0, getOasisNormativeSourceQueryTableSize());
        assertEquals(0, getOasisRefSourceItemTableSize());
    }

    private int getOasisRefSourceItemTableSize() {
        OasisRefSourceItemQuery query = new OasisRefSourceItemQuery();
        return query.getCount();
    }

    private int getOasisNormativeSourceQueryTableSize() {
        OasisNormativeSourceQuery query = new OasisNormativeSourceQuery();
        return query.getCount();
    }

    protected boolean getShowSql() {
        return true;
    }

    protected String getHbm2ddl() {
        return "update";
    }


}
