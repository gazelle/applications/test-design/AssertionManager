package net.ihe.gazelle.AssertionManager.mbv;

import net.ihe.gazelle.AssertionManager.document.AssertionManagerTestQueryJunit4;
import net.ihe.gazelle.assertion.coverage.mbv.factories.MbvCoverageStatusFactory;
import net.ihe.gazelle.assertion.coverage.mbv.factories.MbvServiceFactory;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.MbvCoverageStatus;
import net.ihe.gazelle.oasis.testassertion.MbvCoverageStatusQuery;
import net.ihe.gazelle.oasis.testassertion.MbvService;
import net.ihe.gazelle.oasis.testassertion.MbvServiceQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;

import static org.junit.Assert.assertEquals;

@Ignore
public class MbvServiceRefresherTest extends AssertionManagerTestQueryJunit4 {

    private EntityManager em;
    private MbvService mbvService;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        em = EntityManagerService.provideEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        MbvServiceQuery query = new MbvServiceQuery();
        query.url().eq("httpS://gazelle.ihe.net/XDStarClient");
        query.keyword().eq("testing");
        mbvService = query.getUniqueResult();

        if (mbvService == null) {
            mbvService = new MbvService("testing", "httpS://gazelle.ihe.net/XDStarClient");
            mbvService = (MbvService) DatabaseManager.writeObject(mbvService);
        }

        MbvCoverageStatus mbvCovStatus = new MbvCoverageStatus(MbvCoverageStatus.CoverageStatusEnum.Covered, "idScheme", "assert-id", mbvService);
        em.merge(mbvCovStatus);
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.flush();
        em.setFlushMode(FlushModeType.COMMIT);
    }

    @After
    public void tearDown() throws Exception {
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.flush();
        em.getTransaction().commit();
        cleanData();

        super.tearDown();
    }

    private void cleanData() {
        MbvCoverageStatusFactory.cleanMbvCoverageStatus();
        MbvServiceFactory.cleanMbvService();
    }

    @Override
    protected String getDb() {
        return "assertion-manager-junit";
    }

    @Override
    protected String getHbm2ddl() {
        return "update";
    }

    protected boolean getShowSql() {
        return false;
    }

    @Test
    public void removePreviousStatus() {
        MbvCoverageStatusQuery query = new MbvCoverageStatusQuery();
        query.mbvService().id().eq(mbvService.getId());
        int size = query.getListDistinct().size();
        assertEquals(1, size);

        MbvServiceRefresher refresher = new MbvServiceRefresher();
        refresher.setMbvService(mbvService);
        refresher.removePreviousStatus();

        query = new MbvCoverageStatusQuery();
        query.mbvService().id().eq(mbvService.getId());
        size = query.getListDistinct().size();
        assertEquals(0, size);
    }

}
