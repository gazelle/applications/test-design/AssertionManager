package net.ihe.gazelle.AssertionManager.assertion.link.coverage;

import net.ihe.gazelle.AssertionManager.document.AssertionManagerTestQueryJunit4;
import net.ihe.gazelle.assertion.coverage.factories.AssertionLinkedToTestFactory;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTest;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tm.ws.data.TestStepWrapper;
import net.ihe.gazelle.tm.ws.data.TestWrapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.assertEquals;

/**
 * <b>Class Description : </b>TestLinkRefresherTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 28/09/16
 * @class TestLinkRefresherTest
 * @package net.ihe.gazelle.AssertionManager.assertion.link.coverage
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Ignore
public class TestLinkRefresherTest extends AssertionManagerTestQueryJunit4 {
    private List<String> linkedEntities = new ArrayList<>();
    private List<TestWrapper> remoteEntities = new ArrayList<>();
    private List<TestStepWrapper> remoteTestSteps = new ArrayList<>();

    private AssertionLinkedToTest assertionLinkedToTest;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    private void setupAssertionLink(boolean isDeprecated, int willbeDeprecated) {
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
        OasisTestAssertion testAssertionWithMandatoryFields = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        AssertionLinkedToTest testLink = new AssertionLinkedToTest();
        testLink.setAssertion(testAssertionWithMandatoryFields);
        testLink.setLinkedEntityKeyword("0");
        testLink.setDeprecated(isDeprecated);

        assertionLinkedToTest = (AssertionLinkedToTest) DatabaseManager.writeObject(testLink);

        linkedEntities.add(testLink.getLinkedEntityKeyword());

        remoteEntities.add(new TestWrapper("remote", willbeDeprecated));
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    private void setupNotDeprecatedAssertionLinkStayingNotDeprecated() {
        setupAssertionLink(false,0);
    }

    private void setupNotDeprecatedAssertionLinkBecomingDeprecated() {
        setupAssertionLink(false,1);
    }

    private void setupDeprecatedAssertionLinkStayingDeprecated() {
        setupAssertionLink(true,1);

    }

    private void setupDeprecatedAssertionLinkBecomingNotDeprecated() {
        setupAssertionLink(true,0);
    }

    @After
    public void tearDown() throws Exception {
        AssertionLinkedToTestFactory.cleanAssertionLinkedToTest();
        OasisTestAssertionFactory.cleanOasisTestAssertions();
        super.tearDown();
    }

    @Test
    public void factoryIsWorking() throws Exception {
        setupNotDeprecatedAssertionLinkBecomingDeprecated();
        assertEquals(false, assertionLinkedToTest.isDeprecated());
    }

    @Test
    public void markNewlyDeprecatedTestsAsDeprecated() throws Exception {
        setupNotDeprecatedAssertionLinkBecomingDeprecated();
        computeRefresh();
        assertEquals(true, assertionLinkedToTest.isDeprecated());
    }

    @Test
    public void markNewlyNotDeprecatedTestsAsNotDeprecated() throws Exception {
        setupDeprecatedAssertionLinkBecomingNotDeprecated();
        computeRefresh();
        assertEquals(false, assertionLinkedToTest.isDeprecated());
    }

    @Test
    public void stillDeprecatedTestStaysDeprecated() throws Exception {
        setupDeprecatedAssertionLinkStayingDeprecated();
        computeRefresh();
        assertEquals(true, assertionLinkedToTest.isDeprecated());
    }

    @Test
    public void stillNotDeprecatedTestStaysNotDeprecated() throws Exception {
        setupNotDeprecatedAssertionLinkStayingNotDeprecated();
        computeRefresh();
        assertEquals(false, assertionLinkedToTest.isDeprecated());
    }

    private void computeRefresh() {
        TestLinkRefresher tlr = new TestLinkRefresher();
        tlr.refresh
                (linkedEntities, remoteEntities);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.flush();

        entityManager.refresh(assertionLinkedToTest);
    }
}