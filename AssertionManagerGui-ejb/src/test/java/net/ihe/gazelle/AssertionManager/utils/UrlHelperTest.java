package net.ihe.gazelle.AssertionManager.utils;

import net.ihe.gazelle.AssertionManager.idScheme.IdSchemesFilter;
import org.apache.commons.collections.map.LinkedMap;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class UrlHelperTest {

    @Test
    public void concat_tow_urls_with_trailing_slash() {
        String result = UrlHelper.concatURLtoPath("http://localhost/rest/", "assertion");
        assertEquals("http://localhost/rest/assertion", result);
    }

    @Test
    public void concat_tow_urls_without_trailing_slash() {
        String result = UrlHelper.concatURLtoPath("http://localhost/rest", "assertion");
        assertEquals("http://localhost/rest/assertion", result);
    }

    @Test
    public void concat_tow_urls_without_trailing_and_beginning_slash() {
        String result = UrlHelper.concatURLtoPath("http://localhost/rest", "/assertion");
        assertEquals("http://localhost/rest/assertion", result);
    }

    @Test
    public void concat_tow_urls_with_trailing_and_beginning_slash() {
        String result = UrlHelper.concatURLtoPath("http://localhost/rest/", "/assertion");
        assertEquals("http://localhost/rest/assertion", result);
    }
}
