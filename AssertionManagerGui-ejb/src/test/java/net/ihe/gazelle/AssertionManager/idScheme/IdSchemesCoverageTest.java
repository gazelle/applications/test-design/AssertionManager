package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTest;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * <b>Class Description : </b>IdSchemesCoverageTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 29/09/16
 * @class IdSchemesCoverageTest
 * @package net.ihe.gazelle.AssertionManager.idScheme
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Ignore
public class IdSchemesCoverageTest extends AbstractTestQueryJunit4 {
    private OasisTarget target;

    protected String getDb() {
        return "assertion-manager-junit";
    }

    protected boolean getShowSql() {
        return false;
    }

    protected String getHbm2ddl() {
        return "create-drop";
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }


    private void setupNotDeprecatedAssertionLink() {
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
        OasisTestAssertion testAssertionWithMandatoryFields = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        AssertionLinkedToTest testLink = new AssertionLinkedToTest();
        testLink.setAssertion(testAssertionWithMandatoryFields);
        testLink.setLinkedEntityKeyword("1");
        testLink.setDeprecated(false);
        DatabaseManager.writeObject(testLink);

        target = testAssertionWithMandatoryFields.getTarget();

        OasisTestAssertion testAssertionWithMandatoryFields2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        testAssertionWithMandatoryFields2.setTarget(target);
        DatabaseManager.writeObject(testAssertionWithMandatoryFields2);

        testLink = new AssertionLinkedToTest();
        testLink.setAssertion(testAssertionWithMandatoryFields2);
        testLink.setLinkedEntityKeyword("2");
        testLink.setDeprecated(false);
        DatabaseManager.writeObject(testLink);

        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    private void setupDeprecatedAssertionLink() {
        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
        OasisTestAssertion testAssertionWithMandatoryFields = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        AssertionLinkedToTest testLink = new AssertionLinkedToTest();
        testLink.setAssertion(testAssertionWithMandatoryFields);
        testLink.setLinkedEntityKeyword("1");
        testLink.setDeprecated(false);
        DatabaseManager.writeObject(testLink);

        target = testAssertionWithMandatoryFields.getTarget();

        OasisTestAssertion testAssertionWithMandatoryFields2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        testAssertionWithMandatoryFields2.setTarget(target);
        DatabaseManager.writeObject(testAssertionWithMandatoryFields2);

        testLink = new AssertionLinkedToTest();
        testLink.setAssertion(testAssertionWithMandatoryFields2);
        testLink.setLinkedEntityKeyword("2");
        testLink.setDeprecated(true);
        DatabaseManager.writeObject(testLink);

        if (!EntityManagerService.provideEntityManager().getTransaction().isActive()) {
            EntityManagerService.provideEntityManager().getTransaction().begin();
        }
    }

    @Test
    public void computeCoverageWithoutDeprecated() throws Exception {
        setupNotDeprecatedAssertionLink();
        List<OasisTarget> oasisTargets = new ArrayList<>();
        oasisTargets.add(target);
        List<IdSchemesCoverage> compute = IdSchemesCoverage.compute(oasisTargets, null);
        assertEquals(2, compute.size());
        assertEquals(2, compute.get(0).getNbAssertionsForThisIdScheme());
        assertEquals(2, compute.get(0).getNbAssertionsCoveredThroughTests());
    }

    @Test
    public void computeCoverageWithDeprecated() throws Exception {
        setupDeprecatedAssertionLink();
        List<OasisTarget> oasisTargets = new ArrayList<>();
        oasisTargets.add(target);
        List<IdSchemesCoverage> compute = IdSchemesCoverage.compute(oasisTargets, null);
        assertEquals(2, compute.size());
        assertEquals(2, compute.get(0).getNbAssertionsForThisIdScheme());
        assertEquals(1, compute.get(0).getNbAssertionsCoveredThroughTests());
    }
}
