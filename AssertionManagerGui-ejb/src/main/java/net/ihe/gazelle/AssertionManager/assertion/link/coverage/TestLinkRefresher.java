package net.ihe.gazelle.AssertionManager.assertion.link.coverage;

import net.ihe.gazelle.AssertionManager.ws.wrapper.TmWsClient;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTest;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTestQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tm.ws.data.TestWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xfs on 28/09/16.
 */
@Name("testLinkRefresher")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("TestLinkRefresherLocal")
public class TestLinkRefresher implements TestLinkRefresherLocal, Serializable {

    @Observer("updateDeprecatedTestsCoverage")
    @Transactional
    public void updateDeprecatedTestsCoverage(List<String> assertionsKeywordsIn) {
        if (assertionsKeywordsIn.size() > 0) {
            TmWsClient tmWsClient = new TmWsClient();
            List<TestWrapper> allReadyTests = tmWsClient.getTests(
                    null,
                    null,
                    null,
                    null,
                    null).getTypedWrappedData();
            refresh(assertionsKeywordsIn, allReadyTests);
            OasisTestAssertion.updateCoverageCounts(EntityManagerService.provideEntityManager());
        }
    }

    public void refresh(List<String> linkedEntities, List<TestWrapper> remoteEntities) {
        List<Integer> remoteEntitiesIds = getEntitiesIds(remoteEntities);

        for (String linkedEntity : linkedEntities) {
            if (!remoteEntitiesIds.contains(Integer.valueOf(linkedEntity))) {
                AssertionLinkedToTestQuery q = new AssertionLinkedToTestQuery();
                q.linkedEntityKeyword().eq(linkedEntity);
                changeDeprecatedStatus(q.getList(), true);
            }else{
                AssertionLinkedToTestQuery q2 = new AssertionLinkedToTestQuery();
                q2.linkedEntityKeyword().eq(linkedEntity);
                changeDeprecatedStatus(q2.getList(), false);

            }
        }
    }

    private List<Integer> getEntitiesIds(List<TestWrapper> remoteEntities){
        List<Integer> remoteEntitiesIds = new ArrayList<Integer>();
        for (TestWrapper currentTest : remoteEntities) {
            remoteEntitiesIds.add(currentTest.getId());
        }
        return remoteEntitiesIds;
    }

    private void changeDeprecatedStatus(List<AssertionLinkedToTest> assertionLinkedToTestList, boolean deprecatedStatus){
        EntityManager em = EntityManagerService.provideEntityManager();
        for (AssertionLinkedToTest assertionLinkedToTest : assertionLinkedToTestList) {
            assertionLinkedToTest.setDeprecated(deprecatedStatus);
            em.merge(assertionLinkedToTest);
        }
    }
}