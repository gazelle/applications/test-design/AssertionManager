package net.ihe.gazelle.AssertionManager.scope;

import net.ihe.gazelle.AssertionManager.idScheme.OasisTestAssertionCoverageQueryHelper;
import net.ihe.gazelle.oasis.testassertion.AssertionScope;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScopeCoverage implements Comparable<ScopeCoverage> {

    private static final int HUNDRED_PERCENT = 100;
    private AssertionScope scope;
    private boolean displayActions = true;
    private int nbAssertionsCovered = 0;
    private int nbAssertionsForThisScope = 0;
    private int nbAssertionsNotTestable = 0;
    private int nbAssertionsTestable = 0;
    private int percentCovered = 0;
    private int nbAssertionsNotCovered = 0;
    private int percentNotCovered = 0;

    public ScopeCoverage(AssertionScope scopeIn) {
        super();
        this.scope = scopeIn;
    }

    public AssertionScope getScope() {
        return scope;
    }

    public void setScope(AssertionScope scopeIn) {
        this.scope = scopeIn;
    }

    public boolean isDisplayActions() {
        return displayActions;
    }

    public void setDisplayActions(boolean displayActionsIn) {
        this.displayActions = displayActionsIn;
    }

    public static List<ScopeCoverage> compute(List<AssertionScope> scopes) {
        List<ScopeCoverage> scopeCoverage = new ArrayList<ScopeCoverage>();

        for (AssertionScope scope : scopes) {
            ScopeCoverage cov = new ScopeCoverage(scope);
            cov.compute();
            scopeCoverage.add(cov);
        }

        Collections.sort(scopeCoverage);
        computeTotals(scopeCoverage);
        return scopeCoverage;
    }

    private static void computeTotals(List<ScopeCoverage> idSchemesCoverages) {
        ScopeCoverage idSchemesCoverageTotal = new ScopeCoverage(new AssertionScope("Total", ""));
        int nbAssertionsCovered = 0;
        int nbAssertionsNotCovered = 0;
        int nbAssertionsForThisIdScheme = 0;
        int nbAssertionsNotTestable = 0;
        for (ScopeCoverage idSchemesCoverage : idSchemesCoverages) {
            nbAssertionsCovered += idSchemesCoverage.getNbAssertionsCovered();
            nbAssertionsNotCovered += idSchemesCoverage.getNbAssertionsNotCovered();
            nbAssertionsForThisIdScheme += idSchemesCoverage.getNbAssertionsForThisScope();
            nbAssertionsNotTestable += idSchemesCoverage.getNbAssertionsNotTestable();
        }

        idSchemesCoverageTotal.setNbAssertionsCovered(nbAssertionsCovered);
        idSchemesCoverageTotal.setNbAssertionsNotCovered(nbAssertionsNotCovered);
        idSchemesCoverageTotal.setNbAssertionsForThisScope(nbAssertionsForThisIdScheme);
        idSchemesCoverageTotal.setNbAssertionsNotTestable(nbAssertionsNotTestable);

        idSchemesCoverageTotal.computePercentCovered();
        idSchemesCoverageTotal.computePercentNotCovered();
        idSchemesCoverageTotal.setDisplayActions(false);
        idSchemesCoverages.add(idSchemesCoverageTotal);
    }

    public void compute() {
        computeAssertionNotTestable();
        computeTotalAssertions();
        computeAssertionTestable();
        computeTotalCoveredAssertions();
        computePercentCovered();
        computePercentNotCovered();
    }

    private void computeAssertionTestable() {
        nbAssertionsTestable = getNbAssertionsForThisScope() - getNbAssertionsNotTestable();
    }

    public int getNbAssertionsCovered() {
        return nbAssertionsCovered;
    }

    public void setNbAssertionsCovered(int nbAssertionsCoveredIn) {
        this.nbAssertionsCovered = nbAssertionsCoveredIn;
    }

    public int getNbAssertionsForThisScope() {
        return nbAssertionsForThisScope;
    }

    public void setNbAssertionsForThisScope(int nbAssertionsForThisScopeIn) {
        this.nbAssertionsForThisScope = nbAssertionsForThisScopeIn;
    }

    public int getPercentCovered() {
        return percentCovered;
    }

    public void setPercentCovered(int percentCoveredIn) {
        this.percentCovered = percentCoveredIn;
    }

    public int getPercentNotCovered() {
        return percentNotCovered;
    }

    public void setPercentNotCovered(int percentNotCoveredIn) {
        this.percentNotCovered = percentNotCoveredIn;
    }

    protected void computePercentCovered() {
        if (getNbAssertionsTestable() == 0) {
            percentCovered = HUNDRED_PERCENT;
        } else {
            percentCovered = (HUNDRED_PERCENT * nbAssertionsCovered) / getNbAssertionsTestable();
        }
    }

    protected void computePercentNotCovered() {
        percentNotCovered = HUNDRED_PERCENT - percentCovered;
    }

    private void computeTotalCoveredAssertions() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(scope.getKeyword());
        nbAssertionsCovered = query.countCoveredAssertions();
        nbAssertionsNotCovered = getNbAssertionsTestable() - nbAssertionsCovered;
    }

    private void computeTotalAssertions() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(scope.getKeyword());
        nbAssertionsForThisScope = query.getCount();
    }

    private void computeAssertionNotTestable() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(scope.getKeyword());
        nbAssertionsNotTestable = query.countNotTestableAssertions();
    }

    @Override
    public int compareTo(ScopeCoverage o) {
        if (o.scope == null) {
            return 1;
        }
        if (this.scope == null) {
            return -1;
        }
        return this.scope.getKeyword().toLowerCase().compareTo(o.scope.getKeyword().toLowerCase());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ScopeCoverage)) {
            return false;
        }
        ScopeCoverage o = (ScopeCoverage) obj;
        if (!scope.getKeyword().equalsIgnoreCase(o.scope.getKeyword())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return new HashCodeBuilder().append(scope).toHashCode();
    }

    public int getNbAssertionsNotCovered() {
        return nbAssertionsNotCovered;
    }

    public void setNbAssertionsNotCovered(int nbAssertionsNotCoveredIn) {
        this.nbAssertionsNotCovered = nbAssertionsNotCoveredIn;
    }

    public int getNbAssertionsNotTestable() {
        return nbAssertionsNotTestable;
    }

    public void setNbAssertionsNotTestable(int nbAssertionsNotTestable) {
        this.nbAssertionsNotTestable = nbAssertionsNotTestable;
    }

    public int getNbAssertionsTestable() {
        return nbAssertionsTestable;
    }

    public void setNbAssertionsTestable(int nbAssertionsTestable) {
        this.nbAssertionsTestable = nbAssertionsTestable;
    }

}
