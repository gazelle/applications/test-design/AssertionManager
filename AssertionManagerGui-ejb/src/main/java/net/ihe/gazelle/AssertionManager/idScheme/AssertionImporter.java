package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.xmlloader.Listener;
import net.ihe.gazelle.oasis.xmlloader.XmlLoader;
import net.sf.ehcache.CacheManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>AssertionImporter<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 25/07/16
 * @class AssertionImporter
 * @package net.ihe.gazelle.AssertionManager.idScheme
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Name("assertionImporter")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("AssertionImporterLocal")
public class AssertionImporter implements AssertionImporterLocal, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AssertionImporter.class);
    public static final String UPLOAD_HISTORY = "uploadHistory";
    public static final String CURRENT_UPLOAD = "currentUpload";

    @Observer("importAssertions")
    @Transactional
    public void uploadedFile(File tmpFile, String fileName, String loggedInUser) throws JAXBException, SAXException {

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        XmlLoader xmlLoader = new XmlLoader(entityManager, new Listener() {
            @Override
            public void send(String message) {
                AssertionImporter.setCurrentUpload(message);
            }

            @Override
            public void sendFinalMessage(String message) {
                AssertionImporter.appendUploadHistory(message);
            }
        }, loggedInUser);

        String xml = readFile(tmpFile);
        try {
            xmlLoader.load(xml);
            appendUploadHistory(fileName + " successfully uploaded");
            clearCurrentUpload();
            resetCache();
        } catch (java.lang.Exception e) {
            appendUploadHistory("Error importing: " + fileName);
            appendUploadHistory(ExceptionUtils.getRootCauseMessage(e));
            LOG.error(ExceptionUtils.getRootCauseMessage(e));
        }
    }

    private String readFile(File tmpFile) {
        String xml = null;
        try {
            xml = FileUtils.readFileToString(tmpFile);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return xml;
    }

    private void resetCache() {
        List<CacheManager> allCacheManagers = new ArrayList<CacheManager>(CacheManager.ALL_CACHE_MANAGERS);
        for (CacheManager cacheManager : allCacheManagers) {
            try {
                String[] cacheNames = cacheManager.getCacheNames();
                for (String cacheName : cacheNames) {
                    cacheManager.getEhcache(cacheName).removeAll();
                }
            } catch (Exception e) {
                LOG.error("Failed to clear cache for " + cacheManager);
            }
        }
        FacesMessages.instance().add("Cache is removed !");
    }

    public static String getCurrentUpload() {
        return net.ihe.gazelle.oasis.testassertion.CacheManager.get(CURRENT_UPLOAD);
    }

    public static void clearCurrentUpload() {
        net.ihe.gazelle.oasis.testassertion.CacheManager.put(CURRENT_UPLOAD, "");
    }

    public static void setCurrentUpload(String message) {
        net.ihe.gazelle.oasis.testassertion.CacheManager.put(CURRENT_UPLOAD, getDate() + ": " + message + "<br/>");
    }

    private static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return "<span class=\"gzl-color-light-grey\">" + dateFormat.format(new Date()) + "</span>";
    }

    public static String getUploadHistory() {
        String history = net.ihe.gazelle.oasis.testassertion.CacheManager.get(UPLOAD_HISTORY);
        if (history == null) {
            history = "";
        }
        return history;
    }

    public static void appendUploadHistory(String line) {
        appendUploadHistoryNoDate(getDate() + ": " + line);
    }

    public static void appendUploadHistoryNoDate(String line) {
        String history = getUploadHistory();
        net.ihe.gazelle.oasis.testassertion.CacheManager.put(UPLOAD_HISTORY, line + "<br/>" + history);
    }

    public static void clearHistory() {
        clearCurrentUpload();
        clearUploadHistory();
    }

    private static void clearUploadHistory() {
        net.ihe.gazelle.oasis.testassertion.CacheManager.put(UPLOAD_HISTORY, "");
    }
}
