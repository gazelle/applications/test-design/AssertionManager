package net.ihe.gazelle.AssertionManager.assertion.link.appliesTo;

import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToAipo;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToAipoQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tf.ws.data.AipoWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("appliesToAipoManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AppliesToAipoManagerLocal")
public class AppliesToAipoManager extends AssertionLinkManager implements AppliesToAipoManagerLocal, Serializable {


    private static final long serialVersionUID = -4181671477580363694L;
    private AipoWrapper selectedAipo = new AipoWrapper();

    private GazelleListDataModel<AipoWrapper> aipos;
    private String selectedIntegrationProfileOption;
    private String selectedActor;

    @Override
    public List<String> getDomainsAvailable() {
        return getTmWsClient().getDomainsAvailable();
    }

    public List<String> getIntegrationProfilesNames() {
        return getTmWsClient().getIntegrationProfilesNames(getSelectedDomain(), getSelectedActor());
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedAipoLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedAipoFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedAipoTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isAipoSelected();
    }

    public List<String> getIntegrationProfileOptions() {
        return getTmWsClient().getIntegrationProfileOptions(getSelectedDomain(), getSelectedActor(), getSelectedIntegrationProfileName());
    }

    public List<String> getActors() {
        return getTmWsClient().getActorsForFilter(getSelectedDomain(), getSelectedIntegrationProfileName());
    }

    public void setSelectedAipo(AipoWrapper aipo) {
        selectedAipo = getFullAipo(aipo);
    }

    /*Get full aipo with description*/
    private AipoWrapper getFullAipo(AipoWrapper aipo) {
        return getTmWsClient().getFullAipo(aipo.getId());
    }

    public AipoWrapper getSelectedAipo() {
        return selectedAipo;
    }

    public GazelleListDataModel<AipoWrapper> getAipos() {
        if (aipos == null) {
            aipos = getTmWsClient().getAipos(getSelectedDomain(), getSelectedActor(), getSelectedIntegrationProfileName(), getSelectedIntegrationProfileOption());
        }
        return aipos;
    }

    public boolean isAipoSelected() {
        return getSelectedAipo().getId() != 0;
    }

    public List<AssertionAppliesToAipo> assertionsOfSelectedAipo() {
        List<AssertionAppliesToAipo> listDistinct = new ArrayList<AssertionAppliesToAipo>();

        if (getSelectedAipo() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery(entityManager);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedAipo().getId()));
            listDistinct = query.getListDistinct();
        }
        return listDistinct;
    }

    public void unlinkSelectedAipoFrom(int assertionId) {
        if (getSelectedAipo() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedAipo().getId()));

            AssertionAppliesToAipo uniqueResult = query.getUniqueResult();

            entityManager.remove(uniqueResult);
            entityManager.flush();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void linkSelectedAipoTo(int assertionId) {
        if (getSelectedAipo() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            AipoWrapper aipo = getSelectedAipo();
            AssertionAppliesToAipo link = new AssertionAppliesToAipo(String.valueOf(aipo.getId()), aipo.toString(), aipo.getId(), aipo.getBaseUrl(), aipo.getPermanentLink(), assertion);
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public boolean isAipoCovered(String aipoKeyword) {
        boolean result = false;
        AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery();
        query.linkedEntityKeyword().eq(aipoKeyword);
        List<AssertionAppliesToAipo> listDistinct = query.getListDistinct();
        if ((listDistinct != null) && (listDistinct.size() != 0)) {
            result = true;
        }
        return result;
    }

    public boolean isSelectedAipoLinkedTo(int assertionId) {
        boolean result = false;
        AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery();
        query.assertion().id().eq(assertionId);
        if (getSelectedAipo() != null) {
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedAipo().getId()));
        }
        AssertionAppliesToAipo uniqueResult = query.getUniqueResult();
        if (uniqueResult != null) {
            result = true;
        }
        return result;
    }

    @Override
    public void selectedIntegrationProfileUpdate() {
        selectedAipo = new AipoWrapper();
        aipos = null;
        selectedActor = "";
    }

    @Override
    public void selectedDomainUpdateCallback() {
        selectedActor = "";
        selectedIntegrationProfileOption = "";
        setSelectedIntegrationProfileName("");
        selectedAipo = new AipoWrapper();
        aipos = null;
    }

    public String getSelectedIntegrationProfileOption() {
        return selectedIntegrationProfileOption;
    }

    public void setSelectedIntegrationProfileOption(String selectedIntegrationProfileOptionIn) {
        this.selectedIntegrationProfileOption = selectedIntegrationProfileOptionIn;
        aipos = null;
    }

    public String getSelectedActor() {
        return selectedActor;
    }

    public void setSelectedActor(String selectedActorIn) {
        this.selectedActor = selectedActorIn;
        aipos = null;
    }
}
