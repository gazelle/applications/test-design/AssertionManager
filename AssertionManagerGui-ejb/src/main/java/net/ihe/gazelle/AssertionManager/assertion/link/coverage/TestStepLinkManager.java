package net.ihe.gazelle.AssertionManager.assertion.link.coverage;

import net.ihe.gazelle.AssertionManager.assertion.OasisTestAssertionFilter;
import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTestStep;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTestStepQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tm.ws.data.TestStepWrapper;
import net.ihe.gazelle.tm.ws.data.TestWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

@Name("testStepLinkManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "TestStepLinkManagerLocal")
public class TestStepLinkManager extends AssertionLinkManager implements TestStepLinkManagerLocal, Serializable {


    private static final long serialVersionUID = -6652075209044350380L;

    private TestStepWrapper selectedTestStep;

    private List<TestStepWrapper> testStepsOf;

    public OasisTestAssertionFilter getFilter() {
        return onlyTestableAssertion(super.getFilter());
    }

    public void selectedIntegrationProfileUpdate() {
        resetSelectedTest();
        this.selectedTestStep = null;
        testStepsOf = null;
    }

    public void setSelectedTest(TestWrapper testKeyword) {
        super.setSelectedTest(testKeyword);
        this.selectedTestStep = null;
        testStepsOf = null;
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedTestStepLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedTestStepFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedTestStepTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isTestStepSelected();
    }

    public List<TestStepWrapper> getTestSteps() {
        if (testStepsOf == null) {
            testStepsOf = getTmWsClient().getTestStepsOf(getSelectedTest().getId());
        }
        return testStepsOf;
    }

    public void setSelectedTestStep(TestStepWrapper testStep) {
        this.selectedTestStep = testStep;
    }

    public TestStepWrapper getSelectedTestStep() {
        return selectedTestStep;
    }

    public boolean isTestStepSelected() {
        return getSelectedTestStep() != null;
    }

    public boolean isSelectedTestStepLinkedTo(int assertionId) {
        boolean result = false;
        if (isTestStepSelected()) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionLinkedToTestStepQuery query = new AssertionLinkedToTestStepQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(getSelectedTestStepId());
            AssertionLinkedToTestStep uniqueResult = query.getUniqueResult();
            if (uniqueResult != null) {
                result = true;
            }
        }
        return result;
    }

    public void linkSelectedTestStepTo(int assertionId) {
        if (isTestStepSelected()) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            AssertionLinkedToTestStep link = new AssertionLinkedToTestStep(getSelectedTestStepId(), assertion, null,
                    getSelectedTestStep().getPermanentLink(),
                    getSelectedTestStep().getTestParent() + ": " + getSelectedTestStep().getStepIndex());
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void unlinkSelectedTestStepFrom(int assertionId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionLinkedToTestStepQuery query = new AssertionLinkedToTestStepQuery(entityManager);
        query.assertion().id().eq(assertionId);
        query.linkedEntityKeyword().eq(getSelectedTestStepId());

        AssertionLinkedToTestStep uniqueResult = query.getUniqueResult();

        entityManager.remove(uniqueResult);
        entityManager.flush();

        OasisTestAssertion assertion = getAssertionFromId(assertionId);
        updateCoverage(entityManager, assertion.getId());
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    private String getSelectedTestStepId() {
        if (isTestStepSelected()) {
            return String.valueOf(getSelectedTestStep().getId());
        }
        return "";
    }

    public List<AssertionLinkedToTestStep> assertionsOfSelectedTestStep() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AssertionLinkedToTestStepQuery query = new AssertionLinkedToTestStepQuery(entityManager);
        query.linkedEntityKeyword().eq(getSelectedTestStepId());
        return query.getListDistinct();
    }

    public int getNbAssertionsOfTestStep(int testStepid) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AssertionLinkedToTestStepQuery query = new AssertionLinkedToTestStepQuery(entityManager);
        query.linkedEntityKeyword().eq(String.valueOf(testStepid));
        return query.getCount();
    }

    @Override
    public void selectedDomainUpdateCallback() {
        // TODO Auto-generated method stub

    }

    public List<String> getTransactionNames() {
        return getTmWsClient().getTransactionNamesWithTest(getSelectedDomain(), getSelectedActorName());
    }

    public List<String> getActors() {
        return getTmWsClient().getActorsWithTestForFilter(getSelectedDomain(), getSelectedTransactionName());
    }
}
