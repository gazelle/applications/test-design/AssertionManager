package net.ihe.gazelle.AssertionManager.assertion.link.appliesTo;

import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToAuditMessage;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToAuditMessageQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tf.ws.data.AuditMessageWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("appliesToAuditMessageManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AppliesToAuditMessageManagerLocal")
public class AppliesToAuditMessageManager extends AssertionLinkManager
        implements AppliesToAuditMessageManagerLocal, Serializable {


    private static final long serialVersionUID = -4181671477580363694L;
    private AuditMessageWrapper selectedAuditMessage = new AuditMessageWrapper();

    private static final Logger LOG = LoggerFactory.getLogger(AppliesToAuditMessageManager.class);
    private GazelleListDataModel<AuditMessageWrapper> auditMessages;
    private String selectedTransaction;
    private String selectedActor;

    @Override
    public List<String> getDomainsAvailable() {
        return getTmWsClient().getDomainsWithAuditMessage();
    }

    public List<String> getTransactionNames() {
        return getTmWsClient().getTransactionNamesWithAuditMessage(getSelectedDomain(), getSelectedActor());
    }

    public List<String> getIntegrationProfiles() {
        return getTmWsClient().getIntegrationProfilesNames(getSelectedDomain());
    }

    public List<String> getActors() {
        return getTmWsClient().getActorsWithAuditMessageForFilter(getSelectedDomain(), getSelectedTransaction());
    }

    public String getSelectedAuditMessageDescription() {
        return selectedAuditMessage.getDescription();
    }

    public void setSelectedAuditMessage(AuditMessageWrapper auditMessage) {
        selectedAuditMessage = getFullAuditMessage(auditMessage);
        LOG.info("setSelectedAuditMessage " + selectedAuditMessage);
    }

    private AuditMessageWrapper getFullAuditMessage(AuditMessageWrapper auditMessage) {
        return getTmWsClient().getAuditMessage(auditMessage.getId());
    }

    public AuditMessageWrapper getSelectedAuditMessage() {
        return selectedAuditMessage;
    }

    public GazelleListDataModel<AuditMessageWrapper> getAuditMessages() {
        if (auditMessages == null) {
            auditMessages = getTmWsClient().getAuditMessages(getSelectedDomain(), getSelectedTransaction(), getSelectedActor());
        }
        return auditMessages;
    }

    public boolean isAuditMessageSelected() {
        return getSelectedAuditMessage().getName() != null;
    }

    public List<AssertionAppliesToAuditMessage> assertionsOfSelectedAuditMessage() {
        List<AssertionAppliesToAuditMessage> listDistinct = new ArrayList<AssertionAppliesToAuditMessage>();

        if (getSelectedAuditMessage() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionAppliesToAuditMessageQuery query = new AssertionAppliesToAuditMessageQuery(entityManager);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedAuditMessage().getKeyword()));
            listDistinct = query.getListDistinct();
        }
        return listDistinct;
    }

    @Override
    public void selectedDomainUpdate() {
        selectedAuditMessage = new AuditMessageWrapper();
        auditMessages = null;
        selectedTransaction = null;
        selectedActor = null;
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedAuditMessageLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedAuditMessageFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedAuditMessageTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isAuditMessageSelected();
    }

    @Override
    public void selectedDomainUpdateCallback() {
        // TODO Auto-generated method stub

    }

    public void unlinkSelectedAuditMessageFrom(int assertionId) {
        if (getSelectedAuditMessage() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            AssertionAppliesToAuditMessageQuery query = new AssertionAppliesToAuditMessageQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedAuditMessage().getKeyword()));

            AssertionAppliesToAuditMessage uniqueResult = query.getUniqueResult();

            entityManager.remove(uniqueResult);
            entityManager.flush();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void linkSelectedAuditMessageTo(int assertionId) {
        if (getSelectedAuditMessage() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            AuditMessageWrapper auditMessage = getSelectedAuditMessage();
            AssertionAppliesToAuditMessage link = new AssertionAppliesToAuditMessage(auditMessage.getKeyword(),
                    auditMessage.getName(), auditMessage.getId(), auditMessage.getBaseUrl(),
                    auditMessage.getPermanentLink(), assertion);
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public boolean isAuditMessageCovered(String auditMessageKeyword) {
        boolean result = false;
        AssertionAppliesToAuditMessageQuery query = new AssertionAppliesToAuditMessageQuery();
        query.linkedEntityKeyword().eq(auditMessageKeyword);
        List<AssertionAppliesToAuditMessage> listDistinct = query.getListDistinct();
        if ((listDistinct != null) && (listDistinct.size() != 0)) {
            result = true;
        }
        return result;
    }

    public boolean isSelectedAuditMessageLinkedTo(int assertionId) {
        boolean result = false;
        AssertionAppliesToAuditMessageQuery query = new AssertionAppliesToAuditMessageQuery();
        query.assertion().id().eq(assertionId);
        if (getSelectedAuditMessage() != null) {
            query.linkedEntityKeyword().eq(getSelectedAuditMessage().getKeyword());
        }
        AssertionAppliesToAuditMessage uniqueResult = query.getUniqueResult();
        if (uniqueResult != null) {
            result = true;
        }
        return result;
    }

    @Override
    public void selectedIntegrationProfileUpdate() {
    }

    public String getSelectedTransaction() {
        return selectedTransaction;
    }

    public void setSelectedTransaction(String selectedTransactionIn) {
        this.selectedTransaction = selectedTransactionIn;
        auditMessages = null;
        selectedActor = null;
    }

    public String getSelectedActor() {
        return selectedActor;
    }

    public void setSelectedActor(String selectedActorIn) {
        this.selectedActor = selectedActorIn;
        auditMessages = null;
    }
}
