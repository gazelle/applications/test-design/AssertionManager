package net.ihe.gazelle.AssertionManager.assertion.link.coverage;

import net.ihe.gazelle.AssertionManager.assertion.OasisTestAssertionFilter;
import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkQuery;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTest;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTestQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.async.QuartzDispatcher;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("testLinkManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "TestLinkManagerLocal")
public class TestLinkManager extends AssertionLinkManager implements TestLinkManagerLocal, Serializable {


    private static final long serialVersionUID = 1327768765313983054L;


    public void selectedIntegrationProfileUpdate() {
        resetSelectedTest();
    }


    public List<String> getTransactionNames() {
        return getTmWsClient().getTransactionNamesWithTest(getSelectedDomain(), getSelectedActorName());
    }

    public List<String> getActors() {
        return getTmWsClient().getActorsWithTestForFilter(getSelectedDomain(), getSelectedTransactionName());
    }

    public OasisTestAssertionFilter getFilter() {
        return  onlyTestableAssertion(super.getFilter());
    }

    public boolean isSelectedTestLinkedTo(int assertionId) {
        boolean result = false;
        if (getSelectedTest() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionLinkedToTestQuery query = new AssertionLinkedToTestQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedTest().getId()));
            AssertionLinkedToTest uniqueResult = query.getUniqueResult();
            if (uniqueResult != null) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedTestLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedTestFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedTestTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isTestSelected();
    }

    public boolean isTestDirectlyCovered(String testKeyword) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        boolean result = false;
        AssertionLinkedToTestQuery query = new AssertionLinkedToTestQuery(entityManager);
        query.linkedEntityKeyword().eq(testKeyword);
        List<AssertionLinkedToTest> listDistinct = query.getListDistinct();
        if ((listDistinct != null) && (listDistinct.size() != 0)) {
            result = true;
        }
        return result;
    }

    public void linkSelectedTestTo(int assertionId) {
        if (getSelectedTest() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            AssertionLinkedToTest link = new AssertionLinkedToTest(String.valueOf(getSelectedTest().getId()),
                    assertion, null, getSelectedTest().getPermanentLink(), getSelectedTest().getKeyword());

            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void unlinkSelectedTestFrom(int assertionId) {
        if (getSelectedTest() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            AssertionLinkedToTestQuery query = new AssertionLinkedToTestQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedTest().getId()));

            AssertionLinkedToTest uniqueResult = query.getUniqueResult();

            entityManager.remove(uniqueResult);
            entityManager.flush();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            updateCoverage(entityManager, assertion.getId());
        }
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public List<AssertionLinkedToTest> assertionsOfSelectedTest() {
        List<AssertionLinkedToTest> listDistinct = new ArrayList<AssertionLinkedToTest>();
        if (getSelectedTest() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionLinkedToTestQuery query = new AssertionLinkedToTestQuery(entityManager);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedTest().getId()));
            listDistinct = query.getListDistinct();
        }
        return listDistinct;
    }

    public void refreshAssertionsLink() {
        AssertionLinkQuery query = new AssertionLinkQuery();
        List<String> assertionsLinkKeywords = query.linkedEntityKeyword().getListDistinct();
        QuartzDispatcher.instance().scheduleAsynchronousEvent("updateDeprecatedTestsCoverage", assertionsLinkKeywords);
    }

    @Override
    public void selectedDomainUpdateCallback() {
        // TODO Auto-generated method stub

    }


}
