package net.ihe.gazelle.AssertionManager.ws.wrapper;

import net.ihe.gazelle.AssertionManager.utils.PreferencesManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToRule;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToTestStep;
import net.ihe.gazelle.tf.ws.data.*;
import net.ihe.gazelle.tm.ws.client.WsClient;
import net.ihe.gazelle.tm.ws.data.TestStepWrapper;
import net.ihe.gazelle.tm.ws.data.TestWrapper;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class TmWsClient implements Serializable {
    private static final long serialVersionUID = -3804480006245325197L;

    public TmWsClient() {
    }

    public List<String> getDomainsWithTestsAvailable() {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getDomainsWithTestsAvailable();
    }

    public List<String> getDomainsAvailable() {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getDomainsAvailable();
    }

    public List<String> getDomainsWithAuditMessage() {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getDomainsWithAuditMessage();
    }

    public List<String> getTransactionNamesWithAuditMessage(String selectedDomain, String selectedActor) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getTransactionNamesWithAuditMessage(selectedDomain, selectedActor);
    }

    /*Get full test with description*/
    public TestWrapper getTest(String testId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getTest(testId);
    }

    public List<String> getIntegrationProfilesNamesWithTests(String selectedDomain) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getIntegrationProfilesNamesWithTests(selectedDomain);
    }

    public List<String> getTestTypes(String selectedDomain, String selectedIntegrationProfileName) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getTestTypes(selectedDomain, selectedIntegrationProfileName);
    }

    public GazelleListDataModel<TestWrapper> getTests(String selectedIntegrationProfileName, String selectedDomain, String selectedTestType, String selectedActorName, String selectedTransactionName) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<TestWrapper> foundTestsTmp = client
                .getTests(selectedIntegrationProfileName, selectedDomain, selectedTestType,
                        selectedActorName, selectedTransactionName);
        Collections.sort(foundTestsTmp);
        return new GazelleListDataModel<TestWrapper>(foundTestsTmp);
    }

    public List<String> getActorsWithAuditMessageForFilter(String selectedDomain, String selectedTransaction) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getActorsWithAuditMessageForFilter(selectedDomain, null, selectedTransaction);
    }

    public GazelleListDataModel<ActorWrapper> getActors(String selectedDomain, String selectedIntegrationProfileName) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<ActorWrapper> actorsTmp = client.getActors(selectedDomain, selectedIntegrationProfileName);
        return new GazelleListDataModel<ActorWrapper>(actorsTmp);
    }

    public List<String> getActorsForFilter(String selectedDomain) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getActorsForFilter(selectedDomain, null, null);
    }

    public List<String> getActorsForFilter(String selectedDomain, String selectedIntegrationProfileName) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getActorsForFilter(selectedDomain, selectedIntegrationProfileName, null);
    }

    /*Get full actor with description*/
    public ActorWrapper getFullActor(int actorId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getActor(actorId);
    }

    public List<String> getIntegrationProfilesNames(String selectedDomain) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getIntegrationProfilesNames(selectedDomain, null);
    }

    public List<String> getIntegrationProfilesNames(String selectedDomain, String selectedActor) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getIntegrationProfilesNames(selectedDomain, selectedActor);
    }

    public List<String> getIntegrationProfileOptions(String selectedDomain, String selectedActor, String selectedIntegrationProfileName) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getIntegrationProfileOptions(selectedDomain, selectedActor, selectedIntegrationProfileName);
    }

    public GazelleListDataModel<AipoWrapper> getAipos(String selectedDomain, String selectedActor, String selectedIntegrationProfileName, String selectedIntegrationProfileOption) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<AipoWrapper> aiposTmp = client.getAipos(selectedDomain, selectedActor, selectedIntegrationProfileName, selectedIntegrationProfileOption);
        return new GazelleListDataModel<AipoWrapper>(aiposTmp);
    }

    /*Get full aipo with description*/
    public AipoWrapper getFullAipo(int aipoId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getAipo(aipoId);
    }

    public GazelleListDataModel<AuditMessageWrapper> getAuditMessages(String selectedDomain, String selectedTransaction, String selectedActor) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<AuditMessageWrapper> auditMessagesTmp = client.getAuditMessages(selectedDomain, selectedTransaction, selectedActor);
        return new GazelleListDataModel<AuditMessageWrapper>(auditMessagesTmp);
    }

    public AuditMessageWrapper getAuditMessage(int auditMessageId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getAuditMessage(auditMessageId);
    }

    public GazelleListDataModel<IntegrationProfileWrapper> getIntegrationProfiles(String selectedDomain, String selectedActor) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<IntegrationProfileWrapper> ipsTmp = client.getIntegrationProfiles(selectedDomain, selectedActor);
        return new GazelleListDataModel<IntegrationProfileWrapper>(ipsTmp);
    }

    /*Get full integrationProfile with description*/
    public IntegrationProfileWrapper getFullIntegrationProfile(int integrationProfileId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getIntegrationProfile(integrationProfileId);
    }

    /*Get full standard with description*/
    public StandardWrapper getFullStandard(int standardId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getStandard(standardId);
    }

    public GazelleListDataModel<StandardWrapper> getStandards(String selectedDomain, String selectedActor, String selectedIntegrationProfileName, String selectedIntegrationProfileOption) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<StandardWrapper> standardsTmp = client.getStandards(selectedDomain, selectedActor, selectedIntegrationProfileName, selectedIntegrationProfileOption);
        return new GazelleListDataModel<StandardWrapper>(standardsTmp);
    }

    public TransactionWrapper getFullTransaction(int transactionId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getTransaction(transactionId);
    }

    public GazelleListDataModel<TransactionWrapper> getTransactions(String selectedDomain) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<TransactionWrapper> transactionsTmp = client.getTransactions(selectedDomain);
        return new GazelleListDataModel<TransactionWrapper>(transactionsTmp);
    }

    public List<String> getDomainsWithRulesAvailable() {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getDomainsWithRulesAvailable();
    }

    public List<String> getIntegrationProfilesNamesWithRules(String selectedDomain) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getIntegrationProfilesNamesWithRules(selectedDomain);
    }

    public List<RuleWrapper> getRulesOfIntegrationProfile(String getSelectedIntegrationProfileName) {

        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        List<RuleWrapper> rulesTmp = client.getRulesOfIntegrationProfile(getSelectedIntegrationProfileName).getRules();
        Collections.sort(rulesTmp);
        return rulesTmp;
    }

    public List<String> getActorsWithTestForFilter(String selectedDomain, String selectedTransactionName) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getActorsWithTestForFilter(selectedDomain, null, selectedTransactionName);
    }

    public List<String> getTransactionNamesWithTest(String selectedDomain, String selectedActorName) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getTransactionNamesWithTest(selectedDomain, selectedActorName);
    }

    public List<TestStepWrapper> getTestStepsOf(int selectedTestId) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getTestStepsOf(String.valueOf(selectedTestId));
    }

    public RuleWrapper getRule(AssertionLinkedToRule ruleTmp) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());

        return client.getRule(ruleTmp.getLinkedEntityKeyword());

    }

    public TestStepWrapper getTestSteps(AssertionLinkedToTestStep testStepTmp) {
        WsClient client = new WsClient(PreferencesManager.instance().getGazelleTestManagementRestUrl());
        return client.getTestStep(testStepTmp.getLinkedEntityKeyword());

    }
}