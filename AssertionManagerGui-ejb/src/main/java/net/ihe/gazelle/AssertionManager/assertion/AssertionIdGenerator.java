package net.ihe.gazelle.AssertionManager.assertion;

import org.apache.commons.lang.StringUtils;

/**
 * <b>Class Description : </b>AssertionIdGenerator<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 07/06/16
 * @class AssertionIdGenerator
 * @package net.ihe.gazelle.AssertionManager.assertion.TestAssertion
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
public final class AssertionIdGenerator {

    private AssertionIdGenerator() {
    }

    public static String getNextAssertionId(String currentAssertionId) {
        String[] assertionId = new String[2];
        try {
            String[] split = currentAssertionId.split("-");
            if (split.length == 2) {
                String assertionNumberString = split[1];
                int assertionNumber = Integer.valueOf(assertionNumberString);
                assertionId[0] = split[0];
                assertionId[1] = String.format("%0" + assertionNumberString.length() + "d", assertionNumber + 1);
            } else {
                assertionId[0] = split[0];
                assertionId[1] = "001";
            }
        } catch (java.lang.NumberFormatException e) {
            assertionId[0] = currentAssertionId;
            assertionId[1] = "001";
        }
        return StringUtils.join(assertionId, "-");
    }
}
