package net.ihe.gazelle.AssertionManager.document;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.OasisNormativeSource;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>DocumentManager<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 27/06/16
 * @class DocumentManager
 * @package net.ihe.gazelle.AssertionManager.document
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Name("documentManager")
@Scope(ScopeType.PAGE)
public class DocumentManager implements Serializable {
    private static final long serialVersionUID = -3524113882464940854L;
    private OasisRefSourceItem selectedDocument;
    private String errorMessage = "";
    private DocumentFilter filter;
    private OasisTarget selectedIdScheme;
    private boolean updateAssertionsStatus = false;
    private static final Logger LOG = LoggerFactory.getLogger(DocumentManager.class);

    public DocumentFilter getFilter() {
        if (filter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            filter = new DocumentFilter(requestParameterMap);
        }
        return filter;
    }


    public FilterDataModel<OasisRefSourceItem> getDocuments() {
        return new FilterDataModel<OasisRefSourceItem>(getFilter()) {
            @Override
            protected Object getId(OasisRefSourceItem t) {
                return t.getId();
            }
        };
    }

    public OasisRefSourceItem getSelectedDocument() {
        if (selectedDocument == null) {
            FacesContext fc = FacesContext.getCurrentInstance();
            String id = fc.getExternalContext().getRequestParameterMap().get("id");
            if (id != null) {
                loadDocument(id);
            } else {
                newDocument();
            }
        }
        return selectedDocument;
    }

    private void loadDocument(String id) {
        selectedDocument = EntityManagerService.provideEntityManager().find(OasisRefSourceItem.class, Integer.valueOf(id));
        if (selectedDocument == null) {
            errorMessage = "Cannot find the document your looking for.";
        }
    }

    private void newDocument() {
        selectedDocument = new OasisRefSourceItem();
    }

    public void setSelectedDocument(OasisRefSourceItem doc) {
        selectedDocument = doc;
    }

    public String save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        selectedDocument = entityManager.merge(selectedDocument);
        markAssertionsForReview(entityManager, selectedDocument);

        FacesMessages.instance().add("Document successfully saved");
        return "/documents/index.seam";
    }

    private void markAssertionsForReview(EntityManager entityManager, OasisRefSourceItem refSourceItem) {
        if (isUpdateAssertionsStatus()) {
            refSourceItem.markAssertionsForReview(entityManager);
            FacesMessages.instance().add("Assertions marked to be reviewed");
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void delete() {
        if (selectedDocument != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedDocument = entityManager.find(OasisRefSourceItem.class, selectedDocument.getId());
            unlinkNormativeSources(entityManager);
            entityManager.remove(selectedDocument);
        }
    }

    private void unlinkNormativeSources(EntityManager entityManager) {
        OasisNormativeSource.unlinkNormativeSources(entityManager, selectedDocument);
    }


    public OasisTarget getSelectedIdScheme() {
        return selectedIdScheme;
    }

    public void setSelectedIdScheme(OasisTarget selectedIdScheme) {
        this.selectedIdScheme = selectedIdScheme;
    }

    public String linkToIdScheme() {
        EntityManager em = EntityManagerService.provideEntityManager();

        OasisTarget idScheme = getSelectedIdScheme();
        idScheme.linkToDocument(em, getDocumentToLink());
        if (idScheme.getDocuments().isEmpty() && idScheme.getTestAssertions().isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Document not linked : please add an assertion to this idScheme before");
        } else if (idScheme.getDocuments().isEmpty() && !idScheme.getTestAssertions().isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Document not linked");
        } else {
            FacesMessages.instance().add("Document successfully linked");
        }
        return "index.seam";
    }

    private List<OasisRefSourceItem> getDocumentToLink() {
        List<OasisRefSourceItem> sources = new ArrayList<>();
        sources.add(selectedDocument);
        return sources;
    }

    public boolean isUpdateAssertionsStatus() {
        return updateAssertionsStatus;
    }

    public void setUpdateAssertionsStatus(boolean updateAssertionsStatus) {
        this.updateAssertionsStatus = updateAssertionsStatus;
    }

    public void clearFilter() {
        getFilter().clear();
    }
}
