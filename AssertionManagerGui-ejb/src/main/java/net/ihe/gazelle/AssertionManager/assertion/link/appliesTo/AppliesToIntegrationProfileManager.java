package net.ihe.gazelle.AssertionManager.assertion.link.appliesTo;

import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToIntegrationProfile;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToIntegrationProfileQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tf.ws.data.IntegrationProfileWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("appliesToIntegrationProfileManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AppliesToIntegrationProfileManagerLocal")
public class AppliesToIntegrationProfileManager extends AssertionLinkManager implements AppliesToIntegrationProfileManagerLocal, Serializable {

    private static final long serialVersionUID = -5139466765436003214L;
    private String selectedActor;
    private GazelleListDataModel<IntegrationProfileWrapper> ips;
    private IntegrationProfileWrapper selectedIntegrationProfile = new IntegrationProfileWrapper();

    @Override
    public List<String> getDomainsAvailable() {
        return getTmWsClient().getDomainsAvailable();
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedIntegrationProfileLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedIntegrationProfileFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedIntegrationProfileTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isIntegrationProfileSelected();
    }

    public List<String> getActors() {
        return getTmWsClient().getActorsForFilter(getSelectedDomain());
    }

    public GazelleListDataModel<IntegrationProfileWrapper> getIntegrationProfiles() {
        if (ips == null) {
            ips = getTmWsClient().getIntegrationProfiles(getSelectedDomain(), getSelectedActor());
        }
        return ips;
    }

    public void setSelectedIntegrationProfile(IntegrationProfileWrapper integrationProfile) {
        selectedIntegrationProfile = getFullIntegrationProfile(integrationProfile);
    }

    /*Get full integrationProfile with description*/
    private IntegrationProfileWrapper getFullIntegrationProfile(IntegrationProfileWrapper integrationProfile) {
        return getTmWsClient().getFullIntegrationProfile(integrationProfile.getId());
    }

    public List<AssertionAppliesToIntegrationProfile> assertionsOfSelectedIntegrationProfile() {
        List<AssertionAppliesToIntegrationProfile> listDistinct = new ArrayList<AssertionAppliesToIntegrationProfile>();

        if (getSelectedIntegrationProfile() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionAppliesToIntegrationProfileQuery query = new AssertionAppliesToIntegrationProfileQuery(entityManager);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedIntegrationProfile().getKeyword()));
            listDistinct = query.getListDistinct();
        }
        return listDistinct;
    }

    public void unlinkSelectedIntegrationProfileFrom(int assertionId) {
        if (getSelectedIntegrationProfile() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            AssertionAppliesToIntegrationProfileQuery query = new AssertionAppliesToIntegrationProfileQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedIntegrationProfile().getKeyword()));

            AssertionAppliesToIntegrationProfile uniqueResult = query.getUniqueResult();

            entityManager.remove(uniqueResult);
            entityManager.flush();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void linkSelectedIntegrationProfileTo(int assertionId) {
        if (getSelectedIntegrationProfile() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            IntegrationProfileWrapper ipWrapper = getSelectedIntegrationProfile();
            AssertionAppliesToIntegrationProfile link = new AssertionAppliesToIntegrationProfile(ipWrapper.getKeyword(), ipWrapper.getName(), ipWrapper.getId(), ipWrapper.getBaseUrl(),
                    ipWrapper.getPermanentLink(), assertion);
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public boolean isIntegrationProfileCovered(String integrationProfileKeyword) {
        boolean result = false;
        AssertionAppliesToIntegrationProfileQuery query = new AssertionAppliesToIntegrationProfileQuery();
        query.linkedEntityKeyword().eq(integrationProfileKeyword);
        List<AssertionAppliesToIntegrationProfile> listDistinct = query.getListDistinct();
        if ((listDistinct != null) && (listDistinct.size() != 0)) {
            result = true;
        }
        return result;
    }

    public String getSelectedIntegrationProfileDescription() {
        return getSelectedIntegrationProfile().getDescription();
    }

    public boolean isSelectedIntegrationProfileLinkedTo(int assertionId) {
        boolean result = false;
        AssertionAppliesToIntegrationProfileQuery query = new AssertionAppliesToIntegrationProfileQuery();
        query.assertion().id().eq(assertionId);
        if (getSelectedIntegrationProfile() != null) {
            query.linkedEntityKeyword().eq(getSelectedIntegrationProfile().getKeyword());
        }
        AssertionAppliesToIntegrationProfile uniqueResult = query.getUniqueResult();
        if (uniqueResult != null) {
            result = true;
        }
        return result;
    }

    @Override
    public void selectedDomainUpdateCallback() {
        selectedActor = "";
        setSelectedIntegrationProfileName("");
        selectedIntegrationProfile = new IntegrationProfileWrapper();
    }

    public String getSelectedActor() {
        return selectedActor;
    }

    public void setSelectedActor(String selectedActorIn) {
        this.selectedActor = selectedActorIn;
        setSelectedIntegrationProfileName("");
    }

    public void selectedIntegrationProfileUpdate() {
    }

    public IntegrationProfileWrapper getSelectedIntegrationProfile() {
        return selectedIntegrationProfile;
    }

    public boolean isIntegrationProfileSelected() {
        return getSelectedIntegrationProfile().getId() != 0;
    }

}
