package net.ihe.gazelle.AssertionManager.scope;

import net.ihe.gazelle.oasis.testassertion.AssertionScope;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

@Name("assertionScopeConverter")
@BypassInterceptors
@Converter(forClass = AssertionScope.class)
public class AssertionScopeConverter implements javax.faces.convert.Converter {

    private EntityManager em = EntityManagerService.provideEntityManager();

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return em.find(AssertionScope.class, Integer.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((AssertionScope) value).getId().toString();
    }
}