package net.ihe.gazelle.AssertionManager.assertion;

import net.ihe.gazelle.AssertionManager.utils.UrlHelper;
import net.ihe.gazelle.AssertionManager.ws.wrapper.TmWsClient;
import net.ihe.gazelle.assertion.coverage.provider.client.CoverageProviderClient;
import net.ihe.gazelle.assertion.coverage.provider.data.CoveringEntity;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertion;
import net.ihe.gazelle.assertion.tags.TagKeywords;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.*;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tf.ws.data.RuleWrapper;
import net.ihe.gazelle.tm.ws.data.TestStepWrapper;
import net.ihe.gazelle.tm.ws.data.TestWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;
import java.nio.charset.StandardCharsets;

@Name("assertionManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AssertionManagerLocal")
public class AssertionManager implements AssertionManagerLocal, Serializable, UserAttributeCommon {

    private static final long serialVersionUID = 5537331687255134038L;
    private static final Logger LOG = LoggerFactory.getLogger(AssertionManager.class);
    private final TmWsClient tmWsClient = new TmWsClient();
    private OasisTestAssertionFilter filter;
    private FilterDataModel<OasisTestAssertion> assertions;
    private OasisTestAssertion selectedAssertion;
    private int selectedAssertionForCoverageId;
    private OasisTestAssertion assertionToDelete;
    private int assertionIdToDelete;
    private int ruleIdToDelete;

    private String wantedIdScheme;
    private String wantedAssertionId;
    private int testStepIdToDelete;
    private int testIdToDelete;
    private OasisRefSourceItem newDocument;
    private int transactionId;
    private int actorId;
    private int aipoId;
    private int standardId;
    private int auditMessageId;
    private int integrationProfileId;
    private String backTo;

    @In(value="gumUserService")
    private UserService userService;

    public static void updateCoverageCounts(EntityManager em) {
        LOG.info("updateAssertionCoverages");
        OasisTestAssertion.updateCoverageCounts(em);
        LOG.info("finished updateAssertionCoverages");
    }

    public OasisTestAssertionFilter getFilter() {
        if (filter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            filter = new OasisTestAssertionFilter(requestParameterMap);

            //fixing encoding
            try {
                UrlHelper.fixEncoding(filter);
            } catch (Exception e) {
                LOG.error("Encoding error : {}", e.getMessage());
            }
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        String coveredCheckboxes = fc.getExternalContext().getRequestParameterMap().get("coveredCheckboxes");
        if (coveredCheckboxes != null) {
            filter.setCoveredCheckboxes(new String[]{coveredCheckboxes});
        }
        String notCoveredCheckboxes = fc.getExternalContext().getRequestParameterMap().get("notCoveredCheckboxes");
        if (notCoveredCheckboxes != null) {
            filter.setNotCoveredCheckboxes(new String[]{notCoveredCheckboxes});
        }
        String appliedCheckboxes = fc.getExternalContext().getRequestParameterMap().get("appliedCheckboxes");
        if (appliedCheckboxes != null) {
            filter.setAppliedCheckboxes(new String[]{appliedCheckboxes});
        }
        return filter;
    }

    public FilterDataModel<OasisTestAssertion> getAssertions() {

        if (assertions == null) {
            assertions = new FilterDataModel<OasisTestAssertion>(getFilter()) {
                @Override
                protected Object getId(OasisTestAssertion t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }

        final FacesContext fc = FacesContext.getCurrentInstance();
        String rowKey = fc.getExternalContext().getRequestParameterMap().get("rowKey");
        if (rowKey != null) {
            assertions.setRowKey(rowKey);
            assertions.getFilter().modified();
        }
        return assertions;
    }

    public void persistAssertion(OasisTestAssertion assertion) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        assertion.setLastChanged(new Date());
        assertion.setLastModifierId(Identity.instance().getCredentials().getUsername());
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public void persistAssertionPredicate(OasisTestAssertion assertion) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(assertion.getPredicate());
        entityManager.flush();
        persistAssertion(assertion);
    }

    public void persistAssertionTag(OasisTestAssertion assertion, OasisTag tag) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(tag);
        entityManager.flush();
        persistAssertion(assertion);
    }

    public void persistAssertionPrescription(OasisTestAssertion assertion) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(assertion.getPrescription());
        entityManager.flush();
        persistAssertion(assertion);
    }

    public SelectItem[] getPrescriptionLevels() {
        final List<OasisPrescriptionLevelValues> list = Arrays.asList(OasisPrescriptionLevelValues.values());
        final List<SelectItem> result = new ArrayList<SelectItem>();
        for (OasisPrescriptionLevelValues lifecycleStatus : list) {
            result.add(new SelectItem(lifecycleStatus, lifecycleStatus.getFriendlyName()));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    public SelectItem[] getTagKeywords() {
        return TagKeywords.getTagkeywords();
    }

    public SelectItem[] getTargets() {

        List<OasisTarget> idSchemes = OasisTarget
                .getIdSchemesObjects(EntityManagerService.provideEntityManager());

        Collections.sort(idSchemes);

        final List<SelectItem> result = new ArrayList<SelectItem>();
        for (OasisTarget target : idSchemes) {
            result.add(new SelectItem(target, target.getIdScheme()));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    public SelectItem[] getAssertionStatus() {
        final List<TestAssertionStatus> list = Arrays.asList(TestAssertionStatus.values());
        final List<SelectItem> result = new ArrayList<SelectItem>();
        for (TestAssertionStatus lifecycleStatus : list) {
            result.add(new SelectItem(lifecycleStatus, lifecycleStatus.getFriendlyName()));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    public OasisTestAssertion getSelectedAssertion() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String id = fc.getExternalContext().getRequestParameterMap().get("id");
        String newAssertion = fc.getExternalContext().getRequestParameterMap().get("new");

        wantedIdScheme = fc.getExternalContext().getRequestParameterMap().get("idScheme");
        wantedAssertionId = fc.getExternalContext().getRequestParameterMap().get("assertionId");

        //fixing encoding
        try {
            wantedIdScheme = new String(wantedIdScheme.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
            wantedAssertionId = new String(wantedAssertionId.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        } catch (Exception e) {}

        backTo = fc.getExternalContext().getRequestParameterMap().get("backTo");

        if (wantedIdScheme != null && wantedAssertionId != null && !"1".equals(newAssertion)) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery();
            query.assertionId().eq(wantedAssertionId);
            query.target().idScheme().eq(wantedIdScheme);
            selectedAssertion = query.getUniqueResult();
        } else if ("1".equals(newAssertion)) {
            String wantedPage = fc.getExternalContext().getRequestParameterMap().get("page");
            forgeAssertion(wantedPage);
        } else if (id != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedAssertion = entityManager.find(OasisTestAssertion.class, Integer.valueOf(id));
        }
        return selectedAssertion;
    }

    private void forgeAssertion(String wantedPage) {

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        selectedAssertion = new OasisTestAssertion();
        if (wantedAssertionId != null) {
            selectedAssertion.setAssertionId(wantedAssertionId);
        }
        selectedAssertion.setPrescription(new OasisPrescription());
        selectedAssertion.setPredicate(new OasisPredicate());
        if (wantedIdScheme != null && !"".equals(wantedIdScheme)) {
            linkIdScheme(entityManager);
        } else {
            selectedAssertion.setTarget(new OasisTarget());
        }
        selectedAssertion.setStatus(TestAssertionStatus.TO_BE_REVIEWED);

        List<OasisTag> tags = new ArrayList<OasisTag>();
        OasisTag pageTag = new OasisTag("Page");
        pageTag.setContent("0");
        if (wantedPage != null && !wantedPage.equals("")) {
            pageTag.setContent(wantedPage);
        }
        tags.add(pageTag);

        OasisTag sectionTag = new OasisTag();
        tags.add(sectionTag);
        selectedAssertion.setTags(tags);
    }

    private void linkIdScheme(EntityManager entityManager) {
        OasisTargetQuery query = new OasisTargetQuery(entityManager);
        query.idScheme().eq(wantedIdScheme);

        OasisTarget target = query.getUniqueResult();
        if (target == null) {
            target = new OasisTarget();
        }
        selectedAssertion.setTarget(target);
        updateOasisTarget();
    }

    public void updateOasisTarget() {
        List<OasisNormativeSource> idSchemeNormativeSources = getIdSchemeNormativeSources();

        if (!idSchemeNormativeSources.isEmpty()) {
            selectedAssertion.setNormativeSource(idSchemeNormativeSources.get(0));
        } else {
            newDocument = new OasisRefSourceItem();
            selectedAssertion.setNormativeSource(null);
        }
        List<OasisTestAssertion> testAssertions = selectedAssertion.getTarget().getTestAssertions();
        String assertionId = selectedAssertion.getTarget().getIdScheme();
        if (testAssertions != null && testAssertions.size() > 0) {
            Collections.sort(testAssertions);
            OasisTestAssertion lastAssertion = testAssertions.get(testAssertions.size() - 1);
            assertionId = lastAssertion.getAssertionId();
        }
        selectedAssertion.setAssertionId(AssertionIdGenerator.getNextAssertionId(assertionId));
    }


    public void saveAssertion(OasisTestAssertion assertion) {
        selectedAssertion = assertion;
        saveAssertion();
    }

    public String saveAssertion() {
        LOG.info("Save assertion: " + selectedAssertion.getAssertionId());
        String returnTo;
        String actionDone;

        if (selectedAssertion.getId() == null) {
            actionDone = "created";
        } else {
            actionDone = "updated";
        }

        EntityManager em = EntityManagerService.provideEntityManager();

        if (selectedAssertion.getNormativeSource() == null) {

            OasisNormativeSource normativeSource = new OasisNormativeSource();
            List<OasisRefSourceItem> refSourceItems = new ArrayList<OasisRefSourceItem>();
            refSourceItems.add(getNewDocument());
            normativeSource.setRefSourceItems(refSourceItems);
            normativeSource = em.merge(normativeSource);

            selectedAssertion.setNormativeSource(normativeSource);
        }

        selectedAssertion.setLastChanged(new Date());
        selectedAssertion.setLastModifierId(Identity.instance().getCredentials().getUsername());
        selectedAssertion = em.merge(selectedAssertion);
        em.flush();

        returnTo = "/assertions/show.xhtml?id=" + selectedAssertion.getId();

        FacesMessages.instance()
                .add("Assertion " + selectedAssertion.getAssertionId() + " was successfully " + actionDone);
        return returnTo;
    }

    public String saveAssertionAndCreateNewOne() {
        saveAssertion();
        return newAssertion() + "&idScheme=" + selectedAssertion.getTarget().getIdScheme() + "&page=" + selectedAssertion.getPageNumber();
    }

    private List<OasisNormativeSource> getIdSchemeNormativeSources() {
        OasisNormativeSourceQuery query = new OasisNormativeSourceQuery(EntityManagerService.provideEntityManager());
        String idScheme = selectedAssertion.getTarget().getIdScheme();
        query.testAssertions().target().idScheme().eq(idScheme);
        return query.getListDistinct();
    }

    public String show(int id) {
        return "/assertions/show.xhtml?id=" + id;
    }

    public OasisTestAssertion getAssertionToDelete() {
        return assertionToDelete;
    }

    public void setAssertionToDelete(OasisTestAssertion assertionToDeleteIn) {
        this.assertionToDelete = assertionToDeleteIn;
    }

    public void delete() {
        if (assertionToDelete != null) {
            String assertionId = assertionToDelete.getAssertionId();

            EntityManager em = EntityManagerService.provideEntityManager();

            AssertionScopeQuery query = new AssertionScopeQuery(em);
            query.assertions().id().eq(assertionToDelete.getId());
            List<AssertionScope> listDistinct = query.getListDistinct();

            for (AssertionScope assertionScope : listDistinct) {
                assertionScope.getAssertions().remove(assertionToDelete);
                em.merge(assertionScope);
                em.flush();
            }

            assertionToDelete = em.find(OasisTestAssertion.class, assertionToDelete.getId());
            em.remove(assertionToDelete);
            em.flush();
            if (filter != null) {
                filter.modified();
            }

            FacesMessages.instance().add(Severity.WARN, "Assertion " + assertionId + " was successfully deleted");
        }
    }

    public List<CoveringEntity> getMbvCoverage(OasisTestAssertion assertion) {
        List<CoveringEntity> coveringEntities = new ArrayList<CoveringEntity>();
        if (assertion != null) {
            MbvServiceQuery query = new MbvServiceQuery(EntityManagerService.provideEntityManager());
            List<MbvService> listDistinct = query.getListDistinct();
            CoverageProviderClient client;
            WsAssertion testAssertionsCovered;
            for (MbvService mbvService : listDistinct) {
                client = new CoverageProviderClient(mbvService.getUrl());
                testAssertionsCovered = client
                        .getATestAssertionCoverage(assertion.getTarget().getIdScheme(), assertion.getAssertionId());
                if (testAssertionsCovered != null) {
                    List<CoveringEntity> coveringEntitiesTmp = testAssertionsCovered.getCoveringEntities();
                    if (coveringEntitiesTmp != null) {
                        for (CoveringEntity entity : coveringEntitiesTmp) {
                            entity.setBaseUrl(mbvService.getUrl());
                        }
                        coveringEntities.addAll(coveringEntitiesTmp);
                    }
                }
            }
        }
        return coveringEntities;
    }

    public List<CoveringEntity> getMbvCoverageOfSelectedAssertion() {
        return getMbvCoverage(getSelectedAssertionForCoverageId());
    }

    public List<RuleWrapper> getRules(OasisTestAssertion assertion) {
        List<RuleWrapper> ruleWrapperList = new ArrayList<RuleWrapper>();
        if (assertion != null && assertion.getTfRules() != null) {

            for (AssertionLinkedToRule ruleTmp : assertion.getTfRules()) {
                ruleWrapperList.add(tmWsClient.getRule(ruleTmp));
            }
        }

        return ruleWrapperList;
    }

    public List<RuleWrapper> getRulesOfSelectedAssertion() {
        return getRules(getSelectedAssertionForCoverageId());
    }

    public List<TestWrapper> getTests(OasisTestAssertion assertion) {

        List<TestWrapper> testWrapperList = new ArrayList<TestWrapper>();
        if (assertion != null && assertion.getTests() != null) {

            for (AssertionLinkedToTest testTmp : assertion.getTests()) {
                testWrapperList.add(tmWsClient.getTest(testTmp.getLinkedEntityKeyword()));
            }
        }
        return testWrapperList;
    }

    public List<TestWrapper> getTestsOfSelectedAssertion() {
        return getTests(getSelectedAssertionForCoverageId());
    }


    public List<TestStepWrapper> getTestSteps(OasisTestAssertion assertion) {
        List<TestStepWrapper> testWrapperList = new ArrayList<TestStepWrapper>();
        if (assertion != null && assertion.getTestSteps() != null) {
            for (AssertionLinkedToTestStep testStepTmp : assertion.getTestSteps()) {
                testWrapperList.add(tmWsClient.getTestSteps(testStepTmp));
            }
        }
        return testWrapperList;
    }

    public List<TestStepWrapper> getTestStepsOfSelectedAssertion() {
        return getTestSteps(getSelectedAssertionForCoverageId());
    }

    public void setLinkRuleToDelete(int assertionId, int ruleId) {
        assertionIdToDelete = assertionId;
        ruleIdToDelete = ruleId;
    }

    public void setLinkTestStepToDelete(int assertionId, int testStepId) {
        assertionIdToDelete = assertionId;
        testStepIdToDelete = testStepId;
    }

    public void setLinkTestToDelete(int assertionId, int testId) {
        assertionIdToDelete = assertionId;
        testIdToDelete = testId;
    }

    public void setLinkActorToDelete(int assertionId, int actorIdIn) {
        assertionIdToDelete = assertionId;
        this.actorId = actorIdIn;
    }

    public void setTransactionToDelete(int assertionId, int transactionIdIn) {
        assertionIdToDelete = assertionId;
        this.transactionId = transactionIdIn;
    }

    public void setLinkAipoToDelete(int assertionId, int aipoIdIn) {
        assertionIdToDelete = assertionId;
        this.aipoId = aipoIdIn;
    }

    public void setStandardToDelete(int assertionId, int standardIdIn) {
        assertionIdToDelete = assertionId;
        this.standardId = standardIdIn;
    }

    public void setIntegrationProfileToDelete(int assertionId, int integrationProfileIdIn) {
        assertionIdToDelete = assertionId;
        this.integrationProfileId = integrationProfileIdIn;
    }

    public void setAuditMessageToDelete(int assertionId, int auditMessageIdIn) {
        assertionIdToDelete = assertionId;
        this.auditMessageId = auditMessageIdIn;
    }

    public void deleteLinkedRule() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionLinkedToRuleQuery query = new AssertionLinkedToRuleQuery(entityManager);
        query.id().eq(ruleIdToDelete);

        AssertionLinkedToRule uniqueResult = query.getUniqueResult();

        entityManager.remove(uniqueResult);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    private void refreshSelectedAssertion(EntityManager entityManager) {
        selectedAssertion = entityManager.find(OasisTestAssertion.class, assertionIdToDelete);
        selectedAssertion.updateCoverage();
    }

    public void deleteLinkedTest() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionLinkedToTestQuery query = new AssertionLinkedToTestQuery(entityManager);
        query.assertion().id().eq(assertionIdToDelete);
        query.id().eq(testIdToDelete);

        AssertionLinkedToTest uniqueResult = query.getUniqueResult();

        entityManager.remove(uniqueResult);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public void deleteLinkedTestStep() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionLinkedToTestStepQuery query = new AssertionLinkedToTestStepQuery(entityManager);
        query.id().eq(testStepIdToDelete);

        AssertionLinkedToTestStep uniqueResult = query.getUniqueResult();

        entityManager.remove(uniqueResult);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public void deleteLinkedActor() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery(entityManager);
        query.assertion().id().eq(assertionIdToDelete);
        query.id().eq(actorId);
        AssertionAppliesToActor actorLink = query.getUniqueResult();

        entityManager.remove(actorLink);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public void deleteLinkedTransaction() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionAppliesToTransactionQuery query = new AssertionAppliesToTransactionQuery(entityManager);
        query.assertion().id().eq(assertionIdToDelete);
        query.id().eq(transactionId);
        AssertionAppliesToTransaction actorLink = query.getUniqueResult();

        entityManager.remove(actorLink);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public void deleteLinkedAipo() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery(entityManager);
        query.assertion().id().eq(assertionIdToDelete);
        query.id().eq(aipoId);

        AssertionAppliesToAipo aipoLink = query.getUniqueResult();

        entityManager.remove(aipoLink);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public void deleteLinkedStandard() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionAppliesToStandardQuery query = new AssertionAppliesToStandardQuery(entityManager);
        query.assertion().id().eq(assertionIdToDelete);
        query.id().eq(standardId);

        AssertionAppliesToStandard standardLink = query.getUniqueResult();

        entityManager.remove(standardLink);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public void deleteLinkedIntegrationProfile() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionAppliesToIntegrationProfileQuery query = new AssertionAppliesToIntegrationProfileQuery(entityManager);
        query.assertion().id().eq(assertionIdToDelete);
        query.id().eq(integrationProfileId);

        AssertionAppliesToIntegrationProfile standardLink = query.getUniqueResult();

        entityManager.remove(standardLink);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public void deleteLinkedAuditMessage() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionAppliesToAuditMessageQuery query = new AssertionAppliesToAuditMessageQuery(entityManager);
        query.assertion().id().eq(assertionIdToDelete);
        query.id().eq(auditMessageId);

        AssertionAppliesToAuditMessage standardLink = query.getUniqueResult();

        entityManager.remove(standardLink);
        entityManager.flush();
        refreshSelectedAssertion(entityManager);
    }

    public OasisTestAssertion getSelectedAssertionForCoverageId() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        return entityManager.find(OasisTestAssertion.class, selectedAssertionForCoverageId);
    }

    public void setSelectedAssertionForCoverageId(int selectedAssertionForCoverage) {
        LOG.info("setSelectedAssertionForCoverage");
        this.selectedAssertionForCoverageId = selectedAssertionForCoverage;
    }

    public String newAssertion() {
        return "/assertions/new.xhtml?new=1";
    }

    public String cancel() {
        return "/assertions/index.xhtml";
    }


    public OasisRefSourceItem getNewDocument() {
        return newDocument;
    }

    public void setNewDocument(OasisRefSourceItem document) {
        this.newDocument = document;
    }

    public String getWantedIdScheme() {
        return wantedIdScheme;
    }

    public void setWantedIdScheme(String wantedIdSchemeIn) {
        this.wantedIdScheme = wantedIdSchemeIn;
    }

    public String getWantedAssertionId() {
        return wantedAssertionId;
    }

    public void setWantedAssertionId(String wantedAssertionIdIn) {
        this.wantedAssertionId = wantedAssertionIdIn;
    }

    public boolean validateAssertionId() {
        if (selectedAssertion == null) {
            LOG.info("selectedAssertion is null");
            return false;
        }
        if ((selectedAssertion.getAssertionId() == null) || selectedAssertion.getAssertionId().trim().equals("")) {
            LOG.info("selectedAssertion getAssertionId  is null or empty");
            return false;
        }
        OasisTestAssertionQuery query = new OasisTestAssertionQuery();
        query.target().idScheme().eq(selectedAssertion.getTarget().getIdScheme());
        query.assertionId().eq(selectedAssertion.getAssertionId());
        List<OasisTestAssertion> listDistinct = query.getListDistinct();
        if (listDistinct.size() > 0) {
            FacesMessages.instance().addToControl("assertionIdInput", "Already used");
            LOG.info("selectedAssertion getAssertionId is Already used");
            return false;
        }
        LOG.info("selectedAssertion getAssertionId is ok");
        return true;
    }

    public int getAuditMessageId() {
        return auditMessageId;
    }

    public void setAuditMessageId(int auditMessageIdIn) {
        this.auditMessageId = auditMessageIdIn;
    }

    public SelectItem[] getDocuments() {
        OasisRefSourceItemQuery query = new OasisRefSourceItemQuery();
        List<OasisRefSourceItem> list = query.getList();
        Collections.sort(list);
        final List<SelectItem> result = new ArrayList<SelectItem>();
        for (OasisRefSourceItem refSource : list) {
            result.add(new SelectItem(refSource, refSource.getName()));
        }
        return result.toArray(new SelectItem[result.size()]);
    }


    public String getBackTo() {
        return backTo;
    }

    public void setBackTo(String backToIn) {
        this.backTo = backToIn;
    }

    public String getFilteredIdScheme() {
        String result = "";
        if (!getFilter().getFilterValues().isEmpty()) {
            result = getFilter().getFilterValues().get("idScheme").toString();
        }
        return result;
    }

    // URL encoding fix
    public String getFilterConstraints() {
        return getFilter().getFilterUrlParameters().replace("&", ", ");
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}