package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import net.ihe.gazelle.oasis.testassertion.OasisTargetQuery;

import javax.persistence.EntityManager;
import java.util.Map;

public class IdSchemesFilter extends Filter<OasisTarget> {


    private static final long serialVersionUID = 173551868348004894L;

    public IdSchemesFilter(Map<String, String> requestParameterMap) {
        super(getHQLCriterions(), requestParameterMap);
    }

    private static HQLCriterionsForFilter<OasisTarget> getHQLCriterions() {

        OasisTargetQuery query = new OasisTargetQuery(EntityManagerService.provideEntityManager());
        HQLCriterionsForFilter<OasisTarget> result = query.getHQLCriterionsForFilter();

        result.addPath("idScheme", query.idScheme());
        result.addPath("scope", query.testAssertions().scopes().keyword());
        result.addPath("mbvService", query.testAssertions().mbv().mbvService().keyword());

        return result;
    }

    @Override
    public EntityManager getEntityManager() {
        return EntityManagerService.provideEntityManager();
    }

}
