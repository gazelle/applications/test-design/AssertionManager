package net.ihe.gazelle.AssertionManager.cache;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <b>Class Description : </b>FileCache<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 22/08/16
 * @class FileCache
 * @package net.ihe.gazelle.AssertionManager.cache
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Name("fileCache")
@Scope(ScopeType.EVENT)
public class FileCache {

    public void clearCache() {
        net.ihe.gazelle.common.filecache.FileCache.clearCache();
    }
}

