package net.ihe.gazelle.AssertionManager.utils;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("preferencesManager")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("PreferencesManagerLocal")
public class PreferencesManager implements PreferencesManagerLocal {

    public static PreferencesManager instance() {
        return (PreferencesManager) Component.getInstance("preferencesManager");
    }

    public static String getAssertionManagerRestUrl() {
        return PreferenceService.getString("assertion_manager_rest_url");
    }
    public String getCguUrl(){
            return PreferenceService.getString("link_to_cgu");
    }
    public String getGazelleTestManagementRestUrl() {
        return PreferenceService.getString("gazelle_test-management-url");
    }

    public static String getAssertionManagerRestPathToAssertion() {
        return PreferenceService.getString("assertion_manager_rest_path_to_assertion");
    }

    public static String getGazelleTMUrl() {
        return PreferenceService.getString("gazelle_tm_url");
    }

    public static String getAssertionRestPath(String contextUrl) {
        return UrlHelper.concatURLtoPath(contextUrl, "rest/testAssertion/assertion");
    }

}
