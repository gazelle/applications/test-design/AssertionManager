package net.ihe.gazelle.AssertionManager.assertion.xml.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DocumentSection")
public class DocumentSectionResponse extends JaxbString {

    public DocumentSectionResponse() {
        super();
    }

    public DocumentSectionResponse(String v) {
        super(v);
    }

}
