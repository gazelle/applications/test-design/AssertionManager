package net.ihe.gazelle.AssertionManager.assertion.link;

import net.ihe.gazelle.AssertionManager.assertion.OasisTestAssertionFilter;
import net.ihe.gazelle.AssertionManager.ws.wrapper.TmWsClient;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.filter.util.MapNotifier;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertionQuery;
import net.ihe.gazelle.tm.ws.data.TestDescriptionWrapper;
import net.ihe.gazelle.tm.ws.data.TestWrapper;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

@GenerateInterface(value = "AssertionLinkManagerLocal")
public abstract class AssertionLinkManager implements AssertionLinkManagerLocal {

    public static final String NO_TEST_DESCRIPTION_AVAILABLE = "No test description available";
    private final TmWsClient tmWsClient = new TmWsClient();
    private TestWrapper selectedTest;
    private String selectedDomain = "";
    private String selectedIntegrationProfileName = "";
    private String selectedTestType = "";
    private String selectedTransactionName;
    private String selectedActorName;

    private OasisTestAssertionFilter filter;

    private FilterDataModel<OasisTestAssertion> assertions;
    private GazelleListDataModel<TestWrapper> foundTests;
    private List<String> integrationProfilesWithTests;

    public abstract void selectedIntegrationProfileUpdate();

    public abstract void selectedDomainUpdateCallback();

    public OasisTestAssertionFilter getFilter() {
        if (filter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            filter = new OasisTestAssertionFilter(requestParameterMap);
        }
        return filter;
    }

    public OasisTestAssertionFilter onlyTestableAssertion(OasisTestAssertionFilter filterIn) {

        filterIn.addQueryModifier(new QueryModifier<OasisTestAssertion>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<OasisTestAssertion> hqlQueryBuilder, Map<String, Object> map) {
                OasisTestAssertionQuery query = new OasisTestAssertionQuery(hqlQueryBuilder);
                hqlQueryBuilder.addRestriction(query.testable().eqRestriction(true));

            }
        });
        return filterIn;
    }

    private HQLQueryBuilder<OasisTestAssertion> onlyTestableAssertions() {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery();
        HQLQueryBuilder<OasisTestAssertion> queryBuilder = (HQLQueryBuilder<OasisTestAssertion>) query.getQueryBuilder();
        queryBuilder.addRestriction(query.testable().eqRestriction(true));
        return queryBuilder;
    }

    public FilterDataModel<OasisTestAssertion> getAssertions() {
        if (assertions == null) {
            assertions = new FilterDataModel<OasisTestAssertion>(getFilter()) {
                private static final long serialVersionUID = 407502369946093644L;

                @Override
                protected Object getId(OasisTestAssertion t) {
                    return t.getId();
                }
            };
        }
        return assertions;
    }

    public boolean isIdSchemeSelected() {
        return !("".equals(getFilteredIdScheme()));
    }

    public String getFilteredIdScheme() {
        String result = "";
        if (!getFilter().getFilterValues().isEmpty()) {
            result = getFilter().getFilterValues().get("idScheme").toString();
        }
        return result;
    }

    public GazelleListDataModel<TestWrapper> getTests() {
        if (foundTests == null) {

            foundTests = tmWsClient.getTests(getSelectedIntegrationProfileName(), getSelectedDomain(), getSelectedTestType(),
                    getSelectedActorName(), getSelectedTransactionName());
        }
        return foundTests;
    }

    public List<String> getDomainsAvailable() {
        return tmWsClient.getDomainsWithTestsAvailable();
    }

    public void setSelectedTest(TestWrapper test) {
        this.selectedTest = tmWsClient.getTest(String.valueOf(test.getId()));
    }

    public void resetSelectedTest() {
        this.selectedTest = null;
    }

    public boolean isTestSelected() {
        return getSelectedTest() != null;
    }

    public String getSelectedTestDescription() {
        String response = NO_TEST_DESCRIPTION_AVAILABLE;
        if (selectedTest != null) {
            List<TestDescriptionWrapper> descriptionList = selectedTest.getDescriptionList();
            response = getFirstDescription(descriptionList);
        }
        return response;
    }

    private String getFirstDescription(List<TestDescriptionWrapper> descriptionList) {
        String response = NO_TEST_DESCRIPTION_AVAILABLE;
        if (descriptionList != null && !descriptionList.isEmpty() && descriptionList.get(0) != null && descriptionList.get(0).getDescription() != null) {
            response = descriptionList.get(0).getDescription().replace("<p>&nbsp;</p>", "");
        }
        return response;
    }

    public TestWrapper getSelectedTest() {
        return selectedTest;
    }

    public OasisTestAssertion getAssertionFromId(int assertionId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(entityManager);
        query.id().eq(assertionId);
        return query.getUniqueResult();
    }

    public String getSelectedIntegrationProfileName() {
        return selectedIntegrationProfileName;
    }

    public List<String> getIntegrationProfilesNames() {
        if (integrationProfilesWithTests == null) {
            integrationProfilesWithTests = tmWsClient.getIntegrationProfilesNamesWithTests(selectedDomain);
        }
        return integrationProfilesWithTests;
    }

    public void setSelectedIntegrationProfileName(String selectedIntegrationProfile) {
        this.selectedIntegrationProfileName = selectedIntegrationProfile;
        foundTests = null;
        selectedIntegrationProfileUpdate();
    }

    public String getSelectedDomain() {
        return selectedDomain;
    }

    public void setSelectedDomain(String selectedDomainIn) {
        if (selectedDomainIn == null) {
            selectedIntegrationProfileName = "";
        } else if (!selectedDomainIn.equals(this.selectedDomain)) {
            selectedIntegrationProfileName = "";
        }
        this.selectedDomain = selectedDomainIn;
        selectedDomainUpdate();
    }

    public void selectedDomainUpdate() {
        integrationProfilesWithTests = null;
        foundTests = null;
        selectedDomainUpdateCallback();
    }

    public String getSelectedTestType() {
        return selectedTestType;
    }

    public void setSelectedTestType(String selectedTestTypeIn) {
        this.selectedTestType = selectedTestTypeIn;
        foundTests = null;
    }

    public List<String> getTestTypes() {
        return tmWsClient.getTestTypes(getSelectedDomain(), getSelectedIntegrationProfileName());
    }

    public boolean isSelectedTestLinkedTo(int assertionId) {
        return false;
    }

    public String getSelectedTransactionName() {
        return selectedTransactionName;
    }

    public void setSelectedTransactionName(String selectedTransactionNameIn) {
        this.selectedTransactionName = selectedTransactionNameIn;
        this.foundTests = null;
    }

    public String getSelectedActorName() {
        return selectedActorName;
    }

    public void setSelectedActorName(String selectedActorNameIn) {
        this.selectedActorName = selectedActorNameIn;
        this.foundTests = null;
    }

    public void setSelectedIdScheme(String idScheme) {
        getFilter().resetField("idScheme");
        MapNotifier filterValues = getFilter().getFilterValues();
        filterValues.put("idScheme", idScheme);
        getFilter().setFilterValues(filterValues);
        getFilter().modified();
    }

    public TmWsClient getTmWsClient() {
        return tmWsClient;
    }

    public abstract boolean isSelectedTechnicalFrameworkTo(int assertionId);

    public abstract void unlinkSelectedTechnicalFrameworkFrom(int assertionId);

    public abstract void linkSelectedTechnicalFrameworkTo(int assertionId);

    public abstract boolean isTechnicalFrameworkSelected();


    public void linkAll() {
        final FacesContext fc = FacesContext.getCurrentInstance();
        for (OasisTestAssertion currentAssertion : getAssertions().getAllItems(fc)) {
            if (!isSelectedTechnicalFrameworkTo(currentAssertion.getId())) {
                linkSelectedTechnicalFrameworkTo(currentAssertion.getId());
            }
        }
    }

    public void unlinkAll() {
        final FacesContext fc = FacesContext.getCurrentInstance();
        for (OasisTestAssertion currentAssertion : getAssertions().getAllItems(fc)) {
            if (isSelectedTechnicalFrameworkTo(currentAssertion.getId())) {
                unlinkSelectedTechnicalFrameworkFrom(currentAssertion.getId());
            }
        }
    }

    public int countAssertionsNotLinked() {
        int total = 0;
        final FacesContext fc = FacesContext.getCurrentInstance();
        for (OasisTestAssertion currentAssertion : getAssertions().getAllItems(fc)) {
            if (!isSelectedTechnicalFrameworkTo(currentAssertion.getId())) {
                total++;
            }
        }
        return total;
    }

    public int countAssertionsLinked() {
        int total = 0;
        final FacesContext fc = FacesContext.getCurrentInstance();
        for (OasisTestAssertion currentAssertion : getAssertions().getAllItems(fc)) {
            if (isSelectedTechnicalFrameworkTo(currentAssertion.getId())) {
                total++;
            }
        }
        return total;
    }

}
