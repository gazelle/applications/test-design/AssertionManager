package net.ihe.gazelle.AssertionManager.assertion.xml.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Document")
public class DocumentResponse extends JaxbString {

    public DocumentResponse() {
        super();
    }

    public DocumentResponse(String v) {
        super(v);
    }
}
