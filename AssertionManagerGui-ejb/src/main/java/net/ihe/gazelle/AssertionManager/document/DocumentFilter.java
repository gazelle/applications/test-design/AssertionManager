package net.ihe.gazelle.AssertionManager.document;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisRefSourceItemQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>DocumentFilter<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 18/07/16
 * @class DocumentFilter
 * @package net.ihe.gazelle.AssertionManager.document
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
public class DocumentFilter extends Filter<OasisRefSourceItem> implements QueryModifier<OasisRefSourceItem> {
    private static final long serialVersionUID = 1013681492907105538L;

    private boolean unusedDocument = false;
    private boolean duplicated = false;

    public DocumentFilter(Map<String, String> requestParameterMap) {
        super(getHQLCriterions(), requestParameterMap);
        queryModifiers.add(this);
    }

    private static HQLCriterionsForFilter<OasisRefSourceItem> getHQLCriterions() {
        OasisRefSourceItemQuery query = new OasisRefSourceItemQuery();
        HQLCriterionsForFilter<OasisRefSourceItem> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("idScheme", query.normativeSources().testAssertions().target().idScheme());
        criteria.addPath("name", query.name());
        criteria.addPath("revision", query.revisionId());
        criteria.addPath("uri", query.uri());
        criteria.addPath("provenance", query.resourceProvenanceId());
        return criteria;
    }


    public boolean isUnusedDocument() {
        return unusedDocument;
    }

    public void setUnusedDocument(boolean unusedDocument) {
        this.unusedDocument = unusedDocument;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<OasisRefSourceItem> hqlQueryBuilder, Map<String, Object> map) {
        if (unusedDocument) {
            OasisRefSourceItemQuery query = new OasisRefSourceItemQuery(hqlQueryBuilder);
            HQLRestriction emptyRefSourceRestriction = query.normativeSources().testAssertions().isEmptyRestriction();
            hqlQueryBuilder.addRestriction(emptyRefSourceRestriction);
        }
        if (duplicated) {
            List<String> duplicatedUrls = new ArrayList<>();
            OasisRefSourceItemQuery query = new OasisRefSourceItemQuery();
            List<Object[]> statistics =
                    query.uri().getStatistics();
            for (Object[] statistic : statistics) {
                String url = (String) statistic[0];
                Long count = (Long) statistic[1];
                if (count > 1) {
                    duplicatedUrls.add(url);
                }
            }

            OasisRefSourceItemQuery queryDuplicates = new OasisRefSourceItemQuery(hqlQueryBuilder);
            HQLRestriction hqlRestriction = queryDuplicates.uri().inRestriction(duplicatedUrls);
            hqlQueryBuilder.addRestriction(hqlRestriction);

        }
    }


    public boolean isDuplicated() {
        return duplicated;
    }

    public void setDuplicated(boolean duplicated) {
        this.duplicated = duplicated;
    }
}
