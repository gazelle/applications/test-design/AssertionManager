package net.ihe.gazelle.AssertionManager.mbv;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.async.AsynchronousEvent;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.faces.FacesMessages;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Name("mbvServiceManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "MbvServiceManagerLocal")
public class MbvServiceManager implements MbvServiceManagerLocal, Serializable {

    private static final long serialVersionUID = 6567219594470453848L;

    private boolean showEditPanel = false;

    private MbvService selectedMbvService;
    private OasisTarget selectedOasisTarget;


    public List<MbvService> getMbvServices() {
        MbvServiceQuery query = new MbvServiceQuery(EntityManagerService.provideEntityManager());
        query.keyword().order(true);
        return query.getList();
    }

    public void newMbvService() {
        showEditPanel();
        selectedMbvService = new MbvService();
    }

    public OasisTarget getSelectedOasisTarget() {
        return selectedOasisTarget;
    }

    public void setSelectedOasisTarget(OasisTarget selectedOasisTarget) {
        this.selectedOasisTarget = selectedOasisTarget;
    }

    public void setSelectedOasisTarget(String idScheme) {
        this.selectedOasisTarget = new OasisTarget(idScheme);
    }

    public boolean isShowEditPanel() {
        return showEditPanel;
    }

    public void showEditPanel() {
        setShowEditPanel(true);
    }

    public void hideEditPanel() {
        setShowEditPanel(false);
    }

    public void setShowEditPanel(boolean showEditPanelIn) {
        this.showEditPanel = showEditPanelIn;
    }

    public MbvService getSelectedMbvService() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String id = fc.getExternalContext().getRequestParameterMap().get("id");

        if (id != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            selectedMbvService = em.find(MbvService.class, Integer.valueOf(id));
        }
        return selectedMbvService;
    }

    public void setSelectedMbvService(MbvService selectedMbvServiceIn) {
        this.selectedMbvService = selectedMbvServiceIn;
    }

    public void cancel() {
        hideEditPanel();
        selectedMbvService = null;
    }

    public void save() {
        hideEditPanel();
        EntityManager em = EntityManagerService.provideEntityManager();
        em.merge(selectedMbvService);
        em.flush();
    }

    public void edit(MbvService mbvService) {
        showEditPanel();
        selectedMbvService = mbvService;
    }

    public void delete() {
        hideEditPanel();
        EntityManager em = EntityManagerService.provideEntityManager();
        MbvCoverageStatusQuery mbvCoverageStatusQuery = new MbvCoverageStatusQuery();
        mbvCoverageStatusQuery.mbvService().id().eq(selectedMbvService.getId());
        List<MbvCoverageStatus> mbvCoverageStatusToDelete = mbvCoverageStatusQuery.getList();
        for (MbvCoverageStatus mbvCoverageStatus : mbvCoverageStatusToDelete) {
            em.remove(mbvCoverageStatus);
        }
        selectedMbvService = em.find(MbvService.class, selectedMbvService.getId());
        em.remove(selectedMbvService);
        em.flush();
    }

    public void saveNewTarget() {
        EntityManager em = EntityManagerService.provideEntityManager();
        selectedOasisTarget = em.merge(selectedOasisTarget);
        FacesMessages.instance().add(selectedOasisTarget.getIdScheme() + " idScheme was successfully created");
    }

    public String refreshCoverageOf(int mbvServiceId) {
        List<Integer> mbvServiceIds = new ArrayList<>();
        mbvServiceIds.add(mbvServiceId);
        QuartzDispatcher.instance().scheduleAsynchronousEvent("updateMbvCoverage", mbvServiceIds);
        return "/mbvServices/index.xhtml";
    }

    public String refreshCoverages() {
        MbvServiceQuery query = new MbvServiceQuery();
        List<Integer> mbvServiceIds = query.id().getListDistinct();
        QuartzDispatcher.instance().scheduleAsynchronousEvent("updateMbvCoverage", mbvServiceIds);
        return "/mbvServices/index.seam";
    }

    public List<MbvCoverageStatus> getAssertionsCovered() {
        return getSelectedMbvService().getAssertionsCovered(EntityManagerService.provideEntityManager());

    }

    public List<String> getMissingOasisTarget() {
        return getSelectedMbvService().getMissingOasisTarget(
                EntityManagerService.provideEntityManager());
    }

    public List<MbvCoverageStatus> getMissingOasisTestAssertion() {
        return getSelectedMbvService().getMissingOasisTestAssertion(
                EntityManagerService.provideEntityManager());
    }

    public List<MbvCoverageStatus> getCoverageRemoved() {
        return getSelectedMbvService().getCoverageRemoved(EntityManagerService.provideEntityManager());
    }

    public GazelleListDataModel<Job> getScheduledJobs() throws SchedulerException {
        Scheduler scheduler = QuartzDispatcher.instance().getScheduler();
        List<Job> jobs = new ArrayList<>();
        for (String groupName : scheduler.getJobGroupNames()) {
            for (String jobName : scheduler.getJobNames(groupName)) {
                Trigger[] triggersOfJob = scheduler.getTriggersOfJob(jobName, groupName);
                JobDetail jobDetail = scheduler.getJobDetail(jobName, groupName);
                AsynchronousEvent type = new AsynchronousEvent("AsynchronousEvent", null);
                if (jobDetail.getJobDataMap() != null) {
                    type = (AsynchronousEvent) jobDetail.getJobDataMap().get("async");
                }
                jobs.add(new Job(type.toString(), triggersOfJob[0].getStartTime()));
            }
        }
        return new GazelleListDataModel<Job>(jobs);
    }

    public boolean idSchemeExists(String idScheme) {
        OasisTargetQuery query = new OasisTargetQuery();
        query.idScheme().eq(idScheme);
        int count = query.getCount();
        if (count > 0) {
            return true;
        }
        return false;
    }

    static class Job {
        private String name;
        private Date start;

        Job(String name, Date start) {
            this.name = name;
            this.start = start;
        }

        public String getName() {
            return name;
        }

        public Date getStart() {
            return start;
        }
    }
}
