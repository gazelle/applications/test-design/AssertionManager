package net.ihe.gazelle.AssertionManager.assertion.link.appliesTo;

import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToTransaction;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToTransactionQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tf.ws.data.TransactionWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("appliesToTransactionManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AppliesToTransactionManagerLocal")
public class AppliesToTransactionManager extends AssertionLinkManager implements AppliesToTransactionManagerLocal, Serializable {


    private static final long serialVersionUID = -4181671477580363694L;
    private TransactionWrapper selectedTransaction = new TransactionWrapper();

    private static final Logger LOG = LoggerFactory.getLogger(AppliesToTransactionManager.class);
    private GazelleListDataModel<TransactionWrapper> transactions;

    @Override
    public List<String> getDomainsAvailable() {
        return getTmWsClient().getDomainsAvailable();
    }

    public List<String> getIntegrationProfiles() {
        return getTmWsClient().getIntegrationProfilesNames(getSelectedDomain());
    }

    public String getSelectedTransactionDescription() {
        return selectedTransaction.getDescription();
    }

    public void setSelectedTransaction(TransactionWrapper transaction) {
        selectedTransaction = getFullTransaction(transaction);
        LOG.info("setSelectedTransaction " + selectedTransaction);
    }

    private TransactionWrapper getFullTransaction(TransactionWrapper transaction) {
        return getTmWsClient().getFullTransaction(transaction.getId());
    }

    public TransactionWrapper getSelectedTransaction() {
        return selectedTransaction;
    }

    public GazelleListDataModel<TransactionWrapper> getTransactions() {
        if (transactions == null) {
            transactions = getTmWsClient().getTransactions(getSelectedDomain());
        }
        return transactions;
    }

    public boolean isTransactionSelected() {
        return getSelectedTransaction().getKeyword() != null;
    }

    public List<AssertionAppliesToTransaction> assertionsOfSelectedTransaction() {
        List<AssertionAppliesToTransaction> listDistinct = new ArrayList<AssertionAppliesToTransaction>();

        if (getSelectedTransaction() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionAppliesToTransactionQuery query = new AssertionAppliesToTransactionQuery(entityManager);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedTransaction().getKeyword()));
            listDistinct = query.getListDistinct();
        }
        return listDistinct;
    }

    public void unlinkSelectedTransactionFrom(int assertionId) {
        if (getSelectedTransaction() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            AssertionAppliesToTransactionQuery query = new AssertionAppliesToTransactionQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedTransaction().getKeyword()));

            AssertionAppliesToTransaction uniqueResult = query.getUniqueResult();

            entityManager.remove(uniqueResult);
            entityManager.flush();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void linkSelectedTransactionTo(int assertionId) {
        if (getSelectedTransaction() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            TransactionWrapper transaction = getSelectedTransaction();
            AssertionAppliesToTransaction link = new AssertionAppliesToTransaction(transaction.getKeyword(), transaction.getName(), transaction.getId(), transaction.getBaseUrl(),
                    transaction.getPermanentLink(), assertion);
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public boolean isTransactionCovered(String transactionKeyword) {
        boolean result = false;
        AssertionAppliesToTransactionQuery query = new AssertionAppliesToTransactionQuery();
        query.linkedEntityKeyword().eq(transactionKeyword);
        List<AssertionAppliesToTransaction> listDistinct = query.getListDistinct();
        if ((listDistinct != null) && (listDistinct.size() != 0)) {
            result = true;
        }
        return result;
    }

    public boolean isSelectedTransactionLinkedTo(int assertionId) {
        boolean result = false;
        AssertionAppliesToTransactionQuery query = new AssertionAppliesToTransactionQuery();
        query.assertion().id().eq(assertionId);
        if (getSelectedTransaction() != null) {
            query.linkedEntityKeyword().eq(getSelectedTransaction().getKeyword());
        }
        AssertionAppliesToTransaction uniqueResult = query.getUniqueResult();
        if (uniqueResult != null) {
            result = true;
        }
        return result;
    }

    @Override
    public void selectedIntegrationProfileUpdate() {
    }

    @Override
    public void selectedDomainUpdate() {
        selectedTransaction = new TransactionWrapper();
        transactions = null;
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedTransactionLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedTransactionFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedTransactionTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isTransactionSelected();
    }

    @Override
    public void selectedDomainUpdateCallback() {
        // TODO Auto-generated method stub

    }
}
