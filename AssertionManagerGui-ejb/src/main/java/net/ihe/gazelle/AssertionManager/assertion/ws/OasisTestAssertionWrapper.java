package net.ihe.gazelle.AssertionManager.assertion.ws;

import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "testAssertionSet")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisTestAssertionWrapper implements Serializable {


    private static final long serialVersionUID = 6112977269930792213L;

    public OasisTestAssertionWrapper(List<OasisTestAssertion> listDistinct) {
        oasisTestAssertions = listDistinct;
        common = new Common();
        OasisTestAssertion assertion = oasisTestAssertions.get(0);
        common.setNormativeSource(assertion.getNormativeSource());
        common.setTarget(assertion.getTarget());
    }

    public OasisTestAssertionWrapper() {
    }

    @XmlElement(name = "common")
    private Common common;

    @XmlElement(name = "testAssertion")
    private List<OasisTestAssertion> oasisTestAssertions = new ArrayList<OasisTestAssertion>();

    public List<OasisTestAssertion> getOasisTestAssertions() {
        return oasisTestAssertions;
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public void setOasisTestAssertions(List<OasisTestAssertion> oasisTestAssertions) {
        this.oasisTestAssertions = oasisTestAssertions;
    }
}