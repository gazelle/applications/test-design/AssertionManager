package net.ihe.gazelle.AssertionManager.assertion.ws;

import net.ihe.gazelle.AssertionManager.assertion.ws.api.TestAssertionApi;
import net.ihe.gazelle.AssertionManager.assertion.xml.response.IdSchemeResponse;
import net.ihe.gazelle.assertion.ws.data.WsAssertion;
import net.ihe.gazelle.assertion.ws.data.WsAssertionWrapper;
import net.ihe.gazelle.assertion.ws.data.WsIdScheme;
import net.ihe.gazelle.assertion.ws.data.WsIdSchemeWrapper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.*;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Name("testAssertion")
public class TestAssertion implements TestAssertionApi {

    private static final Logger LOG = LoggerFactory.getLogger(TestAssertion.class);

    public List<IdSchemeResponse> getIdSchemes() throws JAXBException {
        LOG.info("start getIdSchemes");
        List<IdSchemeResponse> result = new ArrayList<IdSchemeResponse>();
        List<String> idSchemes = OasisTarget.getIdSchemes(EntityManagerService.provideEntityManager());

        for (String idScheme : idSchemes) {
            result.add(new IdSchemeResponse(idScheme));
        }
        LOG.info("end getIdSchemes");
        return result;
    }

    public Response getTestAssertionByIdAndIdScheme(String assertionId, String idScheme, String format) throws JAXBException, UnsupportedEncodingException {
        LOG.info("start getTestAssertionByIdAndIdScheme");
        Response response;

        if (format == null) {
            java.net.URI location = null;
            if(assertionId == null){
                try {
                    location = new java.net.URI("../assertions/index.seam?idScheme=" + idScheme);
                } catch (URISyntaxException e) {
                    LOG.error("Cannot create URI:  ../assertions/index.seam?idScheme=" + idScheme);
                }
            }else{
                try {
                    location = new java.net.URI("../assertions/show.seam?idScheme=" + idScheme + "&assertionId=" + assertionId);
                } catch (URISyntaxException e) {
                    LOG.error("Cannot create URI:  ../assertions/show.seam?idScheme=" + idScheme + "&assertionId=" + assertionId);
                }
            }

            response = Response.temporaryRedirect(location).build();
        } else {
            TestAssertionRequest testAssertionRequest = new TestAssertionRequest(assertionId, idScheme, EntityManagerService.provideEntityManager());
            testAssertionRequest.getTestAssertionByIdAndIdScheme();
            LOG.info("end getTestAssertionByIdAndIdScheme");

            response = testAssertionRequest.getResponse();
        }
        return response;
    }

    public Response getTestAssertionDocument(String assertionId, String idScheme) throws JAXBException {
        LOG.info("start getTestAssertionDocument assertionId: " + assertionId + " , idScheme: " + idScheme);
        TestAssertionRequest testAssertionRequest = new TestAssertionRequest(assertionId, idScheme, EntityManagerService.provideEntityManager());
        testAssertionRequest.getTestAssertionDocument();
        LOG.info("end getTestAssertionDocument");
        return testAssertionRequest.getResponse();
    }

    public Response getTestAssertionDocumentSection(String assertionId, String idScheme) throws JAXBException {
        LOG.info("start getTestAssertionDocumentSection");
        TestAssertionRequest testAssertionRequest = new TestAssertionRequest(assertionId, idScheme, EntityManagerService.provideEntityManager());
        testAssertionRequest.getTestAssertionDocumentSection();
        LOG.info("end getTestAssertionDocumentSection");
        return testAssertionRequest.getResponse();
    }

    public List<OasisTestAssertion> getTestAssertions() throws JAXBException {
        LOG.info("start getTestAssertions");
        List<OasisTestAssertion> testAssertions = OasisTestAssertion.getTestAssertions(EntityManagerService.provideEntityManager());
        LOG.info("end getTestAssertions");
        return testAssertions;
    }

    public void openTestAssertionDocument(@QueryParam("id") String assertionId, @QueryParam("idScheme") String idScheme) throws JAXBException {
        LOG.info("start openTestAssertionDocument");
        TestAssertionRequest testAssertionRequest = new TestAssertionRequest(assertionId, idScheme, EntityManagerService.provideEntityManager());
        testAssertionRequest.openTestAssertionDocument();
        LOG.info("end openTestAssertionDocument");
    }

    public void openTestAssertionDocumentSection(@QueryParam("id") String assertionId, @QueryParam("idScheme") String idScheme) throws JAXBException {
        LOG.info("start openTestAssertionDocumentSection");
        TestAssertionRequest testAssertionRequest = new TestAssertionRequest(assertionId, idScheme, EntityManagerService.provideEntityManager());
        testAssertionRequest.openTestAssertionDocumentSection();
        LOG.info("end openTestAssertionDocumentSection");
    }


    public WsIdSchemeWrapper getIdSchemesV2() throws JAXBException {

        List<String> idSchemes = OasisTarget.getIdSchemes(EntityManagerService.provideEntityManager());

        List<WsIdScheme> wsAssertionList = new ArrayList<WsIdScheme>();
        for (String idScheme : idSchemes) {
            wsAssertionList.add(new WsIdScheme(idScheme));
        }

        return new WsIdSchemeWrapper(wsAssertionList);
    }


    public WsAssertionWrapper getAssertionsForSchemeV2(@PathParam("idSchemeId") String idScheme) throws JAXBException {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(EntityManagerService.provideEntityManager());
        query.target().idScheme().eq(idScheme);
        return getAssertions(idScheme, query);
    }


    public WsAssertionWrapper getAssertionForSchemeV2(@PathParam("idSchemeId") String idScheme, @PathParam("assertionId") String assertionId) throws JAXBException {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(EntityManagerService.provideEntityManager());
        query.target().idScheme().eq(idScheme);
        if (assertionId != null) {
            query.assertionId().eq(assertionId);
        }
        return getAssertions(idScheme, query);
    }

    private WsAssertionWrapper getAssertions(@PathParam("idSchemeId") String idScheme, OasisTestAssertionQuery query) {
        List<OasisTestAssertion> assertions = query.getListDistinct();

        List<WsAssertion> wsAssertionList = buildWsAssertions(idScheme, assertions);

        return new WsAssertionWrapper(wsAssertionList);
    }

    private List<WsAssertion> buildWsAssertions(String idScheme, List<OasisTestAssertion> assertions) {
        List<WsAssertion> wsAssertionList = new ArrayList<WsAssertion>();
        for (OasisTestAssertion assertion : assertions) {
            String description = "";
            if (assertion.getPredicate() != null) {
                description = assertion.getPredicate().getContent();
            }
            wsAssertionList.add(new WsAssertion(idScheme, assertion.getAssertionId().toString(), description));
        }
        return wsAssertionList;
    }

    public void openIdSchemeDocumentPage(@QueryParam("idScheme") String idScheme, @QueryParam("page") int page) {
        TestAssertionRequest testAssertionRequest = new TestAssertionRequest(EntityManagerService.provideEntityManager());
        testAssertionRequest.openIdSchemeDocumentPage(idScheme, page);
    }

    @Override
    public WsAssertionWrapper getTestAssertions(String testId) throws JAXBException {
        LOG.info("getTestAssertions");

        AssertionLinkedToTestQuery query = new AssertionLinkedToTestQuery(EntityManagerService.provideEntityManager());
        query.setCachable(false);
        query.linkedEntityKeyword().eq(testId);

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    @Override
    public WsAssertionWrapper getTestStepAssertions(String testStepId) throws JAXBException {
        LOG.info("getTestStepAssertions");

        AssertionLinkedToTestStepQuery query = new AssertionLinkedToTestStepQuery(EntityManagerService.provideEntityManager());
        query.setCachable(false);
        query.linkedEntityKeyword().eq(testStepId);

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    @Override
    public WsAssertionWrapper getRuleAssertions(String ruleId) throws JAXBException {

        AssertionLinkedToRuleQuery query = new AssertionLinkedToRuleQuery(EntityManagerService.provideEntityManager());
        query.setCachable(false);
        query.linkedEntityKeyword().eq(ruleId);

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    private WsAssertionWrapper getWsAssertionWrapper(List<OasisTestAssertion> assertions) {
        List<WsAssertion> wsAssertionList = new ArrayList<WsAssertion>();

        for (OasisTestAssertion assertion : assertions) {
            String assertionId = assertion.getAssertionId();
            String content = assertion.getPredicate().getContent();

            wsAssertionList.add(new WsAssertion(assertion.getTarget().getIdScheme(), assertionId, content));
        }

        return new WsAssertionWrapper(wsAssertionList);
    }

    @Override
    public Response getTestAssertionByIdAndIdSchemeWithoutStyle(String assertionId, String idScheme) throws JAXBException, UnsupportedEncodingException {
        return getTestAssertionByIdAndIdScheme(assertionId, idScheme, "xml");
    }

    @Override
    public WsAssertionWrapper getActorAssertions(String keyword) throws JAXBException {
        AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery();
        query.setCachable(false);
        query.linkedEntityKeyword().eq(keyword);

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    @Override
    public WsAssertionWrapper getTransactionAssertions(String keyword) throws JAXBException {
        AssertionAppliesToTransactionQuery query = new AssertionAppliesToTransactionQuery();
        query.setCachable(false);
        query.linkedEntityKeyword().eq(keyword);

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    @Override
    public WsAssertionWrapper getProfileAssertions(@PathParam("keyword") String keyword) throws JAXBException {
        AssertionAppliesToIntegrationProfileQuery query = new AssertionAppliesToIntegrationProfileQuery();
        query.setCachable(false);
        query.linkedEntityKeyword().eq(keyword);

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    @Override
    public WsAssertionWrapper getAIPOAssertions(@PathParam("keyword") String keyword) throws JAXBException {
        AssertionAppliesToAipoQuery query = new AssertionAppliesToAipoQuery();
        query.setCachable(false);
        query.linkedEntityKeyword().eq(keyword);

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    @Override
    public WsAssertionWrapper getStandardAssertions(@PathParam("id") String id) throws JAXBException {
        AssertionAppliesToStandardQuery query = new AssertionAppliesToStandardQuery();
        query.setCachable(false);
        query.linkedEntityId().eq(Integer.valueOf(id));

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }

    @Override
    public WsAssertionWrapper getAuditMessageAssertions(@PathParam("id") String id) throws JAXBException {
        AssertionAppliesToAuditMessageQuery query = new AssertionAppliesToAuditMessageQuery();
        query.setCachable(false);
        query.linkedEntityId().eq(Integer.valueOf(id));

        List<OasisTestAssertion> assertions = query.assertion().getListDistinct();

        return getWsAssertionWrapper(assertions);
    }
}
