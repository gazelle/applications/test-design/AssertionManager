package net.ihe.gazelle.AssertionManager.assertion.xml.response;

import javax.xml.bind.annotation.XmlElement;

public abstract class JaxbString {

    private String value;

    public JaxbString() {
    }

    public JaxbString(String v) {
        this.value = v;
    }

    public void setValue(String valueIn) {
        this.value = valueIn;
    }

    @XmlElement
    public String getValue() {
        return value;
    }

}