package net.ihe.gazelle.AssertionManager.assertion.link.appliesTo;

import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToActor;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToActorQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tf.ws.data.ActorWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("appliesToActorManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AppliesToActorManagerLocal")
public class AppliesToActorManager extends AssertionLinkManager implements AppliesToActorManagerLocal, Serializable {


    private static final long serialVersionUID = -4181671477580363694L;
    private ActorWrapper selectedActor = new ActorWrapper();

    private static final Logger LOG = LoggerFactory.getLogger(AppliesToActorManager.class);
    private GazelleListDataModel<ActorWrapper> actors;

    public List<String> getDomainsAvailable() {
        return getTmWsClient().getDomainsAvailable();
    }


    public List<String> getIntegrationProfilesNames() {
        return getTmWsClient().getIntegrationProfilesNames(getSelectedDomain());
    }

    public GazelleListDataModel<ActorWrapper> getActors() {
        if (actors == null) {
            actors = getTmWsClient().getActors(getSelectedDomain(), getSelectedIntegrationProfileName());
        }
        return actors;
    }

    public String getSelectedActorDescription() {
        return selectedActor.getDescription();
    }

    public void setSelectedActor(ActorWrapper actor) {
        selectedActor = getFullActor(actor);
        LOG.info("setSelectedActor " + selectedActor);
    }

    /*Get full actor with description*/
    private ActorWrapper getFullActor(ActorWrapper actor) {
        return getTmWsClient().getFullActor(actor.getId());
    }

    public ActorWrapper getSelectedActor() {
        return selectedActor;
    }

    public boolean isActorSelected() {
        return getSelectedActor().getKeyword() != null;
    }

    public List<AssertionAppliesToActor> assertionsOfSelectedActor() {
        List<AssertionAppliesToActor> listDistinct = new ArrayList<AssertionAppliesToActor>();

        if (getSelectedActor() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery(entityManager);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedActor().getKeyword()));
            listDistinct = query.getListDistinct();
        }
        return listDistinct;
    }

    public void unlinkSelectedActorFrom(int assertionId) {
        if (getSelectedActor() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedActor().getKeyword()));

            AssertionAppliesToActor uniqueResult = query.getUniqueResult();

            entityManager.remove(uniqueResult);
            entityManager.flush();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            updateCoverage(entityManager, assertion.getId());
        }
    }


    public void linkSelectedActorTo(int assertionId) {
        if (getSelectedActor() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            ActorWrapper actor = getSelectedActor();
            AssertionAppliesToActor link = new AssertionAppliesToActor(actor.getKeyword(), actor.getName(), actor.getId(), actor.getBaseUrl(), actor.getPermanentLink(), assertion);
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public boolean isActorCovered(String actorKeyword) {
        boolean result = false;
        AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery();
        query.linkedEntityKeyword().eq(actorKeyword);
        List<AssertionAppliesToActor> listDistinct = query.getListDistinct();
        if ((listDistinct != null) && (listDistinct.size() != 0)) {
            result = true;
        }
        return result;
    }

    public boolean isSelectedActorLinkedTo(int assertionId) {
        boolean result = false;
        AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery();
        query.assertion().id().eq(assertionId);
        if (getSelectedActor() != null) {
            query.linkedEntityKeyword().eq(getSelectedActor().getKeyword());
        }
        AssertionAppliesToActor uniqueResult = query.getUniqueResult();
        if (uniqueResult != null) {
            result = true;
        }
        return result;
    }

    @Override
    public void selectedIntegrationProfileUpdate() {
        selectedDomainUpdate();
    }

    @Override
    public void selectedDomainUpdate() {
        selectedActor = new ActorWrapper();
        actors = null;
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedActorLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedActorFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedActorTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isActorSelected();
    }

    @Override
    public void selectedDomainUpdateCallback() {
        // TODO Auto-generated method stub

    }
}