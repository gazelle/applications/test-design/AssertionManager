package net.ihe.gazelle.AssertionManager.assertion;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

@Name("oasisTargetConverter")
@BypassInterceptors
@Converter(forClass = OasisTarget.class)
public class OasisTargetConverter implements javax.faces.convert.Converter {

    private EntityManager em = EntityManagerService.provideEntityManager();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return em.find(OasisTarget.class, Integer.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((OasisTarget) value).getId().toString();
    }
}