package net.ihe.gazelle.AssertionManager.scope;

import net.ihe.gazelle.AssertionManager.assertion.OasisTestAssertionFilter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionScope;
import net.ihe.gazelle.oasis.testassertion.AssertionScopeQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;

@Name("assertionScopeManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AssertionScopeManagerLocal")
public class AssertionScopeManager implements AssertionScopeManagerLocal, Serializable {

    /**
     */
    private static final long serialVersionUID = 4210632994477905383L;
    private AssertionScopeFilter filter;
    private FilterDataModel<AssertionScope> scopes;

    private AssertionScope selectedScope;

    private String selectedScopeForFilter = "";

    private OasisTestAssertionFilter assertionsFilter;
    private FilterDataModel<OasisTestAssertion> scopeAssertions;

    private OasisTestAssertionFilter possibleAssertionsToAddFilter;
    private FilterDataModel<OasisTestAssertion> possibleAssertionsToAdd;

    private OasisTestAssertionFilter assertionsLinkedFilter;
    private FilterDataModel<OasisTestAssertion> assertionsLinked;

    private List<OasisTestAssertion> selectedAssertionFromTable;

    private Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
    private Map<Integer, Boolean> checkedAssertionsToRemove = new HashMap<Integer, Boolean>();
    private Map<Integer, Boolean> checkedScopeLinkToRemove = new HashMap<Integer, Boolean>();

    public String getSelectedScopeForFilter() {
        return selectedScopeForFilter;
    }

    public void resetSelectedScopeForFilter() {
        selectedScopeForFilter = "";
    }

    public void setSelectedScopeForFilter(String selectedScopeForFilterIn) {
        this.selectedScopeForFilter = selectedScopeForFilterIn;
    }

    public List<ScopeCoverage> getScopeCoverages() {

        AssertionScopeQuery assertionQuery = new AssertionScopeQuery();
        if (selectedScopeForFilter != null && !"".equals(selectedScopeForFilter)) {
            assertionQuery.keyword().eq(selectedScopeForFilter);
        }
        List<AssertionScope> scopesTmp = assertionQuery.getListDistinct();

        return ScopeCoverage.compute(scopesTmp);
    }



    @SuppressWarnings("unchecked")
    public AssertionScopeFilter getFilter() {
        if (filter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            filter = new AssertionScopeFilter(requestParameterMap);
        }
        return filter;
    }

    public FilterDataModel<AssertionScope> getScopes() {
        if (scopes == null) {
            scopes = new FilterDataModel<AssertionScope>(getFilter()) {
                /**
                 */
                private static final long serialVersionUID = -2952236327275105177L;

                @Override
                protected Object getId(AssertionScope t) {
                    return t.getId();
                }
            };
        }
        return scopes;
    }

    @SuppressWarnings("unchecked")
    public OasisTestAssertionFilter getAssertionsFilter() {
        if (assertionsFilter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            assertionsFilter = new OasisTestAssertionFilter(requestParameterMap);
        }
        return assertionsFilter;
    }

    public FilterDataModel<OasisTestAssertion> getScopeAssertions() {
        if (scopeAssertions == null) {
            scopeAssertions = new FilterDataModel<OasisTestAssertion>(getAssertionsFilter()) {
                @Override
                protected Object getId(OasisTestAssertion t) {
                    return t.getId();
                }
            };
        }
        return scopeAssertions;
    }

    @SuppressWarnings("unchecked")
    public OasisTestAssertionFilter getPossibleAssertionsToAddFilter() {
        if (possibleAssertionsToAddFilter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            possibleAssertionsToAddFilter = new OasisTestAssertionFilter(requestParameterMap);
        }
        return possibleAssertionsToAddFilter;
    }

    public FilterDataModel<OasisTestAssertion> getPossibleAssertionsToAdd() {
        if (possibleAssertionsToAdd == null) {
            possibleAssertionsToAdd = new FilterDataModel<OasisTestAssertion>(getPossibleAssertionsToAddFilter()) {
                @Override
                protected Object getId(OasisTestAssertion t) {
                    return t.getId();
                }
            };
        }

        return possibleAssertionsToAdd;
    }

    public OasisTestAssertionFilter getAssertionsLinkedFilter() {
        if (assertionsLinkedFilter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            assertionsLinkedFilter = new OasisTestAssertionFilter(requestParameterMap);
        }
        return assertionsLinkedFilter;
    }

    public FilterDataModel<OasisTestAssertion> getAssertionsLinked() {
        if (assertionsLinked == null) {
            assertionsLinked = new FilterDataModel<OasisTestAssertion>(getAssertionsLinkedFilter()) {
                @Override
                protected Object getId(OasisTestAssertion t) {
                    return t.getId();
                }
            };
        }
        return assertionsLinked;
    }

    public String getFilteredIdScheme() {
        String result = "";
        if (!getAssertionsFilter().getFilterValues().isEmpty()) {
            result = getAssertionsFilter().getFilterValues().get("idScheme").toString();
        }
        return result;
    }

    public String newScope() {
        selectedScope = new AssertionScope();
        return getEditPage() + "?new=1";
    }

    @Deprecated
    public String edit(int scopeId) {
        selectedScope = new AssertionScope();
        return getEditPage() + "?id=" + scopeId;
    }

    @Deprecated
    public String show(int id) {
        return getEditPage() + "?show=" + id;
    }

    private String getEditPage() {
        return "/scopes/show.xhtml";
    }

    public void delete() {
        if (selectedScope != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            selectedScope = em.find(AssertionScope.class, selectedScope.getId());
            em.remove(selectedScope);
            em.flush();
            filter.modified();
        }
    }

    private Integer save() {
        Integer savedId = null;
        if (selectedScope == null) {
            FacesMessages.instance().add("Scope value is null cannot go further");
        } else {
            EntityManager em = EntityManagerService.provideEntityManager();
            savedId = em.merge(selectedScope).getId();
            em.flush();

            FacesMessages.instance().add("Scope " + selectedScope.getKeyword() + " was successfully saved");
        }
        return savedId;
    }

    public String create() {
        return "/scopes/edit.xhtml?id=" + save().toString();
    }

    public String saveScope() {
        save();
        return "/scopes/index.xhtml";
    }

    public AssertionScope getSelectedScope() {
        if (selectedScope == null) {
            selectedScope = new AssertionScope();
            FacesContext fc = FacesContext.getCurrentInstance();
            String scopeId = fc.getExternalContext().getRequestParameterMap().get("id");

            if (scopeId != null) {
                EntityManager em = EntityManagerService.provideEntityManager();
                selectedScope = em.find(AssertionScope.class, Integer.valueOf(scopeId));
                getFilter().setExcludeScopeAndLinkedScopes(selectedScope);
                getPossibleAssertionsToAddFilter().setExcludeAssertionsFromScope(selectedScope);
                getAssertionsLinkedFilter().setScopeRestriction(selectedScope);
            } else {

                String newScope = fc.getExternalContext().getRequestParameterMap().get("new");
                if ("1".equals(newScope)) {
                    selectedScope = new AssertionScope();
                } else {

                    String showScope = fc.getExternalContext().getRequestParameterMap().get("show");
                    if (showScope != null) {
                        EntityManager em = EntityManagerService.provideEntityManager();
                        selectedScope = em.find(AssertionScope.class, Integer.valueOf(showScope));
                        getAssertionsFilter().setScopeRestriction(selectedScope);
                        getAssertionsFilter().modified();
                    }
                }
            }
        }
        return selectedScope;
    }

    public void setSelectedScope(AssertionScope selectedScopeIn) {
        this.selectedScope = selectedScopeIn;
    }

    public List<String> getScopeNames() {
        AssertionScopeQuery query = new AssertionScopeQuery(EntityManagerService.provideEntityManager());
        List<String> listDistinct = query.keyword().getListDistinct();
        Collections.sort(listDistinct);
        return listDistinct;
    }

    public List<AssertionScope> getAssertionsScopesReferencing(AssertionScope scope) {
        return AssertionScope.getAssertionsScopesReferencing(scope, EntityManagerService.provideEntityManager());
    }

    public int getAssertionsScopesReferencingSize(AssertionScope scope) {
        return AssertionScope.getAssertionsScopesReferencingSize(scope, EntityManagerService.provideEntityManager());
    }

    public List<AssertionScope> getAssertionsScopesReferencingSelectedScope() {
        return AssertionScope
                .getAssertionsScopesReferencing(selectedScope, EntityManagerService.provideEntityManager());
    }

    public List<OasisTestAssertion> getSelectedAssertionFromTable() {
        return selectedAssertionFromTable;
    }

    public void setSelectedAssertionFromTable(List<OasisTestAssertion> selectedAssertionFromTableIn) {
        this.selectedAssertionFromTable = selectedAssertionFromTableIn;
    }

    public Map<Integer, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<Integer, Boolean> checkedIn) {
        this.checked = checkedIn;
    }

    public void addToScope() {

        EntityManager em = EntityManagerService.provideEntityManager();

        List<OasisTestAssertion> checkedItems = getCheckedAssertions(em, getChecked().entrySet());

        getChecked().clear();

        selectedScope.addAssertions(checkedItems);
        possibleAssertionsToAddFilter.modified();

        selectedScope = em.merge(selectedScope);

        FacesMessages.instance().add(checkedItems.size() + " assertions successfully added");
    }

    public Map<Integer, Boolean> getCheckedAssertionsToRemove() {
        return checkedAssertionsToRemove;
    }

    public void setCheckedAssertionsToRemove(Map<Integer, Boolean> checkedAssertionsToRemoveIn) {
        this.checkedAssertionsToRemove = checkedAssertionsToRemoveIn;
    }

    public void removeAssertionFromScope() {


        EntityManager em = EntityManagerService.provideEntityManager();
        List<OasisTestAssertion> checkedItems = getCheckedAssertions(em, getCheckedAssertionsToRemove().entrySet());

        getCheckedAssertionsToRemove().clear();

        selectedScope.removeAssertion(checkedItems);
        possibleAssertionsToAddFilter.modified();

        selectedScope = em.merge(selectedScope);

        FacesMessages.instance().add(checkedItems.size() + " assertions successfully removed");
    }

    private List<OasisTestAssertion> getCheckedAssertions(EntityManager em, Set<Map.Entry<Integer, Boolean>> entrySet) {
        List<OasisTestAssertion> checkedItems = new ArrayList<OasisTestAssertion>();
        for (Map.Entry<Integer, Boolean> entry : entrySet) {
            if (isEntrySelected(entry)) {
                checkedItems.add(em.find(OasisTestAssertion.class, entry.getKey()));
            }
        }
        return checkedItems;
    }

    private Boolean isEntrySelected(Map.Entry<Integer, Boolean> entry) {
        return entry.getValue();
    }

    public Map<Integer, Boolean> getCheckedScopeLinkToRemove() {
        return checkedScopeLinkToRemove;
    }

    public void setCheckedScopeLinkToRemove(Map<Integer, Boolean> checkedScopeLinkToRemoveIn) {
        this.checkedScopeLinkToRemove = checkedScopeLinkToRemoveIn;
    }

    public void linkScopeToThisScope() {
        List<AssertionScope> checkedItems = new ArrayList<AssertionScope>();

        EntityManager em = EntityManagerService.provideEntityManager();
        for (Map.Entry<Integer, Boolean> entry : getCheckedScopeLinkToRemove().entrySet()) {
            if (isEntrySelected(entry)) {
                checkedItems.add(em.find(AssertionScope.class, entry.getKey()));
            }
        }

        getCheckedScopeLinkToRemove().clear();

        selectedScope.addLinkedScopes(checkedItems);
        possibleAssertionsToAddFilter.modified();
        selectedScope = em.merge(selectedScope);

        FacesMessages.instance().add(checkedItems.size() + " scope successfully linked");
    }

    public void unlinkScope(AssertionScope scope) {

        selectedScope.removeScopeLink(scope);

        EntityManager em = EntityManagerService.provideEntityManager();
        selectedScope = em.merge(selectedScope);

        FacesMessages.instance().add(scope.getKeyword() + " successfully unlinked");
    }
}
