package net.ihe.gazelle.AssertionManager.utils;

import net.ihe.gazelle.common.filter.Filter;

import java.nio.charset.StandardCharsets;

public final class UrlHelper {

    private UrlHelper() {
    }

    public static String concatURLtoPath(String assertionManagerRestUrl, String assertionManagerRestPathToAssertion) {
        String restUrlStripped = assertionManagerRestUrl;
        String restPathToAssertionStripped = assertionManagerRestPathToAssertion;

        if (assertionManagerRestUrl.endsWith("/")) {
            restUrlStripped = assertionManagerRestUrl.substring(0, assertionManagerRestUrl.length() - 1);
        }

        if (assertionManagerRestPathToAssertion.startsWith("/")) {
            restPathToAssertionStripped = assertionManagerRestPathToAssertion.substring(1,
                    assertionManagerRestPathToAssertion.length());
        }
        return restUrlStripped + "/" + restPathToAssertionStripped;
    }

    public static void fixEncoding(Filter filter) {
        //fixing encoding
        // Convert idScheme String from ISO_8859_1 to UTF_8 if it is not null
        Object notEncoded = filter.getFilterValues().get("idScheme");
        if (notEncoded != null) {
            Object encodedObj = new String(notEncoded.toString().getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
            // Replace the old string by the converted string in the filter
            filter.getFilterValues().put("idScheme", encodedObj);
        }
    }
}
