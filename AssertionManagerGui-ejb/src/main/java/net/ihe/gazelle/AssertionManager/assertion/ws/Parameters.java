package net.ihe.gazelle.AssertionManager.assertion.ws;

import org.apache.commons.lang.StringUtils;

import javax.ws.rs.core.Response;

public final class Parameters {

    private static final String ALL_SET = "All set";
    private static final String NO_TEST_ASSERTION_ID_AND_NO_ID_SCHEME = "No Test Assertion Id and no id scheme";
    private static final String ASSERTION_ID_ONLY = "Test Assertion Id only";
    private static final String ID_SCHEME_ONLY = "IdScheme only";
    private static final String NULL_INJECTION = "Null parameter injection";

    private Parameters() {
    }

    public static Response checkParameters(String testAssertionId, String idScheme) {
        Response response = null;
        String paramsState = checkParamsState(testAssertionId, idScheme);
        if (!areParametersOk(paramsState)) {
            if (paramsState.equals(ASSERTION_ID_ONLY)) {
                response = Response
                        .status(Response.Status.PRECONDITION_FAILED)
                        .header("Warning",
                                "Warning: 003 AssertionManager \"NAV: please provide an idScheme parameter\"").build();
            } else if (paramsState.equals(NO_TEST_ASSERTION_ID_AND_NO_ID_SCHEME) || paramsState.equals(ID_SCHEME_ONLY)) {
                response = Response
                        .status(Response.Status.PRECONDITION_FAILED)
                        .header("Warning",
                                "Warning: 004 AssertionManager \"NAV: please provide an idScheme parameter or an id and an idScheme parameter\"")
                        .build();
            } else {
                response = Response.status(Response.Status.BAD_REQUEST)
                        .header("Warning", "Warning: 005 AssertionManager \"NAV: Tried to Attack the webService\"")
                        .build();
            }
        }
        return response;
    }

    private static String checkParamsState(String testAssertionId, String idScheme) {
        String paramsState = null;

        paramsState = checkInputParameters(testAssertionId, idScheme);
        if (!StringUtils.isEmpty(testAssertionId) && testAssertionId.indexOf('\0') != -1) {
            paramsState = NULL_INJECTION;
        }
        if (!StringUtils.isEmpty(idScheme) && idScheme.indexOf('\0') != -1) {
            paramsState = NULL_INJECTION;
        }
        return paramsState;
    }

    private static String checkInputParameters(String testAssertionId, String idScheme) {
        String paramsState;
        if (StringUtils.isEmpty(testAssertionId) && (StringUtils.isEmpty(idScheme))) {
            paramsState = NO_TEST_ASSERTION_ID_AND_NO_ID_SCHEME;
        } else if (StringUtils.isNotBlank(idScheme) && StringUtils.isEmpty(testAssertionId)) {
            paramsState = ID_SCHEME_ONLY;
        } else if (StringUtils.isEmpty(idScheme) && StringUtils.isNotBlank(testAssertionId)) {
            paramsState = ASSERTION_ID_ONLY;
        } else {
            paramsState = ALL_SET;
        }
        return paramsState;
    }

    private static boolean areParametersOk(String paramsState) {

        return paramsState.equals(ALL_SET) || paramsState.equals(ID_SCHEME_ONLY);
    }
}
