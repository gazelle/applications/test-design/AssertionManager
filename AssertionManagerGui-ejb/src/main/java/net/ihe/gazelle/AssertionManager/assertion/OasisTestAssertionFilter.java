package net.ihe.gazelle.AssertionManager.assertion;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.oasis.testassertion.*;

import javax.persistence.EntityManager;
import java.util.*;

public class OasisTestAssertionFilter extends Filter<OasisTestAssertion> implements QueryModifier<OasisTestAssertion> {


    private static final long serialVersionUID = -7984103972774534794L;
    public static final String COVERED_BY = "coveredBy";
    public static final String NOT_COVERED_BY = "notCoveredBy";
    public static final String APPLIED_TO = "appliedTo";
    public static final String NOT_APPLIED_TO = "notAppliedTo";
    private static Map<String, Object> notCoveredCheckboxesValue;
    private static Map<String, Object> coveredCheckboxesValue;
    private static Map<String, Object> notAppliedCheckboxesValue;
    private static Map<String, Object> appliedCheckboxesValue;

    static {
        notCoveredCheckboxesValue = new LinkedHashMap<String, Object>();
        notCoveredCheckboxesValue.put(CheckBoxesFilter.ANY.getFriendlyName(), CheckBoxesFilter.ANY);
        notCoveredCheckboxesValue.put(CheckBoxesFilter.RULES.getFriendlyName(), CheckBoxesFilter.RULES);
        notCoveredCheckboxesValue.put(CheckBoxesFilter.TEST.getFriendlyName(), CheckBoxesFilter.TEST);
        notCoveredCheckboxesValue.put(CheckBoxesFilter.DEPRECATED_TEST.getFriendlyName(), CheckBoxesFilter.DEPRECATED_TEST);
        notCoveredCheckboxesValue.put(CheckBoxesFilter.MBV.getFriendlyName(), CheckBoxesFilter.MBV);
    }

    static {
        coveredCheckboxesValue = new LinkedHashMap<String, Object>(notCoveredCheckboxesValue);
    }

    static {
        notAppliedCheckboxesValue = new LinkedHashMap<String, Object>();
        notAppliedCheckboxesValue.put(CheckBoxesFilter.ANY.getFriendlyName(), CheckBoxesFilter.ANY);
        notAppliedCheckboxesValue.put(CheckBoxesFilter.ACTOR.getFriendlyName(), CheckBoxesFilter.ACTOR);
        notAppliedCheckboxesValue.put(CheckBoxesFilter.TRANSACTION.getFriendlyName(), CheckBoxesFilter.TRANSACTION);
        notAppliedCheckboxesValue.put(CheckBoxesFilter.AIPO.getFriendlyName(), CheckBoxesFilter.AIPO);
        notAppliedCheckboxesValue.put(CheckBoxesFilter.STANDARD.getFriendlyName(), CheckBoxesFilter.STANDARD);
        notAppliedCheckboxesValue
                .put(CheckBoxesFilter.INTEGRATION_PROFILE.getFriendlyName(), CheckBoxesFilter.INTEGRATION_PROFILE);
    }

    static {
        appliedCheckboxesValue = new LinkedHashMap<String, Object>(notAppliedCheckboxesValue);
    }

    private AssertionScope selectedScope;
    private AssertionScope excludeAssertionsFromScope;
    private String[] notCoveredCheckboxes;
    private String[] coveredCheckboxes;
    private String[] notAppliedCheckboxes;
    private String[] appliedCheckboxes;
    private OasisRefSourceItem selectedDocumentForFilter;

    private void resetCheckBoxes() {
        notCoveredCheckboxes = null;
        coveredCheckboxes = null;
        notAppliedCheckboxes = null;
        appliedCheckboxes = null;
    }

    public OasisTestAssertionFilter(Map<String, String> requestParameterMap) {
        super(getHQLCriterions(), requestParameterMap);
        updateCheckboxesFromUrl(requestParameterMap);
        queryModifiers.add(this);
    }

    public void addQueryModifier(QueryModifier<OasisTestAssertion> modifier) {
        queryModifiers.add(modifier);
    }

    private static HQLCriterionsForFilter<OasisTestAssertion> getHQLCriterions() {

        OasisTestAssertionQuery query = new OasisTestAssertionQuery(EntityManagerService.provideEntityManager());
        HQLCriterionsForFilter<OasisTestAssertion> result = query.getHQLCriterionsForFilter();

        result.addPath("idScheme", query.target().idScheme());
        result.addPath("idSchemeLinked", query.target().idScheme());
        result.addPath("assertionId", query.assertionId());
        result.addPath("prescriptionLevel", query.prescription().level());
        result.addPath("assertionIdLinked", query.assertionId());
        result.addPath("scopeField", query.scopes().keyword());
        result.addPath("mbvService", query.mbv().mbvService().keyword());
        result.addPath("status", query.status());
        result.addPath("statusLinked", query.status());
        result.addPath("actor", query.tfActors().linkedEntityKeyword());
        result.addPath("aipo", query.tfAIPOs().linkedEntityName());
        result.addPath("auditMessage", query.tfAuditMessages().linkedEntityKeyword());
        result.addPath("integrationProfile", query.tfIntegrationProfiles().linkedEntityKeyword());
        result.addPath("standard", query.tfStandards().linkedEntityKeyword());
        result.addPath("transaction", query.tfTransactions().linkedEntityKeyword());
        result.addPath("rule", query.tfRules().linkedEntityName());
        result.addPath("testable", query.testable());
        result.addPath("test", query.tests().linkedEntityName());

        return result;
    }

    public void resetSelectedScopeForFilter() {
        setSelectedScope(null);
    }

    public String getSelectedScopeForFilter() {
        if (getSelectedScope() == null) {
            return "";
        } else {
            return getSelectedScope().getKeyword();
        }
    }

    public void setSelectedScopeForFilter(String selectedScopeForFilter) {
        AssertionScopeQuery query = new AssertionScopeQuery(EntityManagerService.provideEntityManager());
        query.keyword().eq(selectedScopeForFilter);

        setScopeRestriction(query.getUniqueResult());
        modified();
    }

    public AssertionScope getSelectedScope() {
        return selectedScope;
    }

    public void setSelectedScope(AssertionScope selectedScopeIn) {
        this.selectedScope = selectedScopeIn;
    }

    @Override
    public EntityManager getEntityManager() {
        return EntityManagerService.provideEntityManager();
    }

    public void setScopeRestriction(AssertionScope selectedScopeIn) {
        this.selectedScope = selectedScopeIn;
        modified();
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<OasisTestAssertion> queryBuilder, Map<String, Object> filterValuesApplied) {

        filterByScope(queryBuilder);
        excludeByScope(queryBuilder);
        filterByDocument(queryBuilder);
        filterCoveredAssertions(queryBuilder);
        filterNotCoveredAssertions(queryBuilder);
        filterAppliedToAssertions(queryBuilder);
        filterNotAppliedToAssertions(queryBuilder);
    }

    private void filterByScope(HQLQueryBuilder<OasisTestAssertion> queryBuilder) {
        if (getSelectedScope() != null) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(queryBuilder);

            List<Integer> scopesIds = new ArrayList<Integer>();
            AssertionScope.getLinkedScopeIds(getSelectedScope(), scopesIds);

            HQLRestriction scopeRestriction = query.scopes().id().inRestriction(scopesIds);
            queryBuilder.addRestriction(scopeRestriction);
        }
    }

    private void excludeByScope(HQLQueryBuilder<OasisTestAssertion> queryBuilder) {
        if (excludeAssertionsFromScope != null) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(queryBuilder);
            query.nin(excludeAssertionsFromScope.getAssertions());
        }
    }

    private void filterByDocument(HQLQueryBuilder<OasisTestAssertion> queryBuilder) {
        if (selectedDocumentForFilter != null) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(queryBuilder);

            HQLRestriction documentRestriction = query.normativeSource().refSourceItems().id()
                    .eqRestriction(selectedDocumentForFilter.getId());
            queryBuilder.addRestriction(documentRestriction);
        }
    }

    private void filterNotAppliedToAssertions(HQLQueryBuilder<OasisTestAssertion> queryBuilder) {
        if (notAppliedCheckboxes != null) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(queryBuilder);
            for (String item : notAppliedCheckboxes) {
                if (item.equals(CheckBoxesFilter.ANY.toString())) {
                    queryBuilder.addRestriction(HQLRestrictions
                            .and(query.tfActors().isEmptyRestriction(), query.tfTransactions().isEmptyRestriction(),
                                    query.tfAIPOs().isEmptyRestriction(), query.tfStandards().isEmptyRestriction(),
                                    query.tfIntegrationProfiles().isEmptyRestriction()));
                    break;
                } else if (item.equals(CheckBoxesFilter.ACTOR.toString())) {
                    queryBuilder.addRestriction(query.tfActors().isEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.TRANSACTION.toString())) {
                    queryBuilder.addRestriction(query.tfTransactions().isEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.AIPO.toString())) {
                    queryBuilder.addRestriction(query.tfAIPOs().isEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.STANDARD.toString())) {
                    queryBuilder.addRestriction(query.tfStandards().isEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.INTEGRATION_PROFILE.toString())) {
                    queryBuilder.addRestriction(query.tfIntegrationProfiles().isEmptyRestriction());
                }
            }
        }
    }

    private void filterAppliedToAssertions(HQLQueryBuilder<OasisTestAssertion> queryBuilder) {
        if (appliedCheckboxes != null) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(queryBuilder);
            for (String item : appliedCheckboxes) {
                if (item.equals(CheckBoxesFilter.ANY.toString())) {
                    queryBuilder.addRestriction(HQLRestrictions.or(query.tfActors().isNotEmptyRestriction(),
                            query.tfTransactions().isNotEmptyRestriction(), query.tfAIPOs().isNotEmptyRestriction(),
                            query.tfStandards().isNotEmptyRestriction(),
                            query.tfIntegrationProfiles().isNotEmptyRestriction()));
                    break;
                } else if (item.equals(CheckBoxesFilter.ACTOR.toString())) {
                    queryBuilder.addRestriction(query.tfActors().isNotEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.TRANSACTION.toString())) {
                    queryBuilder.addRestriction(query.tfTransactions().isNotEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.AIPO.toString())) {
                    queryBuilder.addRestriction(query.tfAIPOs().isNotEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.STANDARD.toString())) {
                    queryBuilder.addRestriction(query.tfStandards().isNotEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.INTEGRATION_PROFILE.toString())) {
                    queryBuilder.addRestriction(query.tfIntegrationProfiles().isNotEmptyRestriction());
                }
            }
        }
    }

    private void filterNotCoveredAssertions(HQLQueryBuilder<OasisTestAssertion> queryBuilder) {
        if (notCoveredCheckboxes != null) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(queryBuilder);
            query.getQueryBuilder().forceOuterJoins();
            for (String item : notCoveredCheckboxes) {
                if (item.equals(CheckBoxesFilter.ANY.toString())) {
                    queryBuilder.addRestriction(
                            HQLRestrictions.and(
                                    query.tests().isEmptyRestriction(),
                                    query.testSteps().isEmptyRestriction(),
                                    query.tfRules().isEmptyRestriction(),
                                    query.mbv().isEmptyRestriction()
                            ));
                    break;
                } else if (item.equals(CheckBoxesFilter.TEST.toString())) {

                    queryBuilder.addRestriction(
                            HQLRestrictions.and(
                                    HQLRestrictions.or(
                                            query.tests().isEmptyRestriction(),
                                            query.tests().deprecated().eqRestriction(true)),
                                    HQLRestrictions.or(
                                            query.testSteps().isEmptyRestriction(),
                                            query.testSteps().deprecated().eqRestriction(true))
                            ));

                } else if (item.equals(CheckBoxesFilter.DEPRECATED_TEST.toString())) {
                    queryBuilder.addRestriction(
                            HQLRestrictions.and(
                                    HQLRestrictions.or(
                                            query.tests().isEmptyRestriction(),
                                            query.tests().deprecated().eqRestriction(false)),
                                    HQLRestrictions.or(
                                            query.testSteps().isEmptyRestriction(),
                                            query.testSteps().deprecated().eqRestriction(false))
                            ));
                } else if (item.equals(CheckBoxesFilter.RULES.toString())) {
                    queryBuilder.addRestriction(query.tfRules().isEmptyRestriction());
                } else if (item.equals(CheckBoxesFilter.MBV.toString())) {
                    queryBuilder.addRestriction(query.mbv().isEmptyRestriction());
                }
            }
        }
    }

    private void filterCoveredAssertions(HQLQueryBuilder<OasisTestAssertion> queryBuilder) {
        if (coveredCheckboxes != null) {
            OasisTestAssertionQuery query = new OasisTestAssertionQuery(queryBuilder);
            query.getQueryBuilder().forceOuterJoins();
            for (String item : coveredCheckboxes) {
                if (item.equals(CheckBoxesFilter.ANY.toString())) {
                    queryBuilder.addRestriction(HQLRestrictions.or(
                            query.tests().deprecated().eqRestriction(false),
                            query.testSteps().deprecated().eqRestriction(false),
                            query.tfRules().deprecated().eqRestriction(false),
                            query.mbv().isNotEmptyRestriction()
                    ));
                    break;
                } else if (item.equals(CheckBoxesFilter.TEST.toString())) {
                    queryBuilder.addRestriction(
                            HQLRestrictions.or(
                                    query.tests().deprecated().eqRestriction(false),
                                    query.testSteps().deprecated().eqRestriction(false)
                            ));
                } else if (item.equals(CheckBoxesFilter.DEPRECATED_TEST.toString())) {
                    queryBuilder.addRestriction(HQLRestrictions.or(
                            query.tests().deprecated().eqRestriction(true),
                            query.testSteps().deprecated().eqRestriction(true)
                    ));
                } else if (item.equals(CheckBoxesFilter.RULES.toString())) {
                    queryBuilder.addRestriction(query.tfRules().deprecated().eqRestriction(false));
                } else if (item.equals(CheckBoxesFilter.MBV.toString())) {
                    queryBuilder.addRestriction(query.mbv().isNotEmptyRestriction());
                }
            }
        }
    }

    public Map<String, Object> getNotCoveredCheckboxesValue() {
        return notCoveredCheckboxesValue;
    }

    public String getNotCoveredCheckboxesInString() {
        return Arrays.toString(notCoveredCheckboxes);
    }

    public String[] getNotCoveredCheckboxes() {
        if (notCoveredCheckboxes != null) {
            return notCoveredCheckboxes.clone();
        }
        return null;
    }

    public void setNotCoveredCheckboxes(String[] notCoveredCheckboxesIn) {
        this.notCoveredCheckboxes = notCoveredCheckboxesIn.clone();
        modified();
    }

    public Map<String, Object> getCoveredCheckboxesValue() {
        return coveredCheckboxesValue;
    }

    public String getCoveredCheckboxesInString() {
        return Arrays.toString(coveredCheckboxes);
    }

    public String[] getCoveredCheckboxes() {
        if (coveredCheckboxes != null) {
            return coveredCheckboxes.clone();
        }
        return null;
    }

    public void setCoveredCheckboxes(String[] coveredCheckboxesIn) {
        this.coveredCheckboxes = coveredCheckboxesIn.clone();
        modified();
    }

    public Map<String, Object> getNotAppliedCheckboxesValue() {
        return notAppliedCheckboxesValue;
    }

    public Map<String, Object> getAppliedCheckboxesValue() {
        return appliedCheckboxesValue;
    }

    public String[] getNotAppliedCheckboxes() {
        if (notAppliedCheckboxes != null) {
            return notAppliedCheckboxes.clone();
        }
        return null;
    }

    public void setNotAppliedCheckboxes(String[] notAppliedCheckboxesIn) {
        this.notAppliedCheckboxes = notAppliedCheckboxesIn.clone();
    }

    public String[] getAppliedCheckboxes() {
        if (appliedCheckboxes != null) {
            return appliedCheckboxes.clone();
        }
        return null;
    }

    public void setAppliedCheckboxes(String[] appliedCheckboxesIn) {
        this.appliedCheckboxes = appliedCheckboxesIn.clone();
    }

    public String getFilterUrlParameters() {
        String urlParameters = getUrlParameters();
        if (getSelectedScope() != null) {
            if (urlParameters.length() != 0) {
                urlParameters += "&";
            }
            urlParameters += "scope=" + getSelectedScope().getKeyword();
        }
        urlParameters += addCheckBoxToString(coveredCheckboxes, COVERED_BY);
        urlParameters += addCheckBoxToString(notCoveredCheckboxes, NOT_COVERED_BY);
        urlParameters += addCheckBoxToString(appliedCheckboxes, APPLIED_TO);
        urlParameters += addCheckBoxToString(notAppliedCheckboxes, NOT_APPLIED_TO);
        if (selectedDocumentForFilter != null) {
            urlParameters += "&document=" + selectedDocumentForFilter.getId();
        }
        return urlParameters;
    }

    /*Avoid error in jsf*/
    public void setFilterUrlParameters(String urlParameters) {
    }

    private String addCheckBoxToString(String[] checkBoxes, String name) {
        String checks = "";
        if (checkBoxes != null && checkBoxes.length != 0) {
            StringBuffer buffer = new StringBuffer();
            buffer.append("&");
            buffer.append(name);
            buffer.append("=");
            for (String checkBox : checkBoxes) {
                buffer.append(checkBox);
                buffer.append(",");
            }
            checks = buffer.substring(0, buffer.length() - 1);
        }
        return checks;
    }

    private void updateCheckboxesFromUrl(Map<String, String> requestParameterMap) {
        Set<Map.Entry<String, String>> entrySet = requestParameterMap.entrySet();
        String[] possibleV = {COVERED_BY, NOT_COVERED_BY, APPLIED_TO, NOT_APPLIED_TO};
        ArrayList<String> possibleValues = new ArrayList<String>(Arrays.asList(possibleV));

        for (Map.Entry<String, String> entry : entrySet) {
            String stringKey = entry.getKey();
            if (possibleValues.contains(stringKey)) {
                String stringValue = entry.getValue();
                if (!"".equals(stringValue)) {
                    List<String> checks = Arrays.asList(stringValue.split(","));
                    if (stringKey.equals(COVERED_BY)) {
                        coveredCheckboxes = checks.toArray(new String[checks.size()]);
                    }
                    if (stringKey.equals(NOT_COVERED_BY)) {
                        notCoveredCheckboxes = checks.toArray(new String[checks.size()]);
                    }
                    if (stringKey.equals(APPLIED_TO)) {
                        appliedCheckboxes = checks.toArray(new String[checks.size()]);
                    }
                    if (stringKey.equals(NOT_APPLIED_TO)) {
                        notAppliedCheckboxes = checks.toArray(new String[checks.size()]);
                    }
                }
            }
            if ("document".equals(stringKey)) {
                String stringValue = entry.getValue();
                OasisRefSourceItemQuery query = new OasisRefSourceItemQuery();
                query.id().eq(Integer.valueOf(stringValue));
                selectedDocumentForFilter = query.getUniqueResult();
            }
            if ("scope".equals(stringKey)) {
                String stringValue = entry.getValue();
                AssertionScopeQuery query = new AssertionScopeQuery();
                query.keyword().eq(stringValue);
                selectedScope = query.getUniqueResult();
            }
        }
    }

    public OasisRefSourceItem getSelectedDocumentForFilter() {
        return selectedDocumentForFilter;
    }

    public void setSelectedDocumentForFilter(OasisRefSourceItem selectedDocumentForFilterIn) {
        this.selectedDocumentForFilter = selectedDocumentForFilterIn;
        modified();
    }

    public AssertionScope getExcludeAssertionsFromScope() {
        return excludeAssertionsFromScope;
    }

    public void setExcludeAssertionsFromScope(AssertionScope scope) {
        this.excludeAssertionsFromScope = scope;
    }

    public void resetSelectedScope() {
        setSelectedScope(null);
    }

    public void resetSelectedDocumentForFilter() {
        setSelectedDocumentForFilter(null);
    }

    public void clearFilter() {
        resetCheckBoxes();
        resetSelectedScope();
        resetSelectedDocumentForFilter();
        super.clear();
    }
}
