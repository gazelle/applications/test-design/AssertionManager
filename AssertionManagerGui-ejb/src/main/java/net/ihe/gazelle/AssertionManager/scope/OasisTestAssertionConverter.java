package net.ihe.gazelle.AssertionManager.scope;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

@Name("oasisTestAssertionConverter")
@BypassInterceptors
@Converter(forClass = OasisTestAssertion.class)
public class OasisTestAssertionConverter implements javax.faces.convert.Converter {

    private EntityManager em = EntityManagerService.provideEntityManager();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return em.find(OasisTestAssertion.class, Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((OasisTestAssertion) value).getId().toString();
    }
}
