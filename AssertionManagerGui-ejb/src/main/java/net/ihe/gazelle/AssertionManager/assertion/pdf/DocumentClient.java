package net.ihe.gazelle.AssertionManager.assertion.pdf;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public final class DocumentClient {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentClient.class);

    private DocumentClient() {
    }

    private static String getPdfJsWsBaseUrl() {
        String pdfJsBaseUrl = "";
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }

        pdfJsBaseUrl = PreferenceService.getString("application_url");
        if (pdfJsBaseUrl == null) {
            pdfJsBaseUrl = "";
        } else {
            if (pdfJsBaseUrl.endsWith("/")) {
                pdfJsBaseUrl = pdfJsBaseUrl.substring(0, pdfJsBaseUrl.length() - 1);
            }
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return pdfJsBaseUrl + "/pdfjs/web/viewer.html?file=/AssertionManagerGui/documents/getPDFdocument.seam%3Furl%3D";
    }

    public static String openReachablePdf(String testAssertionUri) {
        String encodedUrl;
        String url = "";
        try {
            encodedUrl = URLEncoder.encode(testAssertionUri, "utf-8");
            encodedUrl = URLEncoder.encode(encodedUrl, "utf-8");

            url = getPdfJsWsBaseUrl().concat(encodedUrl);
            LOG.info("openPdf: " + url);

        } catch (UnsupportedEncodingException e) {
            LOG.error("Could not encode url: " + testAssertionUri);
        }
        LOG.info("end openPdf");
        return url;
    }

    public static String openPdf(String testAssertionUri) {
        String documentUrl;
        if (getPdfJsWsBaseUrl().equals("")) {
            documentUrl = testAssertionUri;
        } else {
            //documentUrl = DocumentClient.openReachablePdf(testAssertionUri);
            documentUrl = testAssertionUri;
        }
        return documentUrl;
    }
}
